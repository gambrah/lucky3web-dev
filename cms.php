
<?php
include_once 'includes/session.php';
include_once 'cms/CMS.php';

$sql = "SELECT * FROM vw_news WHERE status='active' ORDER BY id DESC LIMIT 2";		
$rst = mysqli_query($con, $sql) or die(mysqli_error($con));
while($row = mysqli_fetch_assoc($rst)){
	$news[] = $row;
}

?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="MobileOptimized" content="320">
<meta name="HandheldFriendly" content="true">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'>

<link rel='stylesheet' href='assets/production/css/moda.css' type='text/css' />
<!--<link rel='stylesheet' href='assets/production/css/grt-youtube-popup.css' />-->
<!--<link rel='stylesheet' href='assets/production/css/loader.css'>-->
<link rel='stylesheet' href='assets/production/css/tab-3.css'>
<link rel='stylesheet' href='assets/production/css/animate.css'>
<link rel='stylesheet' href='assets/production/css/nav.css'>
<link rel='stylesheet' href='assets/production/css/sara-audio.css' />
<link rel='stylesheet' href='assets/production/css/slideshow.css' />
<!--<link rel='stylesheet' href='assets/production/css/tab.css' />-->
<link rel='stylesheet' href='assets/production/css/_saramusic_master.css' />

<title>Saraphina Michael Official</title>
<script src='assets/production/grt-youtube-popup.js'></script>
<script src="assets/production/slider.js"></script>
<script src='assets/production/jquery.min.js'></script>
<script src='assets/production/jquery-ui.min.js'></script>


<script>document.documentElement.className = 'advanced';</script>

<?php CMS::init(false); ?>

</head>
<body class="hero__scroll home-lang">
<section class="home-slide">

<header>
<div class="grid grid-pad">
    <div class="col-1-1">
       <div class="content">
       
       <?php include('mobile-nav.php'); ?>
       
       <!--<div class="mobile-disable_">
       <navigation id="menu" class="mobile-disable"s>
    <ul class="mobile-nav" id="mobile">
        <li class="menu-item menu-item-has-children">
            <a href="#">Home</a>
        </li>
        <li class="menu-item menu-item-has-children">
            <a href="#">SM Profile</a>
        </li>
        <li class="menu-item menu-item-has-children">
            <a href="#">Music</a>
        </li>
        <li class="menu-item menu-item-has-children">
            <a href="#">Tour &amp; Shows</a>
        </li>
        <li class="menu-item menu-item-has-children">
            <a href="#">Contacts</a>
        </li>
    </ul>
    
    
    
    
    
    
    <div class="mobile-social">
    <span class="Icon-SocialMedia">
    <ul>
           <li class="social-top-cons"><a href="#"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_36_">

	<g id="XMLID_76_">

		<path id="XMLID_84_" class="st0" d="M154.3,129.5c0,13.6-11,24.6-24.6,24.6c-13.6,0-24.6-11-24.6-24.6c0-13.6,11-24.6,24.6-24.6

			C143.3,104.9,154.3,115.9,154.3,129.5z"/>

		<path id="XMLID_80_" class="st0" d="M187.2,86c-1.2-3.2-3.1-6.1-5.5-8.5c-2.4-2.4-5.3-4.3-8.5-5.5c-2.6-1-6.5-2.2-13.7-2.5

			c-7.8-0.4-10.1-0.4-29.8-0.4c-19.7,0-22,0.1-29.8,0.4C92.7,69.8,88.8,71,86.2,72c-3.2,1.2-6.1,3.1-8.5,5.5

			c-2.4,2.4-4.3,5.3-5.5,8.5c-1,2.6-2.2,6.5-2.5,13.7c-0.4,7.8-0.4,10.1-0.4,29.8c0,19.7,0.1,22,0.4,29.8c0.3,7.2,1.5,11.1,2.5,13.7

			c1.2,3.2,3.1,6.1,5.5,8.5c2.4,2.4,5.3,4.3,8.5,5.5c2.6,1,6.5,2.2,13.7,2.5c7.8,0.4,10.1,0.4,29.8,0.4c19.7,0,22-0.1,29.8-0.4

			c7.2-0.3,11.1-1.5,13.7-2.5c6.4-2.5,11.5-7.6,14-14c1-2.6,2.2-6.5,2.5-13.7c0.4-7.8,0.4-10.1,0.4-29.8c0-19.7-0.1-22-0.4-29.8

			C189.4,92.5,188.2,88.6,187.2,86z M129.7,167.4c-20.9,0-37.9-17-37.9-37.9s17-37.9,37.9-37.9c20.9,0,37.9,17,37.9,37.9

			S150.6,167.4,129.7,167.4z M169.1,99c-4.9,0-8.9-4-8.9-8.9s4-8.9,8.9-8.9c4.9,0,8.9,4,8.9,8.9C177.9,95,174,99,169.1,99z"/>

		<path id="XMLID_77_" class="st0" d="M129.7,1.1C58.8,1.1,1.3,58.6,1.3,129.5S58.8,258,129.7,258s128.5-57.5,128.5-128.5

			S200.6,1.1,129.7,1.1z M203,159.9c-0.4,7.9-1.6,13.2-3.4,17.9c-3.8,9.9-11.7,17.7-21.6,21.6c-4.7,1.8-10.1,3.1-17.9,3.4

			c-7.9,0.4-10.4,0.4-30.4,0.4c-20,0-22.5-0.1-30.4-0.4c-7.8-0.4-13.2-1.6-17.9-3.4c-4.9-1.9-9.4-4.8-13.1-8.5

			c-3.8-3.7-6.7-8.1-8.5-13.1c-1.8-4.7-3.1-10.1-3.4-17.9c-0.4-7.9-0.4-10.4-0.4-30.4S56,107,56.4,99.1c0.4-7.9,1.6-13.2,3.4-17.9

			c1.9-4.9,4.8-9.4,8.5-13.1c3.7-3.8,8.1-6.7,13.1-8.5c4.7-1.8,10.1-3.1,17.9-3.4c7.9-0.4,10.4-0.4,30.4-0.4s22.5,0.1,30.4,0.4

			c7.9,0.4,13.2,1.6,17.9,3.4c4.9,1.9,9.4,4.8,13.1,8.5c3.8,3.7,6.7,8.1,8.5,13.1c1.8,4.7,3.1,10.1,3.4,17.9

			c0.4,7.9,0.4,10.4,0.4,30.4S203.4,152.1,203,159.9z"/>

	</g>

</g>

</svg>

</span></a></li><li class="social-top-cons"><a href="#"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_7_">

	<g id="XMLID_270_">

		<g id="XMLID_271_">

			<polygon id="XMLID_286_" class="st0" points="68.9,137.6 79.1,137.6 79.1,192.6 88.7,192.6 88.7,137.6 99,137.6 99,128.6 

				68.9,128.6 			"/>

			<path id="XMLID_283_" class="st0" d="M150.2,144.3c-3.2,0-6.1,1.7-8.9,5.2v-20.9h-8.6v64h8.6V188c2.9,3.6,5.9,5.3,8.9,5.3

				c3.4,0,5.7-1.8,6.9-5.4c0.6-2,0.9-5.2,0.9-9.6v-19c0-4.5-0.3-7.7-0.9-9.6C155.9,146.1,153.6,144.3,150.2,144.3z M149.3,179

				c0,4.3-1.3,6.4-3.8,6.4c-1.4,0-2.9-0.7-4.3-2.1v-29.1c1.4-1.4,2.9-2.1,4.3-2.1c2.5,0,3.8,2.2,3.8,6.5V179z"/>

			<path id="XMLID_282_" class="st0" d="M116.5,181.3c-1.9,2.7-3.7,4-5.5,4c-1.2,0-1.8-0.7-2-2c-0.1-0.3-0.1-1.3-0.1-3.4v-35.1h-8.6

				v37.7c0,3.4,0.3,5.6,0.8,7.1c0.9,2.4,2.8,3.5,5.6,3.5c3.2,0,6.4-1.9,9.9-5.9v5.2h8.6v-47.7h-8.6V181.3z"/>

			<path id="XMLID_281_" class="st0" d="M123.9,100.9c2.8,0,4.2-2.2,4.2-6.7V73.9c0-4.5-1.4-6.7-4.2-6.7c-2.8,0-4.2,2.2-4.2,6.7

				v20.3C119.8,98.6,121.1,100.9,123.9,100.9z"/>

			<path id="XMLID_275_" class="st0" d="M129.8,0C59,0,1.6,57.4,1.6,128.1S59,256.3,129.8,256.3s128.1-57.4,128.1-128.1

				S200.5,0,129.8,0z M144,59.9h8.7v35.5c0,2,0,3.1,0.1,3.4c0.2,1.4,0.9,2,2,2c1.7,0,3.6-1.4,5.5-4.1V59.9h8.7v48.2h-8.7v-5.3

				c-3.5,4-6.8,5.9-10,5.9c-2.8,0-4.8-1.1-5.6-3.6c-0.5-1.5-0.8-3.8-0.8-7.2V59.9L144,59.9z M111,75.6c0-5.2,0.9-8.9,2.7-11.4

				c2.4-3.3,5.8-4.9,10.2-4.9c4.4,0,7.8,1.6,10.2,4.9c1.8,2.4,2.7,6.2,2.7,11.4v16.9c0,5.1-0.9,8.9-2.7,11.3

				c-2.4,3.3-5.8,4.9-10.2,4.9c-4.4,0-7.8-1.6-10.2-4.9c-1.9-2.4-2.7-6.2-2.7-11.3V75.6z M87.7,43.5l6.9,25.4l6.6-25.4h9.8

				L99.3,81.9v26.2h-9.7V81.9c-0.9-4.6-2.8-11.4-5.9-20.5c-2-6-4.2-12-6.2-18L87.7,43.5z M198.7,196.8c-1.8,7.6-8,13.2-15.5,14

				c-17.7,2-35.7,2-53.5,2c-17.9,0-35.8,0-53.5-2c-7.5-0.8-13.7-6.4-15.5-14c-2.5-10.8-2.5-22.6-2.5-33.8c0-11.1,0-23,2.5-33.8

				c1.7-7.6,8-13.2,15.4-14c17.7-2,35.7-2,53.5-2c17.9,0,35.8,0,53.5,2c7.5,0.8,13.7,6.4,15.5,14c2.5,10.8,2.5,22.6,2.5,33.8

				C201.2,174.2,201.2,186,198.7,196.8z"/>

			<path id="XMLID_272_" class="st0" d="M177.7,144.3c-4.4,0-7.9,1.6-10.3,4.9c-1.8,2.4-2.7,6.1-2.7,11.2v16.7

				c0,5.1,0.9,8.8,2.8,11.2c2.5,3.3,5.9,4.9,10.5,4.9c4.5,0,8.1-1.7,10.4-5.2c1-1.5,1.7-3.3,2-5.2c0.1-0.9,0.2-2.8,0.2-5.6v-1.3

				h-8.8c0,3.5-0.1,5.4-0.2,5.9c-0.5,2.3-1.7,3.5-3.9,3.5c-3,0-4.4-2.2-4.4-6.6v-8.5h17.3v-9.9c0-5.1-0.9-8.8-2.7-11.2

				C185.5,145.9,182.1,144.3,177.7,144.3z M181.9,163.1h-8.6v-4.4c0-4.4,1.4-6.6,4.3-6.6c2.9,0,4.3,2.2,4.3,6.6V163.1z"/>

		</g>

	</g>

</g>

</svg></span>

</a></li>
  <li class="social-top-cons"><a href="#"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<path id="XMLID_7_" class="st0" d="M258.1,128.5c0,71-57.6,128.5-128.5,128.5S1,199.5,1,128.5S58.5,0,129.5,0

	S258.1,57.6,258.1,128.5z M158.4,108.2h-24.1V90.6c0-5.4,5.6-6.7,8.2-6.7c2.6,0,15.6,0,15.6,0V60.2l-17.9-0.1

	c-24.4,0-29.9,17.7-29.9,29.1v19H92.6v24.5h17.7c0,31.4,0,66.1,0,66.1h24c0,0,0-35,0-66.1h20.4L158.4,108.2z"/>


</svg>

</span></a></li></ul></span>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
</navigation>

<div class="menu-container">
    <a class="menu-btn menu-link" href="#menu" id="nav-icon">
      <span></span>
      <span></span> 
      <span></span>
    </a>
</div></div>-->
       
       
           <nav class="desktop-disable">
           
           
           
        <!--   <div class="hover-to-show-link">
    <a href="#">
        <h2>Logo</h2>
        <p class="hover-to-show">text</p>
    </a>
</div>--><div class="outer-container">
           <div class="language-toggle">
     <a href="#" class="labels label-en" data-lang="en"></a>
      <div class="language-switch on"></div>
      <a href="#" class="labels label-fr" data-lang="fr"></a>
    </div></div>
           
           <ul><li><a href="#" onmouseenter="playAudio()">home</a></li><li><a href="sm-profile.php" class="" onmouseenter="playAudio()">SM profile</a></li><li><a href="music.php" onmouseenter="playAudio()">music</a></li><li><a href="tour.php" onmouseenter="playAudio()">tours & shows</a></li><li><a href="contacts.php" onmouseenter="playAudio()">contacts</a></li>
           
           <audio id="audioID">
  <source src="assets/production/sounds/click.mp3" type="audio/mp3">
  <source src="assets/production/sounds/click.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio>
           
           
           <span class="Icon-SocialMedia">
           <li class="social-top-cons"><a href="https://www.instagram.com/saraphina_michael_official/" target="_blank"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_36_">

	<g id="XMLID_76_">

		<path id="XMLID_84_" class="st0" d="M154.3,129.5c0,13.6-11,24.6-24.6,24.6c-13.6,0-24.6-11-24.6-24.6c0-13.6,11-24.6,24.6-24.6

			C143.3,104.9,154.3,115.9,154.3,129.5z"/>

		<path id="XMLID_80_" class="st0" d="M187.2,86c-1.2-3.2-3.1-6.1-5.5-8.5c-2.4-2.4-5.3-4.3-8.5-5.5c-2.6-1-6.5-2.2-13.7-2.5

			c-7.8-0.4-10.1-0.4-29.8-0.4c-19.7,0-22,0.1-29.8,0.4C92.7,69.8,88.8,71,86.2,72c-3.2,1.2-6.1,3.1-8.5,5.5

			c-2.4,2.4-4.3,5.3-5.5,8.5c-1,2.6-2.2,6.5-2.5,13.7c-0.4,7.8-0.4,10.1-0.4,29.8c0,19.7,0.1,22,0.4,29.8c0.3,7.2,1.5,11.1,2.5,13.7

			c1.2,3.2,3.1,6.1,5.5,8.5c2.4,2.4,5.3,4.3,8.5,5.5c2.6,1,6.5,2.2,13.7,2.5c7.8,0.4,10.1,0.4,29.8,0.4c19.7,0,22-0.1,29.8-0.4

			c7.2-0.3,11.1-1.5,13.7-2.5c6.4-2.5,11.5-7.6,14-14c1-2.6,2.2-6.5,2.5-13.7c0.4-7.8,0.4-10.1,0.4-29.8c0-19.7-0.1-22-0.4-29.8

			C189.4,92.5,188.2,88.6,187.2,86z M129.7,167.4c-20.9,0-37.9-17-37.9-37.9s17-37.9,37.9-37.9c20.9,0,37.9,17,37.9,37.9

			S150.6,167.4,129.7,167.4z M169.1,99c-4.9,0-8.9-4-8.9-8.9s4-8.9,8.9-8.9c4.9,0,8.9,4,8.9,8.9C177.9,95,174,99,169.1,99z"/>

		<path id="XMLID_77_" class="st0" d="M129.7,1.1C58.8,1.1,1.3,58.6,1.3,129.5S58.8,258,129.7,258s128.5-57.5,128.5-128.5

			S200.6,1.1,129.7,1.1z M203,159.9c-0.4,7.9-1.6,13.2-3.4,17.9c-3.8,9.9-11.7,17.7-21.6,21.6c-4.7,1.8-10.1,3.1-17.9,3.4

			c-7.9,0.4-10.4,0.4-30.4,0.4c-20,0-22.5-0.1-30.4-0.4c-7.8-0.4-13.2-1.6-17.9-3.4c-4.9-1.9-9.4-4.8-13.1-8.5

			c-3.8-3.7-6.7-8.1-8.5-13.1c-1.8-4.7-3.1-10.1-3.4-17.9c-0.4-7.9-0.4-10.4-0.4-30.4S56,107,56.4,99.1c0.4-7.9,1.6-13.2,3.4-17.9

			c1.9-4.9,4.8-9.4,8.5-13.1c3.7-3.8,8.1-6.7,13.1-8.5c4.7-1.8,10.1-3.1,17.9-3.4c7.9-0.4,10.4-0.4,30.4-0.4s22.5,0.1,30.4,0.4

			c7.9,0.4,13.2,1.6,17.9,3.4c4.9,1.9,9.4,4.8,13.1,8.5c3.8,3.7,6.7,8.1,8.5,13.1c1.8,4.7,3.1,10.1,3.4,17.9

			c0.4,7.9,0.4,10.4,0.4,30.4S203.4,152.1,203,159.9z"/>

	</g>

</g>

</svg>

</span></a></li><li class="social-top-cons"><a href="#"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_7_">

	<g id="XMLID_270_">

		<g id="XMLID_271_">

			<polygon id="XMLID_286_" class="st0" points="68.9,137.6 79.1,137.6 79.1,192.6 88.7,192.6 88.7,137.6 99,137.6 99,128.6 

				68.9,128.6 			"/>

			<path id="XMLID_283_" class="st0" d="M150.2,144.3c-3.2,0-6.1,1.7-8.9,5.2v-20.9h-8.6v64h8.6V188c2.9,3.6,5.9,5.3,8.9,5.3

				c3.4,0,5.7-1.8,6.9-5.4c0.6-2,0.9-5.2,0.9-9.6v-19c0-4.5-0.3-7.7-0.9-9.6C155.9,146.1,153.6,144.3,150.2,144.3z M149.3,179

				c0,4.3-1.3,6.4-3.8,6.4c-1.4,0-2.9-0.7-4.3-2.1v-29.1c1.4-1.4,2.9-2.1,4.3-2.1c2.5,0,3.8,2.2,3.8,6.5V179z"/>

			<path id="XMLID_282_" class="st0" d="M116.5,181.3c-1.9,2.7-3.7,4-5.5,4c-1.2,0-1.8-0.7-2-2c-0.1-0.3-0.1-1.3-0.1-3.4v-35.1h-8.6

				v37.7c0,3.4,0.3,5.6,0.8,7.1c0.9,2.4,2.8,3.5,5.6,3.5c3.2,0,6.4-1.9,9.9-5.9v5.2h8.6v-47.7h-8.6V181.3z"/>

			<path id="XMLID_281_" class="st0" d="M123.9,100.9c2.8,0,4.2-2.2,4.2-6.7V73.9c0-4.5-1.4-6.7-4.2-6.7c-2.8,0-4.2,2.2-4.2,6.7

				v20.3C119.8,98.6,121.1,100.9,123.9,100.9z"/>

			<path id="XMLID_275_" class="st0" d="M129.8,0C59,0,1.6,57.4,1.6,128.1S59,256.3,129.8,256.3s128.1-57.4,128.1-128.1

				S200.5,0,129.8,0z M144,59.9h8.7v35.5c0,2,0,3.1,0.1,3.4c0.2,1.4,0.9,2,2,2c1.7,0,3.6-1.4,5.5-4.1V59.9h8.7v48.2h-8.7v-5.3

				c-3.5,4-6.8,5.9-10,5.9c-2.8,0-4.8-1.1-5.6-3.6c-0.5-1.5-0.8-3.8-0.8-7.2V59.9L144,59.9z M111,75.6c0-5.2,0.9-8.9,2.7-11.4

				c2.4-3.3,5.8-4.9,10.2-4.9c4.4,0,7.8,1.6,10.2,4.9c1.8,2.4,2.7,6.2,2.7,11.4v16.9c0,5.1-0.9,8.9-2.7,11.3

				c-2.4,3.3-5.8,4.9-10.2,4.9c-4.4,0-7.8-1.6-10.2-4.9c-1.9-2.4-2.7-6.2-2.7-11.3V75.6z M87.7,43.5l6.9,25.4l6.6-25.4h9.8

				L99.3,81.9v26.2h-9.7V81.9c-0.9-4.6-2.8-11.4-5.9-20.5c-2-6-4.2-12-6.2-18L87.7,43.5z M198.7,196.8c-1.8,7.6-8,13.2-15.5,14

				c-17.7,2-35.7,2-53.5,2c-17.9,0-35.8,0-53.5-2c-7.5-0.8-13.7-6.4-15.5-14c-2.5-10.8-2.5-22.6-2.5-33.8c0-11.1,0-23,2.5-33.8

				c1.7-7.6,8-13.2,15.4-14c17.7-2,35.7-2,53.5-2c17.9,0,35.8,0,53.5,2c7.5,0.8,13.7,6.4,15.5,14c2.5,10.8,2.5,22.6,2.5,33.8

				C201.2,174.2,201.2,186,198.7,196.8z"/>

			<path id="XMLID_272_" class="st0" d="M177.7,144.3c-4.4,0-7.9,1.6-10.3,4.9c-1.8,2.4-2.7,6.1-2.7,11.2v16.7

				c0,5.1,0.9,8.8,2.8,11.2c2.5,3.3,5.9,4.9,10.5,4.9c4.5,0,8.1-1.7,10.4-5.2c1-1.5,1.7-3.3,2-5.2c0.1-0.9,0.2-2.8,0.2-5.6v-1.3

				h-8.8c0,3.5-0.1,5.4-0.2,5.9c-0.5,2.3-1.7,3.5-3.9,3.5c-3,0-4.4-2.2-4.4-6.6v-8.5h17.3v-9.9c0-5.1-0.9-8.8-2.7-11.2

				C185.5,145.9,182.1,144.3,177.7,144.3z M181.9,163.1h-8.6v-4.4c0-4.4,1.4-6.6,4.3-6.6c2.9,0,4.3,2.2,4.3,6.6V163.1z"/>

		</g>

	</g>

</g>

</svg></span>

</a></li>
  <li class="social-top-cons"><a href="#"><span class="sm-icons"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 259.5 259.2" style="enable-background:new 0 0 259.5 259.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<path id="XMLID_7_" class="st0" d="M258.1,128.5c0,71-57.6,128.5-128.5,128.5S1,199.5,1,128.5S58.5,0,129.5,0

	S258.1,57.6,258.1,128.5z M158.4,108.2h-24.1V90.6c0-5.4,5.6-6.7,8.2-6.7c2.6,0,15.6,0,15.6,0V60.2l-17.9-0.1

	c-24.4,0-29.9,17.7-29.9,29.1v19H92.6v24.5h17.7c0,31.4,0,66.1,0,66.1h24c0,0,0-35,0-66.1h20.4L158.4,108.2z"/>


</svg>

</span></a></li></span>



</ul></nav></div>
       </div>
    </div>
</div>
</header>
  <div class="bgimg-1_ main-banner" style="background-image: url(assets/creatives/sara-homepage.jpg)">
  <div class="grid grid-pad">
  <div class="col-1-1">
    <div class="content main-brand">
    
    <span class="sara-logo"><svg class="animated bounceInDown" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 549 549.4" style="enable-background:new 0 0 549 549.4;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g>

	<g>

		<defs>

			<circle id="SVGID_1_" cx="275.2" cy="272.8" r="269.8"/>

		</defs>

		<use xlink:href="#SVGID_1_"  style="overflow:visible;opacity:0.2;fill:#EAEAEA;"/>

		<clipPath id="SVGID_2_">

			<use xlink:href="#SVGID_1_"  style="overflow:visible;opacity:0.2;"/>

		</clipPath>

		<g style="opacity:0.19;clip-path:url(#SVGID_2_);">

			

				<image style="overflow:visible;" width="291" height="244" xlink:href="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4QMxaHR0cDov
L25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENl
aGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4
OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzEzOCA3OS4xNTk4MjQsIDIwMTYvMDkvMTQtMDE6
MDk6MDEgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5
OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHht
bG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6
Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUu
Y29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBo
b3Rvc2hvcCBDQyAyMDE3IChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM4
N0FBODY2NEQ3OTExRTlBQTdCRTFCMTIwNkUyRUI3IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlk
OjM4N0FBODY3NEQ3OTExRTlBQTdCRTFCMTIwNkUyRUI3Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0
UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6Mjg0NTYzMDk0RDRDMTFFOUFBN0JFMUIxMjA2RTJFQjci
IHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Mjg0NTYzMEE0RDRDMTFFOUFBN0JFMUIxMjA2RTJF
QjciLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tl
dCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB
AQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMD
AwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCAD0ASMD
AREAAhEBAxEB/8QAqwAAAwEBAQEBAQEAAAAAAAAAAAEFAgYHAwgJCgEBAQEBAQAAAAAAAAAAAAAA
AAECAwQQAAEEAgIBAgIHBgIHBAcFCQUDBAYHAggBCRYRFRQXIRMmJxgZCgAxI0MkJRI1RGRFNkY4
GkEiNzlRYTSGR1iJ0TJUVWVKcYGhQjN1hWkRAQACAQMDAgUFAAMAAAAAAAABERIhMQJBUWEiE3GB
odEykbHB4dLwYgP/2gAMAwEAAhEDEQA/AP462BYMm7EJNXHSR0kVwciGjsQOZSebTiTZvwEl2iko
F+CRmO5e5UyRBNCgWqgpJmPcBQzgcm4UcJCEkRGJPGJxOPehDt+4I1AI2y6OOjlkdvKRXmebQzcf
ciGNmDKxd7bGZsCSB6vK8PoE1RVf6b18LWKcPHnunAFcD7gqoQWAqyCQTUL1lWTCem6Emuu3rsNf
Prtdvr22o9ydyKiHFpGWqQtIyw9hjpJpLwwY+U8zjmV8t2Zw2zbpm8TaafqmjI0h4+Ag/WtP079a
8Zc8Qa3+8u3oN/3cPWPz+s+ritJ9H/T9/Hu8Wmm1k1ixf6efRyPaj3Xpx9ZGlfvDBVtWsL6boUF7
EexENzfXbFfnuFu6c6c24QLSItUBaRFXxD8bO7XL9/zKOZ3jKPiHoME9cJG0zSSv8VKSJECMBCHT
9PxmAxh93j94z45eJ28TrmZ6daczN0wZ2LvjYrNgMXA2DYQBYYsLgGmkBGri+WTLkXiBdAuB+CY9
UCrHo/NAVf1/JuxCTWL3c93FjHYjo5EDvEagcDjPD4BJtopLH355WHab6bQ9Y40KA6qClGpBubNt
iKTjNdMsooXxJ8SyWR4NhxFt959uSnbPbOUAtFeoHRQE2iyqsWbtxVSa31ILQjraP6y6yx9vHUxd
gbI2AKTBs3z5kDXzRzcCscRfKPMOhr0Mly9td59sxfUrUuLgtEunvRME4lOCcpcIiql10qUUjInE
g2Z2ZPuJCkMsHY6wRiRx4wYPDrjPBRcrnmU5Q5mUxeBqwbAk3YfJq46SOkiuDkQ0diJ3OTTWbyXJ
9H5LtDJAD8GhMty9y5kkCaFAlUhCjQeuGDuB6a6i6QhFERiT4icUj4FwW/G4BGmPRx0bsjt4yO8j
jaF7kbkQxqPZ2LvZYjJgSSP15XZ5AoqKgGm1eiVSnDx37rwBcAcSCihBYEtIZBNQuWTZMJ6boSb6
7Ou018+e1y+uBtRbk7kVGOLyQvUhaRlh7DHSTSXEex5lPM4ylfLdmcNs26Rv3xNPnlNKRpDh0BB/
dr+nfrXjnnwW3+8u34N/Dw55AT+tOretJ/H/AE9cuOPd4tNtrZrFjH088cOR7Qe69MfrI0r94YFa
1rC+m6FBuxHsRDfPvtivvkjbunOnNuES0iK1AWkRV8/53Z3a5Iv+ZRlO+JTy4eggTxykbTNJK+qq
UkSIkYCEKn6fjMBjD7vG7xnx28Tl4HXE00505mbke0sXfGxWbAauBsKwgK4xUXANNICMWF8smXIv
EE6BcD8MB6oBWPx+aAV/X8m7EJPY3dx3c2KdiWjkROYxqBQOM8PwEl2gk0ffnVodpvptD1TjQoDq
oKTbEG5s22IpOM10i6qhfEnxLJZHgYgRbnefbcp2z20lALRTqB0VBN4sstFm6Aqpdb6kFoR5tHtZ
dZo63jqYqwNkZ+JSBs375oDcZo5ri8cRf1HMNhr0EXLWz3oWzGNS9S4uC0T6e9EwTiU44SlwiKqT
XOpRaMiXkOzGzJ9xIExlhbG2CLTOvB7B4dcKYKOCueZTlDzKYvALBsGT9iEmrjpI6Sa4OxHRyInc
5NNpvJsn8fku0UlAPwSEy3L3LmSIJoUCVUEKNR7gMGcj011F0xKKIjEnxE4nHwLguCNQGMsejjo3
YnbykV5HWsM3J3IhjYexsTeyxWTAkkfruvDyBRYVANN6+FLFeHjz3TgCuB4IKKkFQKshkE1C1ZVk
wnpvhBrrs67jXF89rl9e21FuRuRUQ4rIy9SFpEXHsONJNJcR7DmVczjyvluzNm2bdI5icTT55TSk
iQ4fASTrBc/LX9O/WnrlxBrd7yregvPGGPPMfn1Z9W9Z2AA+jnnnj3aLTTa2bRYt9PPHxI9sPc+n
H1kaU+8Ma15Yres4T03wkL2JdiIb599sN9+427pxpxbpEtIi1QFpCWfPst292eSD/KUedeU8rvQY
N44SNpm0VP4qUkSIkYCN0CnqejMBjD7vG7x3xy8T14nXM00605mTpgzsXfCxWbAYsBsKwQKwxYXX
2msAFrC/g2XteIFwC4H4YD1QKsej80zxiarkrNf19JuxGTWN3c93FjHYjo3EDvEZgsEjWL8BJ9op
KAfnlYbptptDljjQoFqkGTakW5s03IpLqOEi6qpfEnjLJZHtBCQ9td59syjbXbSUAtE+oDRME3iy
mcWbICql1vqQShHm0f1l1ljzaO4C7B2Pn4pIGzfv2YNfNLNYVhiL5R5hsNeAi5e2+9C2ovqVqXGA
WifT7okCcSnDCUuEBVS65VKLRkTmQbMbMyBxIUhdgbH2AMTOvGA94cXzTzXKZ5lOUfMZi8B2BYEl
7EJNXHSR0kVydiOjcQOZSaczmTZP4/JNo5KAfg0ZjuTuXMkQTUoDqgKUaD3AUK4HpuFXCQlFIRiT
xicTjwFwXBGYDGmPRx0cMjl4yG8jrWG7jbjQxswZWJvbYrJgSRP17Xp9EmqLr7TavxapT417wUxB
OAWL9RQiqBVkEgmgW7KsmFdN8KNddvXaa+fXa/fXttR7k7kVEOLSMtURaRFmDDHSTSXFgx5lPnPl
PLdmcNsm6JrE2kn6pIyRIcOgAP7tf079acf7jW93lW9Bfo4/sE/rTq4rSfR/n6ef83i012tmsWLf
6yPbD3X8yNK/eGBWtaQnpuhYbsT7Egvz77Yb79yt3TfTe3SJWRFqgLSMq+f5bt7tcv3/AJR515R9
e8Bg3jhI3gaSU44URkiREjAQhU/T0agMYfd4/eM+O3gfvE66mmnOnUzdMGVib32KyYDFgFhWCAWF
qi6+01gAtYX8Gy9rxAuAWI/DAcqBVj0fmYFfV/JuxGTWP3cd3FjHYjo3ET3EZgsGjGL4BJtopLHy
B5WG6a6bQ9Y40KhKqBlGpBubNtyCThRwkXVVL4k8ZbLI8DECLb7z7alO2m2koBaKdP8AomCbxXNS
LNkBVSa31IJQjzaP6zazR5vHExc/2Qn4pMEzfvmYNdRLNcVhiK+p5h0NeAFy1ud6FtRfUrUuLgtE
+n3RIE4lOGEpcICql1yqUWjInUg2Y2ZkLiQpCrA2PsEWmceMB7w4vmlmuUzzKco+YzF4Hq3zU/TL
f7peBXl4N/5e/kPwi3n/AMlf/G383z675B+8fPX8RX2Y9m8h92+Wf8Lw/wBs+xP7B5hcFwRqAxpj
0cdHDE5eUivI42hm4+48Maj2Vib3WIyYEkT9e16eQKKi6/03r0YoU+Me+6YgVwPBBRQisBVkMgmg
XbKsqE9OEHOddnXYa4vrtdvr26otydyKiGlZEWqMtIizBhxpLpNiwYcyrzjmU8t2Rw2zbpG8DaSX
qmlJEhw+AhvnmtP079a+vpBrf7yrfg3P+HH1AT+tOris5/H+fp55493jE12tm0WLc+vP9SPaj3Pp
x9ZGlfvDAratoV03woL2I9iIX599sV9+4W7pzpxbhEvIytQFpEXfEOd2t2eSD/mUec8Sjlw9BAnj
hI0mbRU/ipSVIgRgNpnKEGn6gjEBjD7vG7xnx68T15HXUz0605mbpgzsXfGxGbAauAsKwgC41YXX
+msBGrC+WTLkXwBdAuB+CY9YCrHo9NFGUHAIBJuxCS2L3cd3NjHoho7EDuMZgkDjHBABJdo5KAfn
VYdptpvDlTrUoEqkIUakWxo23IpL5rpF1VC+JPiWSyPKMoAkTbfefbcp2z20lILRPqA0UBtouopF
kERVSa31IKQjzaPazayx5vHkxdgbIWAJTBs375kDXURzXF44ivqeYdDXmMvEmUeSLFra70LbjGpe
pcXA6JdPeiQJxKcU5UuiKqTXKpBSMicSHZfZk+4kKYywdjrBFpHXjBg8OOM8FHBXPMpyj5lMnul1
fSwLAk3YhJ646SOkiuDsR0ciJ3OTTecSbN/H5LtHJQL8EjMdytypkiCalAdVBSbUc4ChnA5Nwoum
IRRD4k8YnE48UrguCMwCNMejfo3ZHLykd5HW0M3I3HhjUexsTe2xWQ8igeryvDyJRUVX+m9fClSn
xbz3TgC4A8EFFCCwFWQyCaBdsmyoT04Qk3129dpri+u16+vbqi3J3IqIaXkZapC8iLD2PGkuk2I9
jzKeZxzKuW7M4bZt0jmJzBPnlNGSJDx0BA4+Wv6d+tPp8Ft/vKt6C8844c8gJ/WvVxWc+j/78uf7
vFprtbNYsX/f6OR7Uc69OPrI0r94YOta1hfTdCw3Yj2JBvn52xX5yRt3TnTq3CBWRlafKyIq+f8A
O7O7XL9/zKPO+JPku9BgnrhI2maSU/iJSRIiRgIQafp6MQKMPu8bvGenbwO3gdczPTrTqaOR7Oxd
8LEZsBi4GwrCArjFRcA01gA1YX8Gy5F8AXQLgfgmPVAqx+PzQNV9X8n7EJNY3dx3cWMdiGjkRO8R
qBwOM4vwEm2jkoB+eVh2m+m0OVNtSgSqQhRqQbmjbYik4zcJF1FC+JPGWSyPAhIi3O9C25TtntnK
QWinUDomCbxZVWKt0BNSa31KKQjzaO6y6yx5tHEhdgbI2AKTBtH75mDXzSzXFY4i/quYdDXoIuWt
rvPtmMal6lxcFol0+aKAnEqwTlLhEVUuudSC0ZE4kOzGzB9xIUhlg7G2EMSOPGDB6dcZ4KLlc8yn
KHMymLwNWBYEn7EZNXHSR0kVwciGjkROZSabTiTZEI/JNoZKAfgkZjuXuVMkgTUoEqkKSaD3AYM4
HprqLpCEURGJPGJxOPArgt+NQGNMujjo3ZHbxkd5HW0L3I3IhjYeysTeyxGQ8kieryvDyJRUVANN
q9FKlOHjv3TgA4A4kFVCCwFWQyCahbsmyYT03wo112ddpn59drl9cDqi3J3IqMcXkRapC0jLsGGO
kukvA9jzKcpxlK+W7M2bZt0jeBtNP1TSkiQ4fASdRzzWn6d+tOMvsLb3eXcEF55xw48fn9adXFaT
4B6+vP8Am0Xm21s1ipf6ef6ke1HuvTj6yNKfeGUq1rSFdN8LDdiXYiF+fXbFffuNuacac24RLSIt
UBaRF3z/AJ3b3YyfP/KfOvKOV3gIG8com0jaan8VKSJESEBDn6gp6MwGMPu8fvGfHbxPXidczTTr
TmaOWDKxd8LGZsBqwKw7CALjFhdf6bQAWqL+DZ8i8QLkFwPwwHqgVY9H5oGa/r+TdiMnsbu47ubG
ORHRuIHeIzBYJGcX8fk20Umj5E8tDtNdN4csdalQlUgyjQi3NG25FJdRwkXVVL4k8ZbLI830kIUI
tvvOtuUbabaSgFon1AaKA20WzUizZAVUmuFSCkI82j+tGs0fbx1MVYGyFgCkwTN8+Zg11Es1xWOI
rlHmHQ14TZkwWtvvQtuMal6lRcHol0/aJgnEqwxlLhAVUuuNSikZE4kGzOzEgcyJIXYOx8/FJHHr
Bg9OL5pKLFM8yv1PmMxeFKwLAk3YhJa46R+keuTkQ0biB3KTTmcSbJ/H5LtHJQD8GlMdytypkiCa
FQtUBCbQe4ChXI9Jwq4TEJJCMSeMTiceBXBcEZgEaY9HHRwyOXjIbyONoZuNuNDGw9lYu9tisx5J
E/XtenkCqoqv9Nq/GKFPjXvBTEC5A4kFFCKoFWQyGaBcsqyYV03Qk1129dhv599sF9e21FuRuRUY
4tIi1QlpEWYD8dJNJcWDDmU8TnynluzNnGTdE1gbST55TSkqQ4dAAfrWn6d+tPp8Ft7vKt2C8fR/
YJ/WnVvWk/Afv54/u0Wmm1k1ixX0/wBJHth7n+ZGlPvDBVrWsK6boUG7EuxIL8+u2G+/cbd0303t
0iXkRany8iLPn2W7W7fL9/5RxOfKOXD0IDeOEjeBpJTjhRGSJESMACFT9PxmAxl73j94r47eJ+8D
rmZ6dadTJyPZWJvfYjIeMWA2FYQFYWqKr/TaAC1hnwTL2vEC4BYj8MByoJWPR+Zg6+r6S9iMmsfu
47uLGORHRuIHeIzBoLGcSACTbRyaPvzysP0102hyxxqVCVSEKtSDc0abkUl1HCRdVUviTxlssjwI
QJtrvQtqUbabZycFon0/6Jgm8WzUizdAVUut9SCkI83j2s2s0fbR1MXP9kJ+KTBM375mDXUSzXFY
YiuUeYdDXgMuXtvvQtuL6lalxcFon0/aJgnEpxwlLhAVUuuNSikZE6kGzGzEgcyFIXYGyFgDEjrx
gweHF80s1ymeZT6nzGYvALAsGS9iEmrjpI6R64OxDRuIHeZNOZzJsn8fku0clAPwSMy3K3LmKINo
UC1SEKNB7gKFcj0nCrhMQkkJxJ4xOJx4PVfy/ugD/c78xrn/AO9+X981vh43/wA63/jv+Yz7d89P
+Rj0+5f6vxrxT/TvJ/8Ajv8AYONsmyoT03wk113ddhn59dr18+3VHuRuRUQ0rIi1RFpEXHsMdJNJ
cWLDyjzniU5NmRw4ybpG8DaSXqklJEhw6ABv7tf08FacevEFuDvKt6C/93H1j8/rTq2rOfR/9/Pp
7vF5ttZNosX+nn+pHtR7n6PrI0p94YFa1tC+m+FBexLsSDc332w337jbmnOnFukC0jLVCWkRZ8/5
3Z3a5fv+ZRzOvKfiHoIG9cJm0zaan8VKSJECMBCDT1PxqAxh93jd4z45eJ28TrmaadaczNywZ2Nv
hYrNgMWAWFYQFYYsMgGmsAGqi+WbPkXwBdAuB6aY5UArHo9NK5nX9fyfsQktjd3PdxYx2IaOxE7x
GYJBI1w/ASbaOSgH51WHababw9Y40KBKpBlGpBsaNtyKThRdIuoqXxJ4yyWR4AQItrvPtuUbZ7aS
gFop1A6KA20VVVirZAVUmt1SCkI82j2sussfbx1MVYGyE/Epg2b98zBr5o5risMRX1PMOhrwEXL2
13oWzF9StTIuC0S6fdEwLmU4YSpwkKqXXKpRaMiXkGzOzJ9zIUxlg7H2CLTOvGA94ccZ4ZuCueZT
lHzGZPA3YNgybsQk1b9I/SPXB2IaORE7nJ5vOJNmQj8l2ikoB+CRmW5W5cyRBtCgSqgxJoOcBQzg
em4UXTEIoiMSeMTicejodv3BG4BGWPRx0cMjl4yK8jjWF7kbkQxswZWLvdYjJgSSP15XZ5AmqKgG
m1fClinxbv3TgCuB4fqKEFgKshkE1JquWVZUK6boQa67eu018+e12+/baj3J3IqMcXkRepC8jLMG
OOkmkuI9hzKuZxzKuW7M2bZt0jeJtJP+GjJEhw+AlH3afp36045+w1vd5dvQbn/DhxyAn9adXFZz
6P8AH05fQWi012tmsWL/AOsj2zB1/MjSv3hkOtK1hfTbCgvYj2JBvn32w337jbunGnFuES0jK0+W
kJV8/wCd2t2eSD7mUedeU5LvQQN44SNpm0lOfrUpIkRIwEqFT1QRmAxl/wB4/eM+PXidvA65menO
nUycsGdi74WI0YDVwNg2CAXGLC6/00gA1YXyyZe14AXILEfhgPVAKx6PzQHX9fybsRk1i93HdzYx
2I6NxA7jGYJA41i/ASbaKSgH55WHababw5U41KBKqBk2pBuaNtyKThRdIsqoXxJ8SyWR4gECLc7z
7blG2e2cpB6J9QGigJtFlVIs2QFVLrfUwpCPto9rLrLHm0dTFWDshYApMGzfvmgNxmjmuKxxF/U8
w6GvQRctbPefbMW1L1Li4PRLp70TBOJTjjKnCIqpdc6kFoyJeQbM7Mn3EhTGWDsfYIxM68YMHp1x
ngo4K55lOUfMZk9KdgWFJuxCTVx0k9JFcHYjo3ETuUmm84k2T8BJto5IAfgkZluXuXMUgbQoDqkM
UZj3AYM4HpuFF0hCKIjEnjEonHgLfuCNQCNsejjo3YnbykV5HG0L3I3IhjYeysXeyxGTAkieryvT
yJRQVX+m1fi1Snxbv3TgAuBxIKKEFgK0gkE0C5ZdkwnpvhBrrt67TXF9drt9cDai3J3JqIcXkRap
C8iLsGHGkmkvDBj5TlN/K+W7M2bZt0jfBtNP+GlI0hw+Akm+jPOVafp36z45+w1vd5dvQbn/AA4/
4o/Pq06uKznwDj15+j3eLTTa2axUv9PPHxI9sPdfR9ZGlPvDqTIratoV03wsN2I9iIXm++2K+/cL
d0405twkVkRWny0jKviHO7W7XL9/zKPOvKOV3oMG9cJm0zSKn8VKSpESMBk6RZlCBT9PxiAxh73j
94z07eJ68DrmZ6caczJywZ2LvhYrMeNWAWFYQBYYsLgGmsBFrC/g2fIvgC6BYj8MB6oFWPR+aWjK
BX9fybsQk1jd3HdxYxyIaORI7jGoJBI1w/ASbaGSgCB5WHab6bQ5Y61KBKpDFGhBucNtyKThRdIu
ooXxJ4y2WR6LuyID253n23J9s9tZQB0T6gNEwbaLKKRZsgLqXW+oxSEebR7WXWWPNo6kLsDZGwBS
YNm+fMwa6iWa4rDEVyjzDoa8KyYL2z3oW1GNStSosC0T6fdEgTiU4YSlwiKqXXGpRaMicyHZjZqQ
OZEmLsHY+wRiR14wHvTjjNJRcpnmU5R8xmTyszNwVgWDJexCTVx0j9I9cHoho3EDmUmnM4k2T8BJ
do5MAIAkpjuVuXMUgTUoFqkKUaD3AYK5HpOFXCYhFIRiTxicTjxLjyxcNwRqARph0c9HLI7eEhvI
62hm4+4sLbMGdi73WKxYEkT9e16eQKKiq/01r4WoU+Ne8FMQLgBi/wA8yKgFWQSCZxrdcsqyoT03
Qo1129dpr59dr99e3VHuTuRUY4tIy1RFpEWYD+NJNJcWDDynic8ynluzOHGTdE1iaST9UkZIkOHQ
Ao+7T9O9Wn/Atvd5VuwX939hn9adXFaT8Bz6evr7vF5rtZNYuV/1ke2Huv5kaU+8MCta1hXTdCgv
Yj2Jhfn12wX37jbum+m9uES0iL0+WkRd8/y3b3b5fv8AyjzriUfEPAgN44SN4GklOPrUpIkRIwEI
VPU/GoDGH3eP3ivjt4yC8TrqZ6dadTNywZWJvdYjIeMWAWFYIFYYqLr/AE1r8WqL+DZe14gXALEf
hgOVAqx6PzMHX1fyXsRk1jd3HdxYxyI6NxA7jGYNBYziQASXaOTR8gdWh+mum8OWONCgSqQhRoRb
mjbcik4UcJF1VS+JPGWSyPACBNt959tSnbXbOUAtFOoDRME3i2akWboCqk1vqQUhHm8e1m1mj7eO
pi5/shPxSYJm/fswayiWa4rHEVyjzDoa8ALlrb70Lbi+pWpMXBaJ9P8AomCcSnDCUuURVS641ILR
kTmQ7MbLyBzIUhU/2QsEWkdeMGDw4vmlmuUzzKco+YzF4BYFgybsRk1cdI/SPXJ2H6NRE7lJp1OZ
Nk/ASXaOTAH4JGY7lblTJEG0KBapCFGg9wFCuR6ThVwmISSEYk8YnE48BcFwRmARpj0cdHLI5eMh
vE61hm4240MbD2Vi722IyHkkT9e16fQKKCq/02r8WqU+Ne8FMQTgDiQzzIqgFpDIZmHq/wD06FJ/
7o/mKUX5z/yWeQ+4y3wH85f/AMb/AMLf1/yp9n+Rf4dPsx775F5T8yv4XtPuf2J/YOM+7X9O9Wnr
zzBre7ybeg3/AHePUBP606t60n4Dnjnnn6TEXmu1s1ixb6ef6ke1Huvo+sjSv3hhuta2hXTfCgvY
j2JBeb77Yb65I27pxpzbpEtJCtQFZEWfEOd2d2uX7/KU+deUcrvQQN44SNpG0VP4qUkSIkYCS9aQ
qep+MQGMvu8XvGfHbxPXgddTTTrTmZOmDOxd8LGZMBi4CwbCArDFhkA01r8WuL+DZ+18AXQLEfhg
OVAqx6PTQrNf1/J+xGTWL3c93FjHYjo7ETvEagkEjOL4BJdopKAfnlYbptptDljjQoEqkIUakG5s
23IpOFV0i6ipfEnjLJZHq5tiQ9t959uSjbPbSUAtFOoHRME3iyikWbICql1vqQU3j7ePaz6yx5vH
UxdgbIWAKSBs375mDXURzWFYYivqeYdDXq1xlguVtrvQtqL6l6lRcFol0+6JgnEqxxlThEVUuudS
DEZE5kOy+zEgcSFMXYOx9gikzrxgwenHCmGbgrnmU5R8xmLxZjLVgWBJuxGTVz0k9JFcHYho5EDu
UmnE4kuT8BJdo5KAfgkZluVuVM0QTQoEqkKSaD3AYM4HJuFF0hKKIjEnjE4nH1mMi4LhjUAjTHo4
6OGJ28pFeR1rDNx9x4a2HsrF3tsVkwJJH68rw8iUVFV/pvXwtYp8Y8914BLgcSCihFYCrIZBNFLl
C7ZNlwnpvhBrrs67jXz57Xb64HVFuRuRUY4tIi1RlZEWYMMdJNJcWLHmU5TjmVct2Zw2zbpG8TaS
fqmjJEh4+AqMoH+KtP079a+vPEGt7vKt6C884Y8cgLArTq3rOfx/j05559S8Wm21s1ixj6eeOHI9
sPdfR9ZGlfvDUZQdbVvC+m6EhexHsRDc352xX3yRtzTfTm3SJaRFqfKyMq+f87tbs8kH3Mo5nXk/
K70GDeOEjSZpFT+IlJEiBCAqMuyDT9PxiAxl93jd4747eJ28TrqaadadTJyPZ2LvjYrRgNXA2FYQ
FcYsMr/TWAClhfwbLkXiBdAsR+CY5UCrHo9NFGUNV/AJN2Hyaxu7ju5sY5ENHIgc4jUDgcZ4fAJN
tHJQBA8rDtN9N4cqdalAlVBCjUi3Nm2xFJwo4TLqKl8SfEslkejQECLc7z7alG2m2coBaJ9QOioN
vFVVYs2QFVLrfUopCPNo9rLrNHm8cSF2BshPxSYNm/fswa+aOa4rHEVylzDoa9J8CLlrZ70LZi+p
epcXBaI9PmiYJxKcMJS4QF1LrlUotKROJBsxsyfcyJIZYOx9gjEzrxgwenHGeCjgrnmU5Q8xmTwd
NTsCwJN2ISauOkfpIrc7ENHIidyk03m8lyfgJLtFJQD4EjMtyty5kiDaFQdVBiTQe4DBXA5Nwouk
IRREYk8YnE4+UrfuGNQCMsujjo3ZHbykd5HG0M3I3IhjYeysXe2xWTAkkfryuzyBRUVANN6+FqFP
jHnunAJcFwQUUIrAVpDIJraZyhcsiy4R03wg1129dhv59drl8+21FuTuRUQ4tIy9SFpEXHsONJNJ
cWDLynmb8yrluzOG2TdI3ibTT/hpSRIcPgKjKGvWtP079aevpBre7yregv8A3cf7BPq06uK0nwDj
nnL1494jE12smsVL/Tzx8SPaj3Xpx9ZGlfvDjTNbVpCum+FBuxLsRDfPvtivv3G3dOdOLcIlpGWp
8rIiz5/zu1u1yQfcyjmd+U/EPQQN64SNpG0VP4qUkSIkYDWKpBp+n4zAYw97xe8V8dvE7eJ1zM9O
dOZm5Hs7F3wsVmwGrgLCsECuMWF1/prABqovlkz9rwAugXA/DAcqBVj0fmmbnjxvkyK+r+TdiMms
bu57uLFOxHRyIncYzBYJGcX8fk20cmAPzysO0203hyx1qUCVSDKNSDc2bbkUl1F0i6ipfEnjLZZH
tKBIe2+8+2pRtptpKAWivUDomCbRZRWLIIiql1wqQUhH20e1l1ljzaOpC5/sjPxSQNm/fMwa+aWa
wrDEX9TzDoa8jcaQwYL233oW3F9StSosD0T6fdFATiU44SlwiKqTXGpRKMicSHZnZmQOZCmLsDY+
wBaRx4wYPTjjNNRwVzzK8o+YzF4Vmfz+TdiEmrjpG6SK5OQ/RuIHcpNOZzJsiACS7RyWPvwaMx3J
3KmSINoUCVSFJMx7gKFcD03CrhISkkIxJ4xOJx62xjIuC3o3AI0y6OejlicvGRXkdaw3cfceGNmD
Kxd7LFYsCSJ+va8PIk1BUA02r4WsU+NecFMATgFiQUUIKgVpDIJmsxlZsqy4V03wk112ddxr599s
F9e3VHuRuTUY4tIitQlpGXYMMdJNJsR7Hynic8SnlszOHGbdI3ibST55SRkqQ4dAI2XPy0/TvVp/
wLb3eVb0F9eOP7DP606uKzn4D/t9fd4vNdrJrFyvp/pQ9sPdfzI0p94YFbVrCum6FBexLsTC/Pvt
gvr3G3dN9N7cIlpEXqAvIi759zu3u3k/f+UedeUfEPAYN44SN4G0lOPrUpIkRIwEIVPU9GoDGH3e
P3jPjt4yC8TrmZ6dadTNywZWJvdYjMeMWAWFYIFYWqKr/TWAC1RfwTL2vEC4BYj8MByoFWPR+Zg6
/r+S9iEmsbu47uLGORHRuIHcYzBoLGcSACTbRyaPkDq0O0103hqx1oVCVSEKNCDc0bbkUnCjhIuq
qXxJ4yyWR4ASJtvvPtqUba7ZygFop1AaKA28VzUizdEVUmt9SCkI+3j2s2s0fbxxMXP9kJ+KTBs3
79mDXURzWFY4ivqeYdDXgBctbfefbUX1L1Ji4LRPp/0UBOJThjKXKAqpNcakFoyJ1IdmNmJA5kKY
qf7IWCLTOvGA94cXzSzXKZ5lOUfMZi8AsGwZN2ISWuOkfpHrk7D9Goidyks6nMmyfgJLtHJgD8Ej
MdydypkiDaFAtUhSjQe4ChXI9Jwq4TEpJCMSeMTiceAuC4IzAI0x6OejhkcvGQ3idawvcbcaGNh7
Kxd7bFZMCSJ+va9PolFBdf6bV8LVKfGveCmIJwBwIZ5kVQKshkMzC5ZVlQnpvhRrrt67DXz67X77
9uqPcncioxxaRlqhLSIqwH8aSaS4sWPlPE55lPLdkcOMm6JrE2kn6pIyRIcOgAcN/wBMxut6eO/M
KjPmj8i/evAvmpVH/Or6/NP8vH4z5neQ/PX8Hf3le9+y+Jf6L8b7J9qv2D0Kta1hXTfCwvYl2Jhv
n32wX17lbunOm9ukS0jK0+VkRV8/53a3a5IP+ZRxOvKuXD0EDeOEjaZtFT+KlJUiJGAhCp+nozAY
097xu8d8dvE7eJ1zNNOtOpk4YM7F3wsVkwGrALCsIAuLWFQDTaADFhfwbLkViBdAsR+GA5UCrHo9
MzNa21X9fybsRk1j93PdxYx2IaORA7xGYHA41i/ASbaOTAH51WHabaaw9Y40KA6pClGhFubNtiKb
hRdIuoqXxJ8SyWR40QgPbfefbco2z2zlAPRTqA0UBNosqrFm7cVUmuFSikI82j+sussfbxxMXYGy
NgCkwbN++Zg180s1xWOIr6nmHQ15bYxky5e2u8+24tqXqXFwWifT5omCcSnDCUuERVSa51KKRkTm
QbM7MSBxIUhlg7H2CMSOPGDB6ccZ4Zrlc8yvKPMymTyNiwLBk/YhJq46SOkiuD0R0ciJ3KTzibyX
J/H5NtDJY+QBJTHcvcqZIg2hMJVIUm1HOQoZwPTcKOEhCKIngnjE4nHgdwW/GoBGmXRx0bsT15SK
8jrWGbkbjwxsPZWLvZYrNgSSP17Xx5EmoKr/AE2r8WqU+Me+68AXAHggooRWArSGQTQLVk2TCum6
EmuuzruNfPrtevv22o9ydyKhHF5GXqMtIiw9jjpLpJiwY8yrKb+V8t2Zw2zbpm8TaSXqkjJEhw+A
W2MZPn5a/p3614yy8Ft7vKt2C884Y8cgZ9WfVxWc/j/0evP93is02tmsWLfv/qR7Ue6+j62NK/eG
sxkq1rSF9N8KDdiHYiG+ffbFffJG3tOdOLdIFpGWqAtIi79/lu1u1yQe8yjzviUfEPQQJ64SNpm0
VP4iUkSIEYCSpQ6fp6NQCMvu8bvGfHLxPXiedTPTnTmZOWDOxd8LFZsBq4CwrCAri1RcA01gAxYX
yzZci+ALoFwPwwHqgFY9H5otcZOvq/k3YjJrF7ue7ixTkQ0ciJ3GMwSCRnEhH5NtFJo+/Oqw7TbT
aHrG2pUJVQMo1INzRxuQScKLpF1VS+JPGWSyPEqTDibb7z7blG2m2kpBaJ9QGigNvFlVIq3SFVLr
fUopCPNo9rNrLH28eTFT/ZCwBWAJm/fMwa+aWbgVjiK+p5h0Ne5vS3QFi9td6FsxjUvUqLg9Eun3
RME4lOOEpcIiak1zqQWjI3Eg2Z2ZPuJCkLsHY6wRaZ14wYPDrjPDNwVzzKcoeZTJ7UsrBsGTdiMm
rjpI6SK4OxDRyIncpPN5xJsn8fku0UlAPwSUx3L3KmSINmUCVUFJtBzgMGcD03Ci6YhFERiTxicT
j1pnKRcFxRqARpj0cdHDI7eMivI42he5G5ELbD2Ni722IzYEkT1eV4eQKKiq/wBNq+FqlOHjz3Tg
CuB4IKKEFgK0hkM0UuULVlWVCem6FGuuzrsNfPvtcvrgbUW5W5FRDi0jL1IWkRYewx0l0lxYMeZT
zOPK+W7M4bZt0zeBxNPnlNKSJDx0BMnzzWv6d+tPXniC2/3lW9BOecceOY/P606uKznwD9/0e7xa
abWzaLF/p5/qR7Vg5+j6yNK/eGG61reGdN0KDdiPYiG+ffbDfnuFu6dab24RLSMtUBaRFX5Dndrd
nkg/5lHM74k/K70GCeOEjiZpFT+KlJEiJGAqXKUCn6fjECjD7vG7xnx68D14HXM00606mbkezsTf
GxWbAYuBsKwgSw1YZANNYAMWF/Bs+RfAF0C4H4JjlgKsej00LM3Ar+vpN2ISaxu7nu5sY9EdHYic
4jUDgcZxfR+TbRSUA/PLQ7TbTaHrHGpUJVIQm1ItzZtuQScKrpl1VS+JPiWSyPGTECLc7z7blG2W
2cpBaKdQGigNvFVVIq3QFVLrfUolCPNo9rJrLHm8dTF2BshYApIGzfv2YNdRLNcVjiK+p5h0NeAi
5a2u9C2YvqVqVFwOifT7ooBcSnHCUuEBVS65VILRkTiQ7L7MSBzIUhlg7HWCMTOvGDB4ccZ4ZuCu
eZTlHzGZPFNZQLAsCTdiMmrjpI6R65OxHRyIHcpNOJvJsn4CS7RSQA/BozHcrcuZIg2hQNVIQm0H
uAoVwPTXUcJCUkRHBPGJxOPxp87huCMwGMsujjo4YnbykN4nGsM3H3HhbYeysXe2xWQ8kifryvTy
JRQVX2mtfDFSnxj3gpgBXBcEFFCCwFWQyCaEW7LsqE9N8JNddnXaa4vrtevr22o9yNyaiHFpEWqI
tIiw9jjpJpJiwYcynmdcSjlszOHGTdE3gbSS55SSkiQ4dABcM8/LT9O/Wv0+DW93lW9Bvo9PYZ9W
vVxWk+j/AO/nnn3eLTXayaxYr/rI9qPc/wAyNKfeGUq2rSE9N8LC9iXYmF+fXbBffBK3NN9N7dIl
pCXqAtIyz59lu3u38c/8o4nXlHxDwIDeOEjeBpLPj61KSJESMBCFT9PRmAxp93jd4747eMgvE65m
enOnUycsGVi732KyYC1gFh2CAWFqC6+01r8UsL+DZe14gXAHEcngPVAqx6PTQHX9fybsQk1j93Hd
zYx2I6ORA7jGYNBYzi/ASbaOSgH51WHababw5Y60KBKoClGhBuaNtiKThRwkXVVL4k8ZZLI8AJEW
33oW3KNtNtJQC0T6f9EwbaLZqRZsiKqTW6pBKEebx7WXWaPt44mLsDZCwBSYNm/fswa+aWawrHEV
yj4dDXgIuXtrvPtuL6malxcHon0/6JgnEowwlK6AqpNcalFIyJzINmdl5A5kKQuwNkLAFpHHjAe8
OL5pZuCueZX6nzGYvAdgWBJuxCS1x0kdI9cHYho3EDmUmnM5k2b8BJto5LHyAJGY7lblTFIE0KBa
oDFGg9wFCuRyThVwmISSEYk8YnE48BcFwRqAxpj0cdHDI5eMhvI61hm4240MbD2di722MyYEkD1e
16eRKKiq/wBNa/GKlPjXvBTAC4A8EFFCKoBWQyGZhcsmyYV03wk113ddprm+u16+uRtRbk7kVENL
SMtURaRFx4/HSTSXFgx8o854lHwzM4cZN0TWJtJPnlJGSIjx8BB/dp+nfrT/AIFt7vKt2C/u9AM/
rTq5rOfgPT1+n3eKzXa2axct/rI9sPdfzI0rx8ww8K/Kq74fYPePaNq/cfBfzVPFPM7v9/8Anx8y
/Bfd/ifhfFvzHfFvtp7J7l8xfGf4H+PyH+xfsHr1PU/GYDGX3eN3jvTt4nrwOuZpp1pzM3LBnYu9
9ishwxUDYdggVRiguvtNYALVF/BM/a8QLgDwPwwHqgVY9HpoGq+r+T9iMmsbu47uLGOxLRuIHcY1
BILGeH4CS7RSUAQPKw7TbTaHKnWxQJVIMo1ItzZtuRScKrpF1FC+JPGWSyPAxAi2+8+25TtptnKQ
WinUBooBbRZRWLN0RVSa31IKQj7eP6zazR1vHEhdg7H2AKSBs3z9oDcZpZrCscBfKPMNhrwEWL21
3n2zF9StS4uB0S6fdEwTiU4pypwgKqTXKpRaMjcSDZfZg+4kSQywNj7BFpnXjBg9OOFMFHBXPMpy
j5jMXgOwbCk3YfJq56SekeuTsQ0biBxSTTicSbN+Aku0UlAPwSMz3L3KmSAJqUCVUFJtBzgKGcD0
3Ci6YhFERiTxicTjwFv3BG4BGmPRx0cMTt4yK8jrWGbkbkQ1qwZ2JvZYzIeRSPV7Xh9AoqLr/Tav
xixTh49904ArgeCCihBYCrIJBNAuWVZcI6boSa67euw18+u12+vbqi3I3IqMcWkRaoi0iKsWOOkm
kmLBlzKeZzzKsm7M4bZN0jWBtJP1SRkiQ4fASRqzzlWn6d6teefsLb3eXb8F/wC7x/YZ9WnVvWc+
j/p9Pr7tFpptbNouX4+n+pHth7r6PrY0r94ZRW1bwvpvhQXsR7EQvz77Yb79xt3TjTm3SJaRlqgL
SEs+f87tbs5ECHMn868p+IeAgbxwkbTNpKfxUpIkRIwAIdPVBGYBGHveN3jPTt4nrwOupppzpzMn
LFnYu+Fish4xcBYNhAFxawqv9NK/FqivgmXteIFwCxH4YD1QKsej8zAr6ASbsQk1jd3HdzYx2I6O
RA5jGoJBYzi/BSXaOSgH51WHababQ5U40KBKpCFGxBubNtyKS6q6RdVUvgTxlksjwMSKtvvPtqUb
a7aSkFop1AaJg28VVUirdEVUmt9SikI+3j2sus0ebxxMXP8AZCwBaQNm+fswa+aWawvHEXylzDYc
7BFzFtd6NsxfUrUqLg9E+n3RMC5lOGEpcICql1yqUUjInEh2Y2YPuZEkLsLY+wBaZ14wYPDjhRNR
crnmU5R5mUyeSvTXgOwbBk3YhJq46R+keuDkR0ciB3KTTicSfJ/HpLtHJI++BozLcrcqYog2pQHV
YYmzHuAwZyPTXUcJCUURGJPGJxOP0O37gjcCjTHo36OWJy8pDeRxtDNyNx4Y1YMrE3usVkwJonq8
rs+iTVF1/pvXwtYr8W8904ALgfcFFCCwFWQyCaW2MZW7KsmFdN8JNddnXaa4vntdvr22otydyKiH
F5GXqMvIizBhhpJpLgPY+VZTfynlsyNm2bdM3ibTT9U0pIkOHwFZjJ/dr+nfrT1+wtvd5dvQX/u4
+oCf1n1c1nPwHH05enu8Xmu1k0ixf9/o5HtR7r+ZGlfvDIVbVrC+m6FBexHsRDfPvthvvkjbunGn
FuES0jK1AVkRV8Q53a3ZyIPuZR535P8AEPQYN44SNpGklf4qUkSIEYCEKn6gjMBjD7vH7xnx28D1
4nXMz0605mboezsXfGxWY8YuAsKwgK4xYXANNIANWF8s2XIvgC6BcD8MB6oBWPR6aA6+r+T9iEms
bu47uLGOxHRyIHMYzA4HGsXwCS7RSUA/PKw7TbTaHLHGhQJVAMm1ItzRtuRScZrpF1FS+JPGWSyP
ACBFtd59uSjbPbSUAtFOoHRQG2iyqkVboC6l1vqUUhHm0f1l1ljzeOpC7A2RsAUmDZv3zMGvmjmu
KxxF/U8w6GvQRcvbPehbUY1L1Li4PRPp80TBOJThhKXCIqpdcqkFoyJxIdmNmT7mQpjLB2OsIWkd
eMGDw44zwUcFc8ynKHmMyeA7BsGTdh8nrjpI6SK4OxHRyInc5POJvJsn8fk20UkAPwaMx3L3KmSI
NqUB1UGJtB7gMGcD03Ci6QhFERiTxicTj4ZuC4I1AI0y6OOjhicvKQ3idbQzcbciGNmDGxd7bFZM
CaJ+va8PoFFBUA01r4WoV+Lee6cAVwWJBRQisBVkEgmkbXbKsmE9N8JNddnXaa+fXa9fXt1R7kbk
VEOLyMvUZaRlx7DHSTSTFgx5lPnPMoybszhtk3SN4m0k+eUkZIkOHQArOfy1/Tv1pz68QW3u8u3o
N6cY88gJ/WnVxWc+j/7/APa0Wmu1k0ixf/WR7Ye5/mRpX7wyTrD51tWsK6boUF7EuxEL8+u2G+uC
Vu6b6b24SLyIrT5aRFnz/LdvdrJ+/wDKOJzzKfiHgQG8cImsDSSnH1qUkSIkYCN0GnqfjMBjL7vH
7xnx68ZBeJ1zM9OdOpm6Hs7E3usVmwGLAbCsICsLUF1/prABawz4Jl7XiBcAsR+GA5UCrHo/Myiv
oBJuxGTWP3cd3FjHYjo3EDvEYg0FjWL8BJdopNHyB5aHaa6bQ1Y20JhKpCFGpBuaNtyCS6jhIuqq
XxJ4yyWR4AQJtvvPtqUbabaSgFop0/6JAm0WzzizZAVUmt9SCkI63j+s2s0fbx1MXP8AY+wRSYJm
/fMwa6iObgVhgK5R5hsNdgy5e2u9C2ovqVqVFwWinT9okDcSpNOVOEBVSa4VILRkLmQ7MbMSBzIU
hdgbIWCLROvGDB4cXzSzXK55lOUeZlMXgZsCwJL2IyWuOkfpHrk7ENG4gdyk05nMmyfgJLtHJQBA
ElMdydypkiDaFAlUhCjQc4ChXA9Nwq4TEJJCMSeMTiceB3DcMagMZY9HHRwyOXjIbyOtYbuNuNDW
7BlY29tismBJE/Xten0Sioqv9Nq/FqFPjXvBTAC5A4v1FCKoBWQyGaBdsuy4V03wk312ddpr59dr
99e21HuRuRUQ8vIi1QlpCXYD8dJNJcWDDynznynluzNnGbdI1gbST55SSkiQ4dAAPWtP071af8DW
93k27Bf3f2Gf1p1b1rPwHPpzzxzyWi012smsXK/6yPbD3X8yNKfeGBWtawnpuhYXsS7Egvz67YL6
9xt3TbTe3SJeQlqfLSIs+f5bt7t8vn/lPnXlHLh4EBvHCRvA0kpx9alJEiJGAB+U/wASXet/4oea
Xp7v6/nc++eOV3594n6/g+/E57Z7D5z8jfB/s94Z8P4X8tf6/wBh8R/rf2D9K1/AJL2ISaxu7nu4
sY7EdHIgexjMFgkZxfgJNtFJ4+RPLQ7TbTaHLHGpUJVIQo1ItzRtuRScKOEi6ipfEnjLZZHgYkTb
Xefbko2020lAPRPqA0TBtoqqrFm6IqpNbqlFIR5vH9Z9Zo83jiQqwNj7AFJg2b58zBr5pZrCscRX
KPMNhrwMlzNtd59tRfUrUqLg9E+n3RMG4lWOEpWQF1HrlUopGROZFszszIHMiSF2DsfYAxI48YMX
h1xmmouVzzK8o8zGYvLTOUHYNhybsQk9cdJHSPXJyIaNxA5lJ5xOJNmQASXaOSgHwJGY7lblTJEE
0KBqoBkmg5wFCuB6bhRdMQkkJxJ4xOJx5RlBXDcUagMaY9HPRyxO3lIbyPNoZuNuNDGjBnYm9lis
mBJA9XtenkCigqv9Na9FLFPjHvumAFcFwQUVIrAVZBIJpFtcsmy4T03Qoz12ddhr59dr19e3VHuT
uRUY0tIy1RFpEWYD+NJdJcWDDynznmU8t2Zw4ybom8DaSfPKSUkRHDoAUc5Vp+ngrX/ga3e8q3oN
9HHPj9gVp1cVnYEf5/f9JeLTXa2bRYvzx6+jke2Huv5kaV+8MHWtbQrpvhIbsS7Eg3F9dsF98kbd
0403t0iVkJWoC0iLEH+W7W7PJB/5RxOvKOXD0GDeOETaZpNTjhRKSJECMBCBT9PxmARl93jd4747
eR68TrmaadaczJ0wZWJvfYrIeLVj9hWEBXGKjIBprARaov4JnyLxAuQWI/DAeqBVj0fmhNmq/r+T
9iElsbu47uLGPRHRuIHcYzBIJGeCACTbRyWPvzysO0203hypxqUCVSEKNSDc0bbkUl1HCRdVUviT
xlssjxSECLa70LblG2m2kmB6J9P+iYNtFlFIq3QFVJrdUopCOto/rNrPH20dSFT/AGRn4pIGzfvm
YNdRLNcVhiL5R5h0NeAFy9td59tRfUvUqLA9E+n7RQE5lOCcqcICql1yqQUjInUg2X2YkDiRJip/
sfYItE48HsHhxfNLNcrnmU5R8xmLwh2BYMm7D5PXHSR0j1ydiGjcROZSaczmTZv4/JdopMBfg0Zh
uXuVMkgTMoEqcKSZj3AYK4HpLquExCSQnEnjE4nHincFwRqARpl0b9HDI7eUivE61hm4+40MbD2V
ib22KyYEkZBX1eH0SiouvtNq+GKlPjHvBTECuCxIKKEVgKshkE0C5ZNlwjpuhJrrt67TOV9dr19c
Dqj3I3IqMYWkReoy0kLMGGOkmk+LAf5TzOfKfh2Rw4zbpGsTaSfqmjJEhw6AGYutW+M61/Tv1r/3
vBbf7yrdg3P+Hj1j8/rTq4rOfgPTnnnn+7xWabWzaLFfp5/qR7Ye6+j6yNK/eGaFa1tCem6FhexD
sRC/Pvtivz3G3NONOLcJFpIWqAtISz9/lu3uzk/f8yfznyn4h6CBvHCRtM2ir/FSkiREjAbbGMod
PU9GoBGH3eP3jvjt4HrxOupnp1pzM3LBlYu+Nis2A1cDYVhAVxiwuAaawEYqL5ZsuRfAF0D4Hp4D
1QKsej00WYyK/r+TdiEmsXu57ubFOxHRyIneIzA4JGMX4CS7RSYAQPKw7TbTaHqnGpQJVIQo1Itz
ZtuRScKLpF1FS+JPGWSyPEMQItrvPtyUbZ7aSkFop1A6KA20VVUizZAVUmt9SikI82j+sussebxx
MXYGyFgCkwbN+/Zg180s1xWOIr6nmHQ16GS5a2e8+2oxqXqXFwWifT5omBcSnHCVOEBVSa5VKMSk
S8g2X2ZPuJCkMsHY6wRaZx4wYPTjjPBRwVzzKcoeYzJ6DsCwZN2Iyet+kjpIrg5EdG4idzk84nEl
zIR+S7RSYA/BozHcvcuZIAmhQHVIYk0HuAwZwPTcKLpCEURGJPGJROPgXBcEZgEaZdG/RwyO3lIr
yOtYZuRuRDGw9lYu9tismBJI/Xlen0Caoqv9N6/FKlPjHnJTgEuB4IKKEVgKshkE0C5ZVlQnpwhB
rru67Tfz67Xr69vqLcjcmohpaRlqjKyMuwYYaSaS4sGWUp5nHMpybMjhxm3SN4nE0+eUkZIkOHQC
dLayLnmtP079a+vPEFt7vLt6C8+nHHsFgVp1cVlPo/8Av59OS8Xmu1s1ixfn6f6ke2Huf5kaV+8M
0Va1rC+m6FBuxLsRC8332wX1wRt3TjTe3CJaRFqgLSIs/f8AO7e7XL995RxOvKfr3oIG8cIm0zaS
n8RKSJESMADn6gp6M1/GX3eN3jPjt4n7yOOZnpzp1MnQ9nYu99is2AxYBYVggHAtYZX2msAFrC/g
2fteIJwCxHYYD1QSsfj80mt+EjSGK/r+S9iEmsbu47ubGORDRuIHcYzBoLGcX4CTbRyaPkDq0O01
03hyp1oVCVSFKNCDc0bbkUnCjhIuqqXxJ4yyWR2qBIm2+8+2pTtptnKAWifUBomCbxXNSLN0BVSa
31IKQjzePazazR9tHUxc/wBkLAFJgmb9+zBrKJZriscBXKPMOhrwEXL233n21F9S9SYuC0T6f9Ew
TiU4YylwiKqXXGpBaMidSHZjZeQOZCkKsDZCwRaZ14wYPDi+aWa5TPMpyj5jMXgOwLBkvYhJq46R
+keuTsP0biJ3KTTqcybJ+Ak20clAPwSMy3K3KmKINoUC1SEKNBzgKFcj0nCjhMQkkIxJ4xOJx4C4
LgjMAjTHo56OWJ28ZDeJ1rDNxtxoY1YM7F3tsVkwJIn69r08gUVF1/ptX4tUp8a94KYgXAHEhnmR
VALSGQzMLllWVCum+FGuu3rsNfPrtfvr26o9ydyKiHFpGWqEtIirAfxpJpLixY+U8TnynluzOHGT
dE1iaST9UkZIkOHQADn5afp3q0/4Ft7vKt2C8fR/YZ/WnVxWk/Ac/v8AX3eLzXayaxcr/rI9sPdf
zI0p94YFbVrCum6FBexLsTC/Prtgvr3G3dN9N7cIlpEXqAvIi759zu3u3k/f8Sjzryj694DBvHCR
vA0kpx9alJEiJGAhCp+no1AIy+7x+8Z8dvCQXidczPTrTqZuWDKxN7rFZjxqwCwrCALDFRdfaa1+
LVF/Bsva8QDgFiPwwHqgVY9H5mEL/qFu5f8A8UPqsfZ/nn+O73z5b2h4D8lP+T/5Je2eX+Dfga85
+zvp8P7v8y/6/wAk8u/rf2mUfSx0YkVbXefbUo2020lAPRTqA0TBNosopFm6AqpNcKkFIR9tH9Zt
ZY82jqQqwdkLAFJA2b5+zBrqJZriscRXKPMOhrzVM5QRcvbXehbMX1L1Li4LRLp+0TBOJThhKnCI
qpNcalFIyJzIdmNmZA5kSQywdkLAFpHXjAe8OL5p5uCueZTlHzGYvCXpXVmfz6TdiMmrfpH6R65O
xDRuIHcpNOJxJsn4CS7RyWPvwSMy3K3KmKINoVB1SFKNBzgKFcjk3GbhMQkkIxJ4xOJx5ZjIuG4I
1AYyy6OejdidvKQ3idawzcfcaGtmDOxt7bFZMCSR+va9PolFRUA02r4WqU+Ne+6YgXAHF+ooRUAq
yGQTQi5ZVkwrpuhRrrt67DXF9dr99e3VHuRuTUg0vIi1QlpCWYD+NJNJeGDDyjmc8Sr4dkcNsm6R
rA2knzyklJEhw6AR0PnmtP08Facf7i273lW7BuPo48fn1adXFZz8D/8A7eLTXa2aRYv6fudMGo91
x/8A3I0p94QFa1pCum6FhuxLsSDfPvtgvvgjbunGm9ukC0iK1AWkRZ8+y3b3a5IP+ZR515R8Q8CB
HjhE1gaSU44VSkiREjAQh0/T8ZgMZfd43eM9OXgfvA67menWnUycsGdi73WKyHjFgNh2EBXGKi6+
01r8WsL+CZ+14gXILEfhgOVBKx6PzMFX9fSbsQk1j93HdxYxyIaNxA7jGYLBozi/ASbaKTR8gdVh
+mum0OWPNSgSqQhNsRbmjbcik4VcJF1VS+JPGWyyPEnWDECbb7z7blG2m2kpBaJ9P+igJtF1M4s2
QFVLrfUglCPNo9rNrNH28dSGT/Y+wBKYNm+fMwS6iWa4rDEVyjzDoa8tJlBFy1td59txfUrUqLgt
E+n7RIC5lOGEqcIiql1wqQWjI3Uh2Z2YkDiQpC7A2QsAWmdeMGDw4vklm4K55leUfMZi8UZQdg2B
JuxCSVx0j9JFcHIjo5EDuUmnM5k2RABJdo5NH34JGY7k7lzJEE0KBapBlGg9yFCuR6ThVwkJSSEY
k8YnE49F2jRq37fjUAjTLo56OGBy8pDeR1rDNx9xoa1YMbE3tsZmPJpH69r08iTVGV7ptX4xcpw9
e+6YAXAHF+ooQVAqyGQzQq5ZFlwnpvhJnrt67jPN9dr19cjai3J3JqIcXkRaoi0iKDx+OkmkuLBj
zKeJxzKsm7I2cZt0TWBxJPnlJGSJDh0AtM5Qf3afp3q0+jiC293k27Bf+7jx4/Pqz6t6znwD6fXn
jgtFprtZNYsW5/8AxI5swc+nH1kaV+8OLr8jrWtYT03QoL2JdiQXi++2K+uSNu6cacW6RLSMtUBa
Rl3z/Ldvdvkg/wCZP5z5Ry4eggbxwkbTNpKfxEpKkRIwEqHT1QReARp93j94z49eB68jjqaadadT
JyPaWJvhYrFgMWA2HYQBYYqLr/TWAi1RfwTLkXwBcguB+CY5UCrHo/NAdfV/JuxCTWN3c93FjHIj
o5EDuMYgsDjOJCPybaKSgCB1aHabaaw5Y41KA6pCFGhBubNtyKThVwkXVVL4E8ZZLI8RoQItvvPt
uUbabaSgHon0/wCiYJtFlVYq2QF1JrfUopvH20e1k1kj7eOpCrA2RsAWkDZv37MGvmlmuKwxFco8
w6GvUTbOMkXLWz3n21GNS9SowC0T6fNEwTiVYYSpwgKqTXKpRiMiXkGy+zJ9xIUhlg7H2ELTOvGD
B4dcZ4ZuCueZTlHzKZPagsCwZN2IyeuOkjpIrc7EdHIiezk04m8m5fx+S7RSWPvwSUx3L3LmKINo
UB1SFJNB7gMGcD03Ci6QhFERiTxicTj4O37gjMAjTLo36OGR28pFeR1tDNyNyIY2HsrF3tsVkwJI
n69r08gUVFV/ptX4tQp8Y95KcAVwOJBRQisBVkMgmgW7LsmE9N0INddvXaa+fXa7fXt1R7kbk1CO
LyMvUZaRF2DHHSTSTAey5lPnHMq5bszhtk3SN4m0kvVJGSJDx8BAy4rT9O9Wnrz4Nb3eXb0G55xx
49gn9adXFZT6P8fTlzx7tF5rtbNYsX/f/Uj2o9z/ADI0p94bW/AK1rSE9NsKDdiXYiG+ffbDffuN
u6cacW4QLSItT5WRFnz7Ldrdrl+/ylHE64lHK7wEDeOEjaZtJX+KlJEiBCAxvdz9QVBGYBGX3eL3
jPjt4n7xOuppp1p1MnLBlYm99iM2AxYBYVhAFhiouvtNYALWF/Bs/a8QLgFiPwwHKgVY9H5oJmmY
BX8m7EZLY/dv3cWMdiGjcPO4xmCQWM4vwEn2ikwB+eWhumum0PWOtCoSqQZRqRbmzbcikuo4SLqq
l8SeMslketJlBCBFt96FtynbPbSUgtE+oDRMG3iqmcVbpC6l1vqQUhHm0f1n1mj7eOJi5/sjYApM
Ezfv2YNdRLNcVhgK5R5h0NeRoi5a2+8+2oxqXqVFgWifT/okCcSlPGUroCqk1xqUWjInUg2Y2YkD
mRJCrA2QsEWmceMB7w4vmlmuUzzK8o+YzF4BYFgSXsRktcdI/SPXJ2H6NxA5lJZzOZNk/ASXaOSg
CAJGY7lblTJIG0KA6pCFGg9yGCuB6blRwmISSEYk8YnE48BcFvxqARlj0c9HDE5eMhvE62hm4240
MasGVi722KxYEkJBXten0CaoqvtNq+GKlPjXvBTEC4A4kM1CKgFaQyGZhdsqyYT03Qo112ddpvi+
+1++vbqj3I3JqMcXkZaoS0iLMB+OkmkuI9jzKeJzxKeWzI4cZN0TWJtJLnlJGSJDh0AB88Vn+nfr
P145g1vd5VvQbj0459gn9adXNZz8B/6+S8Wmu1k1ixb0/wBJHth7n+ZGlPvDArWtIX03wsN2I9iQ
b59dsF98Ebc0303t0iWkRWoC8iLPX+W7e7XL5/5RxOvKOV3gQG8cJG8DSSnH1qUkSIkYCEGn6djM
AjL7vH7xn5y8j94nHU00507mblgysPe+xGTAWqBsKwQCwxQZX2mtfi1hfwTL2vEC4BYj8MB6oFWP
R+ZgV/Xsl7EZNY/dx3cWMdiOjcQO4xmCwWNYvwEm2kkwAgdWh2mum8OVOtCgSqQhRoRbmjbcikuo
4SLqql8SeMslkdC3/wBSoe8g95/L+1S9u85+VniniNa+P/la/LPwP8sz4j5LeUeCeUfbT3/3L2by
b+B4t4//AGL9s4z9KEcsVtzvPtqL6l6lRcFon0/aJg3EpTTlLhAXUuuNSi0ZG4kOzOzMgcSFIXYO
x9gikzjxgPeHHGaebgrnmU5R8xmLzdsYyLAn8m7EZNXPSR0kVyciGjkQOZSacTiTZPwEm2jksffg
kphuVuVMkQTQoEqkIUaDnIUI5HpuM3CYhFIRiTxicTjyzGRcFwRmAxpl0c9HLI7eMhvE61hm4+40
MbD2Vib3WMyYEkD1e14eblFRdf6a18MVKfGPeSvAFwBxIZ5kVQC0hkMzja3ZVlQrpvhJvrs67TfF
9dr19e21HuRuTUQ0vIi1RFpGXHj8dJNJsWDLynic+U8t2Zw4zbom8DaSXPKSUkSHDoBXM/Wtf071
aenpBre7y7egvH/5BPq06uKzn4Dj/wC9/i5Lxea7WzWLlvp9PiR7Vg5/mRpT7w1NZQK1rWFdN0KD
diPYkF+ffbDffBC3NN9N7eIFpEVqArIyz9/zu1u1yQfcyjideUcuHgMG8cJGsDSanH1iUkSIkYAo
yhCp+n4zAYw+7x+8Z+dvE/eJ11NNOdOpk5HsrF3vsVkPGLALCsEAqNVF1/prABawv4Jn7XiCcAuB
yeA9UCrHo/M1GUFX1fyXsQk1jd3HdxYx2IaNxE7hGILBoxi/ASXaOTR8geWh2mum0OWONigSqQZR
qQbmjbcikuo4SLqql8SeMtlkeUZQYgTbfedbco2020lAPRTp/wBFAbeLZZxZuiKqXXCpBSEdQj2s
2ssebx5MVYOyNgCkgTN8/ZA11Es1xWGIrlHmHQ14oygi5a2+8+2ovqXqXFwWifT7omCcSpNOUroC
6k1xqUWhI3Uh2Y2YkDmQpDJ/shYAtI69YD3hxfNLNwVzzKco+YzF4ZFgWBJexCTVx0j9JFcnYho3
EDuUonM5k2RCPyXaKSx9+CRmO5W5cyRBNSgSqQpRoPcBgrkek4VcJCEUhGJPGJxOPB9LguGNQGNM
ujno3ZHbykN4nGsL3F3GhbZgysXe2xGTAmkfr2vD6BNQVANN6/GKFOHjzgpiBcAcSCihBYCrIZDN
I6Llk2RCOm+FGeu3rsM8312v317bUe5G5FRji0jLVCWkRZgPx0k0kwYMOZR5z5Rk2ZnDbNukbxNp
JeqSMkSHDoAD5xrb9PBWmP8AuLb3eVb8F59OPUBPq06uK0n0f+n159S8Ym21k0ipj6ef6oc1YOvo
+tjSn3h22MZFbVpCum6FhuxDsRD/AD77YL74I25pvpxbpEtIi1QFpEWev8t2t2uXz/mU+d+UcuHg
MG8cJG0zaanoqlJEiJCAxq/ihU9T8XgMafd43eQQO3ifvA46menOnMycsGdi73WKyYDFgNhWCAXG
KjK/01r8WqL+CZe14AXILgfhgOWAqx6PzQX8RX9fSXsQk1jd3HdxY5yI6NxA7xGYLBI1i+ASXaKS
gCB1aHababQ5Y40KA6pCFWhBubNtiKS6jhIuqqXxJ4yyWR4oECLb7z7ak+2e2coBaJ9QGiYJtFVF
Is3QFVHrfUgpCPNo9rNrNH20dSF2BsfYApMGzfPmYNdRLNYVhgK5R5hsNdk2jQyxe2+8+2ozqXqX
GAeifT7omBcSjHCUuEBdSa41ILRkTqQ7MbMn3EhSF2DsfYItI48YMHhxxnhm4K55lOUfMZi8k303
U7AsGS9h8krjpJ6SK5ORHRyIneZNN5xJsn8fku0clAPwSUy3L3KmSINqUCVUGJNB7gMGcD011F0h
KKIjEnjE4nH9OZ3BcEagEaY9HPRwyPXlI7xONYZuRuRDGzBlYu9tismBJE/XleHkCioqv9Nq+GKl
OHj33TgC4A8EFFCKwFWQyGaBcsqyoT03Qg1129dxr59dr19+3VHuTuRUY4vIy1Rl5GWHsMdJNJMW
DHmU+c8yn4ZkcNsm6JvE2mnzykjJEhw6AA/u1/Tv1rxzz4Nb3eXb0G55xx45AT+s+ris5/H+Ppy/
zeLTXa2aRUv9PPo5HtR7r+ZGlfvDBVrW0L6boUG7EexIN8++2C++CNu6cac24SLyMtT5aRFn7/nd
rdnkg/5lHnflHxD0GDeOEjaZtNX+IlJESJGAlq0KoKfjMAjD7vG7xnx28Tt3nHM00605mblgzsXe
+xWbAasBsKwgK4xYXX+mtfi1hfwbP2vEC4A4j8MByoFWPR+aRY00fOAV7JuxGT2N3c93FjHIho3E
TuMZgsEjOL8BJtopNHyB5aHaa6bQ9Y60KBKpCFGpBuaNtyKS6jhIuqqXxJ4yyWR6kxYEh7b70Lal
G2m2coBaKdQGiYJvFlc4s3QFVLrfUgpCPN49rNrNHm0cTFT/AGQn4pMEzfv2YNdRJRcVjiK5S5h0
NeEqd2S5e2+9C24tqVqXGAWifT7okCcypPCUuEBVR641IKRkTmQbM7MSBxIUxlgbH2CKTOvGA94c
XzSzXKZ5lOUfMZi8jUTYsCwJN2ISauOkfpHrg5ENG4gczk06nEmyfgZLtFJgD8ElMtyty5iiCaFA
dUBCbQc5ChXA9Nwq4TEJJCMSeMTiceKdwW/GYDGmPRx0cMTl5SG8TjWF7j7jQxswZWLvdYjIeSQk
FeV6fRKKCq+01r4YqU4evOCmAFwAxIZqEFAKshkMzC5ZVlQjpvhRrrs67TPF99r98+3VFuTuTUY0
rIy1QlpGVHsMdJNJcWDDynznynlszOHGTdI3gbTT9UkpIkOHQAl/EcfLT9O9Wv0+D293lW9BfX6O
QFgVp1b1nP4/6fT6e7xabbWTWLlv9ZHtx7n6PrIyr94hTrWtYV03QsN2JdiIX599sF98Ebd0403t
wkWkRWny8iKv33O7W7XL9/5RxOuZTw4eAwbxwkbwNoqcfWpSRIiRgIQqep+MwGMvu8bvHfHbxP3i
ddTPTnTqaOWDKxd7rEZsBywGwrBAOBiouvtNYALVF/BsvbMALgFiPwwHqgVY9H5mCr6v5N2ISax+
7nu5sY7ENG4gdxjUFgsZxfgJLtHJQBA4rDtNdNocscalAlUhCjMg3NG25FJwo4SLqql8SeMslkdB
iBFt951tyjbTbSUAtFOoDRME2iuakWbICql1wqQUhHW0f1m1ljzeOpjJ/shYIpMEzfv2QNdRHNcV
hgK5R5h0NeB6t+c31Jevjn5W3Pyt/wCRjwP3E16/lJenzU+K+M+efjv45fxi/eT7v7L8b6f0vmnv
f2q/YPLbAsCTdiElrnpI6SK4OxDRyJHcpLOJzJsn8fku0ckj78ElMdyty5kkDaFAlUhSbQe5DBHA
5Nyq4SEIpCMSeMTiceBXBb8ZgEaZdG/RwxO3lIbxOtYZuPuPDGo9lYu9tiMmBJE/Xlen0CaouvtN
q/GKlOHjzgpgBcAsX6ihBYCrIZBNAu2VZUJ6b4Sa67uu038+u16+uRtR7k7kVGOLSMvURaRlh7DH
STSTEex8o4nPlHLdkcNs26JrE2kl6pIyRIcOgALnGtf079afTxBbe7ybegvrxx6gJ/WnVxWc/j/P
PPP7y8Wmu1k1ixb/AFke2Huv5kaV+8O2xjJVrWsL6bYUG7EuxEL8/O2C+vcrc0404twiXkRanysi
Lvn/ADu3u18c/wDKeJ35T9e8Bg3jhI3gbSU/ipSRIiQgIqUGn6ejMAjD7vG7xnx28pBeJ11NNOtO
Zm5YM7F3vsRkwGLgLCsEAsMVF1/prX4tYX8Ez9rxAuAWI/DAeqCVj0fmZCr6v5N2Iyaxu7ju4sY5
EdGogdxjMFg0Zxfx+TbRyaPkDq0O0102hyxxoUCVSDKNCDc0bbEUl1HCRdVUviTxlssjwAgRbfeh
bko2z20lAPRTqA0TBN4spnFmyAqpNbqkFIR5tHtZtZ4+2jqYqf7IT8UmCZvnzMGuolmuKwxFco8w
6GvAC5a2u8+2ovqVqXFweifT9ooCcSrBOUuEBVTa41KKQkTiQbMbMn3EiTF2FsfYItM48YD3hxxm
lmuUzzKco+YzF4DsCwJL2ISWuOkfpIrk7EdG4gcyk05nEnyIR6S7RyWPvwSUx3K3LmSIJqUCVQFK
NBzgKFcj0nCjhMQikJxJ4xOJx4HcFvxqARpl0c9HDI7eMgvE61hm4+48MbMWVi722KzYEkT1eV4e
RJqi6+01r8YqU+Me8FMQK4HF+ooRWAqyGQzRa4yvWVZkK6cIUa67euw1zfXa9fXt1R7k7j1COLyM
tUZaRFmDDHSXSXhgx8qynXlOTZmcOM26ZvE2klzykjJEhw6ARq/i192n6d6s/X7DW93lW/BeeePT
x+f1n1b1nPwHP7vpLxaabWzWLFvTnn0cj2w91/MjSn3hlOta1hXTdCgvYj2IheL77Yr89xtzTnTi
3SJeRlqhLSIs/f8AO7W7ORF/zKOZ1jKfiHgIG9cJmkzaKn8VKSpESMBCFT9PRiBRh93kd5D41eJ6
8TrmaadadTRyPZWNvhYrJgMWAWFYYBcWsLgGmsAFrC/g2fIvgC4BYj8MB6oFWPR+aE2Ov6/kvYjJ
rG7ue7ixTkR0ciB3GMwSCRnghH5NtFJgBA6tDtNdNoaqdaEwlUhCbUi3Nmm5BJdVwkXUUL4k8ZbL
I8VkSItzvOtuU7a7aygFon0/6KAm8XVUirdEVUmt9SCko83j2s2s0ebx1IXP9kZ+JTBM3z5mDXzS
zWFYYC+UeYbDXgIuWtrvRtqMal6lxcFon0/aJgnEpxxlK6IqpdcqlFoyJxIdmNmZA5kSQuwNkLBF
JnXjAe8OOFE1HBXPMpyj5jMXgasCwJP2ISat+knpHrg7ENHIid5k04nMnyfx+S7RySPkASUx3L3J
mSIJoUC1SEKNRzgKGcD011HCYhFERwTxicTjwFvW/GoBGmPRx0cMTl5SK8jrWGbj7kQtswY2LvdY
jJgSSPV7Xp5EoqKgGm9fi1Cnxj3gpwBXA4kFFCCwFWQSCaViqXLKsqE9N8JNddvXaa4vvtdvr26o
9yNyKiHF5GWqIvIiw9hjpLpLiwY+U+ccynluyOG2TdE3gbSS9UkZIkOHwAjX3a/p36145+w1vd5V
vQbn/DjxyAn9Z9XFZz+P8fTz/m8Xmu1k0ixf9/o5HtWDn+ZGlPvDArWtIV03woN2JdiIbm/O2G+/
cbc0405t0iWkZaoC0iKvn/O7W7PJB9zKPOvKOXD0GDeOEjaZtFT+KlJEiJGAhCp6n4zAYw97x+8d
8dvE9eJ11NNOtOpk5YM7G3wsZmwGLAbCsIAsMWF1/prABawv4NlyL4AugWI7DAeqBVj8fmkbjYq+
r+TdiElsbu37uLGOxHRyIneIzBIJGcX4CTbRSWPkDy0P0103hyxxoUCVSEJtSDc2abkUnCi6RdVU
viTxlssjxWRAm2u9C25PtptpKAeinT/ooCbRVRSLN0BVS64VIKQjzeP6y6yx5vHUxdgbIWAKTBs3
75mDXUSzWFYYCuUeYdDXgZMF7a70Lbi+pepUWB6J9P2iYJxKccJSuiKqTXGpRSMicyHZnZmQOJCk
LsDY6wBaZx4wYOzi+aWa5TPMpyj5jMXlZufmVg2DJew+S1x0kdI9cHIho3EDmUlnE3k/JCPyTaOS
gSAJKY7l7lTJEG1KA6pCFGg9wFCOB6bhVwmIRSEYk8YnE48SJorguKNQGNMejno5YHrwkV4HW0M3
H3HhbUeysbe2xWY8kker2vT6BRUXX+m1fC1inD177piCcAcSGeZFUCtIZDM1LlC7ZVlwnpuhRrrt
67DPN9dr19e21FuRuTUQ4vIi1QlpEWHj8NJNJMWDHmU+ceT8t2Rw4zbomsDaSXqmlJEhw6AKMoLj
Ktf071a8cekFt3vJt2DfR/kE+rTq4rSfgPo5449S8Wmm1c0ixb0/0oe2Hufo+sjKn3hqMoaratoV
03QoL2JdiQX599sF98kLd0303t0iWkZaoC0iLP3/ADu1u1kQIcynznyj696DBvHCRtM0mr6KJSRI
iRgKjKECnqfjMBjL7vH7xnx28T94nXU00406mbhgzsXe6xGTAYrH7CsECuLWF1/prX4pUX8Gy9rx
AuAXA9PAeqBVj0emijKDr+v5L2Hyaxu7ju4sY7ENG4ge4jMFgsYxfx+TbRyaPkDqsO0102hypxoV
CVSDKNSLc0bbkUl1HCRdVUviTxlksjqjKAIE213n23KNtNs5OD0U6gNEwbeLKZxZsgKqTW+pBSEe
bx7WfWePt46mKn+yE/Epgmb58zBrqJZriscBXKPMOhryUZQC5e2u9C2ovqVqVFweifT9omCcSnBO
UuEBVSa41MLRkK8h2Y2YkDiQpDLB2PsAWkdeMB7044zSUcFc8ynKPmMyeWjKHq/zU/TKf7o+BXn4
N/h/L38g+Fc+f/JT/wAbvzfPr/kJ7x89PxFfZj2fyH3b5Z/wfDvbPsT+yjKHl1w29GYDGmPRz0cM
Dt5SG8TjWGbkbjwxswZWLvbYjNgTRP15Xp5ImqKr/TavhihT4x5wUxALgcX6ihBYCrIZDM40u2VZ
UJ6boQa67euwz8+u16+fbqj3I3JqMeVkRaoi0iLj2HGkmkuI9hzKfOfKOW7I4cZN0jWJtJL1SRki
Q4dAAXPy1/TwVrx6+C293k29BeecceOQE/rTq3rOfgP+3092i012tmsWK+nr/Uj2o91/MjSn3hgV
tW0L6boSF7EexEN8++2G+uSNuab6cW4RLSMrUBaQlnz/AC3a3a5fPvKOJ15R9e8BA3jhE2mbSU/i
pSRIiQgIc/T1QRmAxl93j94z07eJ+8TrmZ6dadTJyPZ2JvfYjIeMXAWFYQFcYsLr/TWAC1hfwTL2
vEC4BYj8MByoJWPR+aWmcu5V9X8m7EZNY/dx3cWMdiOjcRPYxmDQaM4vgEm2jkwB+dVh2mumsPWO
NCoSqQhRoQbmjbcik4UcJF1VS+JPGWyyPGTECbb7z7blG2e2koBaKdQGiYJtFVFIs2QFVJrdUgpC
PNo9rLrPH28dTFT/AGQsAUmCZvn7MGuolmuKwxFco8w6GvARcrbXefbUW1L1LjALRPp+0TBuJThh
KnCAqpdcqlFoyJzIdmNmZA4kKQuwNj7AFpnXjBg7OL5pZrlM8ynKPmMxeAWBYMm7EZLXHSR0kVwc
iOjcROZSacTiTZP4/Jdo5MBfgkpjuVuVMkATUmEqgIUaj3IUK4HJuFHCYhFIRiTxicTjwFwXBGoB
GmXR10csTt4yK8jrWGbjbjQxuwZ2LvbYjJgRRP17Xh5AmqLr7TWvxipT4x57piBcgcX6ihFYCrIZ
DNA6CybKhHTfCjXXd12mvn12vX1yOqPcjcioh5eRlqiLSMswY/gk0kxHseZT5zzKOWzI4cZN0jWB
tJLnlJGSJDh0AjofrWn6d+s+OefBre7yregv/d49Y/Pq06uKzn0f9fX/AGvFprtbNYqW/f8A1I9s
PdfR9ZGlfvDI1WtawvpuhQbsS7EQ3N99sF9+4W5pxpzbhErIC1QF5GWfP+d2d2ciD/mVedeUfXvQ
QN25SNJm0lP4qUkSIkICVCp+n41AYw/7xu8Z6cvE9eJ1zM9OtOpm5Ys7F3wsRmwGLALCsICuLVGQ
DTWADFRfwbP2vgC6BcD8MByoFWPR+aAq/r6TdiElsbu47ubHORHRyIncY1BILGsXwCTbRyUAQOqw
3TXTeHrG2pUJVIMo2INzRtuQScKLpF1VS+JPGWSyPEnWAIEW13n21KNtNtJQD0U6gdEwbaKqKRVu
gKqTW+pBSEdbR7WXWWPt46mLsDZGwBSQNm/fMwa+aOa4rDEX9TzDoa8KRctbXefbUX1M1Li4LRLp
+0TBOJThjKXCAqpdcKlGIyJxINmdmJA5kSQuwNj7BFJHXjBg8OL5p5uCueZXlHzGYvALBsGTdiEl
rjpI6R65OxHRyIncpNOZxJuX8fk20Ulj78GjMdyty5kiDaFA1UgyrMe5ChXI9Jzm4SEopCMSeMTi
ceAuC4IzAI0x6OejhidvKQ3kdawzcjceGNmDGxN7bFZMCaJ2va9PIlFBdf6bV+MVKfGPfdMQK4LE
gooQWAqyCQzQL1lWVCem+EmeuzrtM/Pntevn22otydyKjGlZGXqMtISzBhjpJpJwPH+U5Tjynlsy
OHGbdM3icTT55SRkiQ4dACTrA9a1/Tv1rxzz4Pb3eXb0F554x9QE/rTq5rSfgPT/AL3+bxea7WzW
LFv3+jke1Huf5kaV+8OsHWtawvpuhQXsQ7Eg3z77Yb75I27pxpxbpEtIy1QFpEWfP+d2d2eSD7mU
ed+U8rvQYJ44SNpG0VP4iUkSIkYCEKn6gjMBjD7vG7xnx28T14nXMz0505mboezsXfCxWbAYuAsK
wQKwxYXX+msAFrC/g2fIvEC6BcD8MB6oFWPR6aAQCASbsQk1jd3PdxYx2IaOw85jGYJBIzg/ASXa
KTACB5aHababw9Y61Kg6qCE2pFubNtyKS6q6RdVQviTxlktjxq/0ISJtrvQtuUbabZygFop1AaKA
m8WVUirZAVUmuFSiko83j+s+ssebR1MXYGyE/FJA2b58zBr5pZrCscBXKPMNhryNMly9td59sxfU
vUqMAdE+n3RQG4lOKUpcICqk1yqUUlInEh2Y2YkDmQpC7B2OsAYkdeMGD044zTzcFc8yvKPmMxeA
WBYEl7EJLXPSR0j1wciOjkRO5SecTiTZP4/JdopNHyAJGYblblzFEG0KA6pClGg9yGCuR6bhVwmI
RSEYk8YnE48HzuG4I1Ao0x6OejdicvKQ3kdawzcfcWFtWDOxd7bFZsCSJ+va9PoFFRVf6bV+MVKf
GPeCmIFwCxfqKEVgKsgkM0rmu2XZUK6b4Ua67eu0zxffa9fft1Rbk7k1EOLyItUJaRFR7DHSTSXE
ex5lXnPlPLdmcOM26JrE2klzykjJEhw6AAetafp360/4Ft3vKt6DfRxzwAn9Z9XFZz+P/wDb9JeL
TXa2axcr/rI9sPdfzI0p94YFa1pCum2FhuxLsSC/Prtgvvgjbmm+nFuES8iK1AVkZZ++y3a3a5fv
vKOJ15T9e8Bg3jhI2maSU44VSkiREjAQhU/T8YgMZfd4/eM+OXifvE65mmnWnUycsGdi732KzYDF
gNhWEAVGKC6+01r8WsL+CZe14gXALEdhgOVBKx+PzQFX1fSXsQk9j92/dxYxyIaORA7jGYLBY1i/
ASbaOTR8geWh2m2m0OWOtCgSqQZRqRbmzbYik4UcJF1VS+JPGWSyPAw4i2+8+25RtptrKAeinUBo
mCbxZTOKt0BVSa3VIKbx5tHtZdZ4+3jiQqf7IT8UmCZvn7MGuolmuKwxFfU8w6GvADBW2u8+2ovq
XqVFwWiXT9ooDcSnDCUuERVS641KLRkTqQ7MbMSBxIUxdgbH2ALTOvGA94cXzSzXKZ5lOUfMZi8B
2BYEm7EJLXHSR0j1yciGjcQO5SacziTZEI/Jdo5KAIAkZjuVuTMUQbQoEqgKSaD3AUK4HpuFHCQh
JIRiTxicTjweq/l+/p//APc78xv6f/L++av1Eb/51/T57/mN+2/PX/kZ9PuY+r8a8U/07yf/AI7/
AGDjLKsmE9N8KNddvXYa4vrtevrkbUe5O5FRjSskL1EXkZZiwx0k0lxHj+ZT5xzKMmzM4cZN0TeJ
tJPnlJGSJDh8AjoPu1/Tv1pxz9hre7ybegvrxx6gLArXq4rSfgP/AOr14LRabbWTSLGP9ZHth7n+
ZGlPvDDNbVtCum2EhuxLsSDc332xX37jbunGnFukS0iLVAWkRV6/y3Z3a5IvuZRxOvKOXDwEDeOE
jaZtJT+KnJEiJGAk2QKfqCMwGMPu8bvGfHbxP3icczPTrTqZuR7OxN77FZjxi4Cw7BALjVhdf6a1
+LWF/BMva8QDgFiPwwHKglY9H5naTKCr6v5N2Iyaxu7ju4sY7ENG4gdxjMFgsZxfx+S7RyYA/Oqw
3TXTaHLHWhQJVIMo1ItzRtuRSXUcJF1VS+JPGWyyPGTECbb7z7blG2e2koBaKdQGiYJtFVFIs2QF
VJrdUgpCPNo9rLrPH28dTFT/AGQsAUmCZvn7MGuolmuKwxFco8w6GvARcrbXefbUW1L1LjALRPp+
0TBuJThhKnCAqpdcqlFoyJzIdmNmZA4kKQuwNj7AFpnXjBg7OL5pZrlM8ynKPmMxeA7AsGTdiEmr
jpI6SK5OxDRyIHcpNOJxJsn8fku0clAPwSMx3K3KmKINoUCVSFKNB7kMFcj03CjhMQikIxJ4xOJx
4Fb9wxmARpl0c9HLI7eUhvE81hm4240LbMGVi722KzYEkD9e16eQJqiq/wBNa/GLFPjXvBTAC4A4
kFFCKwFaQyGZhcsqyYT03Qoz129dpr59dr19e21FuRuRUQ4vIy1QlZEWHsMdJNJcR7HmU+c+U8tm
Z04zbomsTaSXqmjJEhw6AA/8Vafp361+niDW93lW9BufT/IJ9WvVxWc/AfRzz/m8Wm21k0ixX/WR
7Ye6/mRpT7w1LlLVaVvCum+FBexHsSC/Prthvv3K3dONOLcIl5EVp8rIir59lu1u1k/feUcTryn4
h4DBvHCRtM2ip6KpSVIiRgEaib+KLT9PxiAxl93j94747eJ+8TrmZ6dadTNywZ2JvhYrJgMWAWFY
QBUYqMgGmtfi1hfwTPkXiBcAsR2GA9UCrHo9NCiv4BJuxCS2N3c93FjnIjo3EDuMZgkEjHD+PyXa
KTACB1WG6a6bQ5U61Kg6pCE2pFubNtyKS6q6RdRQviT4lksjwMOItzvQtyUbabaSgDon1AaKAm0W
UzirdEXUmt1SikI63j2s2s0ebx1MXP8AZGwBSQNm/fMwa+aWa4rDEV9TzDYa8ALl7a7z7ajGpWpU
XA6KdPuiQJxKcMJS5QFVNrlUgpKROpDsxsxIHMiSF2FsfYIxM68YMHhxxnhm4K55lOUfMZi8BWDP
5N2ISauekfpIrg5EdG4idyk05nMmyfx+S7RSaPkASUy3L3LmKINoTB1SEJtB7kKFcj03CjhIQiiI
4J4xOJx4HcNwRmARpl0cdHDI5eMhvI41he4+48LbMGVi722IyHkkT9eV4fRKKi4BptX4pQpw9ecF
MQLgDwQUUIKgVZDIJoFuy7JhXTdCTXXb12mvn12vX17fUW5G49RDi8iL1EWkRZiw40l0l4YsfKfO
OZTk3ZHDjJuiawNpJ/wkZIkOHwAH61p+nfrTjnnwe3+8q3oN6Y4/2Cf1p1c1pPo/x6888f3eLTXa
ybRYv9Pp8SPbMHXp/FjSv3hg61rWFdN0KC9iPYiG5vrthvv3G3dN9OLdIFpCWqAvIS75/lu1uzkR
f8yfzrynld4CBvHCRtM2kr/FSkqREjAK5zFIVPU/GYDGH3eP3jPjt4nrxOupnp1pzM3LBlYu+Fit
GAxYBYNhAVxiwuv9NIAMWF/BsuRfAF0C4H4YD1QKsej00Ar6v5P2Iyaxe7ju4sY7EdHIidxjUFgk
ZxfR+TbRSUA/PLQ3TbTaHrHWhQJVAQo1ItzZtuRScKrpF1VS+JPGWSyPAxAi2+9C3JRtptnKQWin
UBomDbRZRSLNkBVSa31IKQjzePay6zR9COJC7A2QsAUmCZv3zIGuojmuKxxFfU8w6GvFrjLBctbX
efbUX1K1Li4LRPp90SBuJThhKXCAupdcqkFoyJzIdmdmZA4kKQuwdj7BFJHXjBg8OOM083BXPMry
j5jMXkbKwLBkvYhJq46SOkeuDkQ0biJ3KTTicSXJ+Ak20ckAPwSMy3K3KmSIJqTCVSEJtBzgKFcD
03CjhMSkkI4J4xOJx4M2/cMagMaY9HPRuxO3jIbxOtoXuPuPDWw9lYu9tiMmBFA/Xlen0CioqvtN
q+GKlPjXnumIFwBxfqKEVQKshkM0tM5QuWXZUJ6b4Ua67eu0z8+u1++/bqi3I3JqIcWkZaoS0iLj
2HGkmkuI9j5TxOfKOWzM4cZN0TWJtJL1TRkiQ4fACTMSOea0/Tv1p9Hgtvd5VvQb6OPQBP6z6uK0
nwD04559fd4tNdrJpFi3+sj2w91/MjSn3hkFa1rCum2FBuxLsSC/Pvthvvgjbmm+nFukS0iK1CWk
ZZ8+y3c3a5fvvKeJ35R9e8Bg3jhE3gaSz4+sSkiREjAQg09T8ZgMZfd43eK+OXievE65menOnUyc
j2Vib3WKzHjFwFhWCAWGKi6/01gAtYX8Ez9rxAuAWI/DAeqCVj0fmYar6ASfsQktj93HdxYx2IaN
xE7xGYNBozi+ASXaSTACB5aG6a6bQ9Y40KhKpBlGpBuaNtyKS6jhIuqqXxJ4yyWR4AQJtrvPtuUb
Z7aSgFop1AaJgm8VUUizZAVUmt1SCm8ebR7WbWePt44mKn+yFgCkwTN8/Zg11Es1xWGIrlHmHQ14
CLlba7z7ai2pepcYBaJ9P2iYNxKcMJU4QFVLrlUotGROZDsxszIHEhSF2BsfYAtM68YMHZxfNLNc
pnmU5R8xmLwHYFgybsQk1cdJHSRXJ2IaORA7lJpxOJNk/j8l2jkoB+CRmO5W5UxRBtCgSqQpRoPc
hgrkem4UcJiEUhGJPGJxOPArguGMwCNsejno4YnbxkV4nWsM3H3FhjZgysTe2xWbAkifr2vT6BRU
VX+m1fjFSnxr3gpiCcAsX6ihFYCrIJDMw9W/6dClP90fzFaL85/5LfIfcpX4D+ct6/O78Lf1/wAq
PaPkZ+Hb7L+++Q+UfMv+F7T7n9if2DjOea1/Tv1p68+C293lW9Bvo445AT+tOres58A4+n6Pd4vN
NrZrFi/Hrz6Oh7Ue6/mRpT7w43uxW1awnpuhIXsR7Egvz77Yb69xt3TnTe3SJeRlafLSEu+f5btb
tckH/Mo868p5XeAgbxwkbTNpKfxUpIkRIwEbIFQVBGYDGH3eN3jPjt4n7xOOpnpzp1MnLBlYu91i
smIxcDYdggFhiouv9NoAMWF/BM/a8QDgFiPwwHqgVY9H5pUmbgq+r+TdiMmsbu47uLGOxDRuIHcY
zBYLGcX8fku0cmAPzqsN0102hyx1oUCVSDKNSLc0bbkUl1HCRdVUviTxlssjxkxAm2+8+25Rtntp
KAWinUBomCbRVRSLNkBVSa3VIKQjzaPay6zx9vHUxU/2QsAUmCZvn7MGuolmuKwxFco8w6GvARcr
bXefbUW1L1LjALRPp+0TBuJThhKnCAqpdcqlFoyJzIdmNmZA4kKQuwNj7AFpnXjBg7OL5pZrlM8y
nKPmMxeA7AsGTdiEmrjpI6SK5OxDRyIHcpNOJxJsn8fku0clAPwSMx3K3KmKINoUCVSFKNB7kMFc
j03CjhMQikIxJ4xOJx4FcFwxmARtj0c9HDE7eMivE61hm4+4sMbMGVib22KzYEkT9e16fQKKiq/0
2r8YqU+Ne8FMQTgFi/UUIrAVZBIZmFyyrJhXTdCjXXb12mvn12v31yOqPcjcioRxaRlqiLSMsPYc
aSaTYj2PMp854lGTZmcOM26JrE2kl6poyRIcPgAP1rT9O/WnHH2Gt7vJt2DfRx/YJ/WnVxWc+AfR
z/taLTXayaRYt6fucj2w91/MjSn3hgVtWsK6boUG7EuxIL8++2G++CNuab6cW4RLyErUJaRF3z/n
dvdrl+/8o4nXlHxDwIDeOEjeBtLP+IjJEiJGAhEp6novAIw+7xu8Z8dvE/eJ1zM9OtOZm5YM7E3v
sVkPGLgbCsICuMWF1/prABSov4Jl7XiBcAuB6eA9UCrHo9NI3FQdf17KOxCTWL3c93FjHIjo3ETu
MZgkDjOJABJ9o5LHyJ5aHababQ5U60KBKoBlGpBuaNtiKThRwkXVVL4k8ZZLI8NehhxNt959tyjb
TbSUA9E+oDRMG3iqqkWboiqk1vqQUhH20f1l1mj7aPJirA2Qn4rAEzfP2YNZRLNcVhiL5R5hsNdl
IsXtnvPtqL6lalxcHol0+aJgnEpTwlLhEVUmuNTC0ZE5kOzOzEgcyFIZYOyFgC0jj1gPeHV88M3B
XPMpyj5jMXgbsCwZN2IyeuOkjpIrk7ENHIidyk04m8myfx+S7RSaPvwaEy3K3LmSAJqUCVUFJtBz
gKGcDk3CjhMQkiHxJ4xOJx4FcNwRqAxpl0cdHDI5eUivE41hm4+40MasGVjb22IyYEkT9e14eQKK
i6/02r4YqU4ePOCmIJcDi/UUIrAVZDIZoFyybKhfTfCTfXd12m8b67Xr69uqPcrciohxaRl6iLSE
uxYY6SaS8MGHMp5nHlOTZmcNtG6RvA2klzykjJEhw6ABr7tf071a8c8+C293lW9Bfox45j8+rTq3
rOfR/n6ef84i012smkWLf6yPbMHX8yNK/eGBWlbQvpvhIXsS7EQvz77YL79xtzTjTm3SJaRlqfKy
Ms/fc7s7tcv3/Mp868o4cPAYN64SNJG0Vf4iUkSIkYAEGn6gjMBjD3vH7x3x28D15HXM00605mbl
gzsXfCxmbAasBsKwgCwxUZX+msAFrC/g2fIvgC6BYjsMByoFWPR+aViZs6+r+TdiEmsbu57uLFOx
DR2InMYzBIHGeH8fku0Ulj5A8tDdN9NocscalAlUgyjUg3Nm25FJwqukXUVL4k8ZZLI8QCBFt96F
tyjbPbSUg9FOoDRMG2iyqsWbIC6j1vqUUhHm0e1m1lj7eOpC7A2Qn4pMGzfvmYNdRLNYVjiK+p5h
0NeAFy9s96FtxbUrUuMA9E+n3RMG5lWGEqcIi6l1xqUUjIl5DszsyfcyFMXYOx1gC0zjxiwenHGe
Ga5TPMryj5jMnkb3KwbBk3YjJa46SekiuDkP0ch53KTTicSTJ/H5LtHJI8QBIzLcvcuZIgmhQFVI
Qq1HOAwVyOTXVcJiEUhGJPGJxOPFZuC4Y1AIyy6OOjhievKQ3kdawzcfceGNWDKxd7rFZsCKJ6va
8PIk1Rdf6bV8LVKfGPPdOATgFiQUUILAFZDIZoTZasiyIV03Qo1129dpv59dr19e21FuTuPUQ4vI
y1RFpGXHsMdJNJcWDHynznmU8t2Zw4zbom8DaSfqklJEhw6AVmrHrWn6d+tOOPWDW93k29Bvo459
gn1adXFZz4B9HP8AteLTXayaRYt/6HI9qPdfzI0p94ZBWtawrpthQXsS7Egvz77Yb64JW7pvpxbp
IvIStQlpGXfP+d2t2uX7/wAo868o+veAwbxwkbwNJKcfWpSRIiRgIQaep+MwGMvu8bvFfHLxPXid
czPTnTqZOR7KxN7rFZjxi4CwrBALDFRdf6awAWsL+CZ+14gXALEfhgPVBKx6PzMHX1fybsRk1jd3
HdxYx2IaNxA7jGYLBYzi/j8l2jkwB+dVhumum0OWOtCgSqQZRqRbmjbcikuo4SLqql8SeMtlkeBi
BNt959tyjbPbSUAtFOoDRME2iqikWbICqk1uqQUhHm0e1l1nj7eOpip/shYApMEzfP2YNdRLNcVh
iK5R5h0NeAi5W2u8+2otqXqXGAWifT9omDcSnDCVOEBVS65VKLRkTmQ7MbMyBxIUhdgbH2ALTOvG
DB2cXzSzXKZ5lOUfMZi8B2BYMm7EJNXHSR0kVydiGjkQO5SacTiTZP4/Jdo5KAfgkZjuVuVMUQbQ
oEqkKUaD3IYK5HpuFHCYhFIRiTxicTjwK4LhjMAjbHo56OGJ28ZFeJ1rDNx9xYY2YMrE3tsVmwJI
n69r0+gUVFV/ptX4xUp8a94KYgnALF+ooRWAqyCQzMLllWTCum6FGuu3rtNfPrtfvrkdUe5G5FQj
i0jLVEWkZYew40k0mxHseZT5zxKMmzM4cZt0TWJtJL1TRkiQ4fAA4b/plt1v93fmFRnzS+RnvXgP
zUqj/nU9fml+Xh8Z8zvIfnp+Dn7y/e/ZfEv9F+N9k+1X7B6BW1awnpvhIXsS7EQvN99sN9+427px
pxbZEtIitQFpGWfkOd2t2uSD/mUedcyjld6DBvHCRtI2mp/FSkqJEjAY3Gzn6gp+MwGMPu8bvGfH
bxPXgdczPTrTqZOWDOxN77FZMBiwGwrCArjFhdf6a18LWF/Bs/a8QLgFiPwwHqgVY9H5oJ1hivq+
k3YhJrG7uO7mxjsR0biJ7GMwaCxnAgBk20Umj5A6tDtNdNoesdaFAlUBCjUg3NG25FJdRwkXVVL4
k8ZbLI9WDECbb7z7blG2e2koBaKdQGiYJtFVFIs2QFVJrdUgpCPNo9rLrPH28dTFT/ZCwBSYJm+f
swa6iWa4rDEVyjzDoa8BFyttd59tRbUvUuMAtE+n7RMG4lOGEqcICql1yqUWjInMh2Y2ZkDiQpC7
A2PsAWmdeMGDs4vmlmuUzzKco+YzF4DsCwZN2ISauOkjpIrk7ENHIgdyk04nEmyfx+S7RyUA/BIz
HcrcqYog2hQJVIUo0HuQwVyPTcKOExCKQjEnjE4nHgVwXDGYBG2PRz0cMTt4yK8TrWGbj7iwxswZ
WJvbYrNgSRP17Xp9AoqKr/TavxipT417wUxBOAWL9RQisBVkEhmYXLKsmFdN0KNddvXaa+fXa/fX
I6o9yNyKhHFpGWqItIyw9hxpJpNiPY8ynzniUZNmZw4zbomsTaSXqmjJEhw+AA/WtP079accfYa3
u8m3YN9HH9gn9adXFZz4B9HP+1otNdrJpFi3p+5yPbD3X8yNKfeGBWtawrpthQXsS7Egvz77Yb64
JW7pvpxbpIvIStQlpGXfP+d2t2uX7/yjzryj694DBvHCRvA0kpx9alJEiJGAhDp6no1AIw+7x+8Z
8cvE/eJ1zNNOdOZm5YMrF3vsRkPGLgLCsAAsMUF1/prABawv4Jn7XgBcgsR+CY9UErHo/MwK/ryT
9iEmsfu47uLGORHRyIHeIzBoLGcSACTbRSaPkDqsN0103hyx1oUCVSDKNCDc0bbEUl1XCRdVUviT
xlksjy1xkCBVud51tyjbPbOTgtE+oDRMG3iyqkWbIi6k1vqUUhHm0f1m1mjzePJiZ/shYApMGzfP
mgNxmlmuKwwFco8w2GvFGUtljNs96NsxfUrUqLAtE+n7RME4lOGEqcICqm1xqUUjInMg2X2ZkDmQ
pC7A2PsAWkdesGDs4uolmuUzzKco+YzF5Gom2bBsGTdh8mrnpH6R65OxHRqInMpLN5xJeX4CS7Ry
WPPwSUx3J3KmKINoUCVSEKNBzgKFcD03CjhMQikIxJ4xOJx6pUxs1b9wxqAxll0cdHDI5eMhvI61
hm4240MbD2Vjb22IyHkkT9eV6fQKKi6/01r4YsU+MecFMQK4HF+ooQWAqyGQTSNLlk2VCenCEmuu
3ruNfPrtfvrgdUe5O5NRDi0iLVGWkRceP40l0l4YMfKeZ15Ry2ZnDjNukbxNpJc8pJSRIcOgAa9a
1/TvVpx9EFt3vKt2C+uOPPj8/rTq4rSfAOfXn6fd4rNdrZrFiv08/wBSPasHPpx9ZGlfvDArWtYX
03wsN2I9iYX59dsN9ckrd0404t0iWkRaoC0jLPn/ADu1u1yQf8yjzryj4h6CBvHCRtM0kp/FSkaR
EjAAh1BT8ZgMYfd43eK+O3ievA66mmnWnE0csGdi732MyYClgNhWFH1hiwqv9NYALWF/BM/a8QLo
FwPwwHqgFY9HpoSbkV/X8n7EJNY3dx3cWKdiGjcRO4xmCQSM4PwEl2jkwB+dVh2m2m0OVOtSgSqg
hRqRbmzbcik4UXSLqKl8SfEslkets4yBAm2u8625RtptpKAeifUBooCbRdVWLNkBVSa31IKQjzaP
6zayx5vHUxdgbIWAKTBs375mDXUSzXFY4ivqeYdDXhCLl7Z7z7ajGpepcXBaJ9PmioNxKU8JS4RE
1LrlUotGROJDsxszIHMhTFWDshYIpM68YD3hxwonm4K55lOUfMZk8B2DYEm7EJLW/ST0kVwdiGjc
QO5yacTeTZvwEl2jkkffgkZhuXuXMkATUoFqkKTaD1woVyPTcKLpCUURHBPGJxOPRuNmbhuCMwGM
sujjo3YnbxkN5HGkM3H3HhjZgzsTeyxWY8mier2vT6JRUXX2mtfClSnDt77piBXBcP1VCKoFWQSC
aFXLLsmE9N8IM9dnXaZ+fXa9fPt1R7j7k1ENLSItUZeRFx7DjSXSbFgw5lPE64lPLZmcOM26JvE2
kn6pIyRIcOgBJ1hnjmtP071af8C293lW7BvXj6AE/rPq4rOfgf38/SWi012tmsWLfv8ARyPaj3X0
fWRpT7w6kTUFW1awrpuhQbsS7Egvz67YL79xt7TfTi3CJaRFagLSMu+f5btbs8v3/lHE58o+veAw
bxwkbwNJZ8fWJSRIiRgJlCp+n4zAIw+7xu8Z8cvE9eJ1zM9OdOpk5YM7E3usVmwGLALCsECqMVF1
9prABawv4Jl7XgBcAsR+GA5UErHo/MwK/r+TdiElsXu47ubGOxDRyIneIzBYJGcX0fk20Ulj788r
DdNtOIcsdaFAlUAyjQi3Nm25FJwo4SLqql8SeMtlkeBiBNt959tyjbPbSUAtFOoDRME2iqikWbIC
qk1uqQUhHm0e1l1nj7eOpip/shYApMEzfP2YNdRLNcVhiK5R5h0NeAi5W2u8+2otqXqXGAWifT9o
mDcSnDCVOEBVS65VKLRkTmQ7MbMyBxIUhdgbH2ALTOvGDB2cXzSzXKZ5lOUfMZi8B2BYMm7EJNXH
SR0kVydiGjkQO5SacTiTZP4/Jdo5KAfgkZjuVuVMUQbQoEqkKUaD3IYK5HpuFHCYhFIRiTxicTjw
K4LhjMAjbHo56OGJ28ZFeJ1rDNx9xYY2YMrE3tsVmwJIn69r0+gUVFV/ptX4xUp8a94KYgnALF+o
oRWAqyCQzMLllWTCum6FGuu3rtNfPrtfvrkdUe5G5FQji0jLVEWkZYew40k0mxHseZT5zxKMmzM4
cZt0TWJtJL1TRkiQ4fAAfrWn6d+tOOPsNb3eTbsG+jj+wT+tOris58A+jn/a0Wmu1k0ixb0/c5Ht
h7r+ZGlPvDDwv8qvvi9g949o2q9x8F/NU8U80u/3/wCfHzL8F94+I+H8W/Md8V+2nsfuXzE8Z/gf
4/If7F+2sZ+tI9dp+n4xAoy97xu8d+cvE7eJ1zM9OtOpm5YM7E3wsVmwFrgbDsEAuMWF1/prABi4
vlmz5F4gXQLEengPVAqx6PzTDpGkCAV9JexCTWL3cd29jHIjo3ETuMZgkEjOL4BJtopNHyB5aHab
abQ5U41KBKpCFGpBubNtyKS6i6RdVUviTxlssjxWBQi2+8+25RtntpKAWifUBomCbRVRSLNkRVSa
31KKQjzePay6zR9vHUxdgbHz8UmDZvn7MGvmlmuKwxF8o8w6GvKzMX8WC5W2u8+2otqXqXGAWifT
9omDcSnDCVOEBVS65VKLRkTmQ7MbMyBxIUhdgbH2ALTOvGDB2cXzSzXKZ5lOUfMZi8MnYFgybsQk
1cdJHSRXJ2IaORA7lJpxOJNk/j8l2jkoB+CRmO5W5UxRBtCgSqQpRoPchgrkem4UcJiEUhGJPGJx
OPArguGMwCNsejno4YnbxkV4nWsM3H3FhjZgysTe2xWbAkifr2vT6BRUVX+m1fjFSnxr3gpiCcAs
X6ihFYCrIJDMwuWVZMK6boUa67eu018+u1++uR1R7kbkVCOLSMtURaRlh7DjSTSbEex5lPnPEoyb
Mzhxm3RNYm0kvVNGSJDh8AB+tafp36044+w1vd5Nuwb6OP7BP606uKznwD6Of9rRaa7WTSLFvT9z
ke2Huv5kaU+8MCta1hXTbCgvYl2JBfn32w31wSt3TfTi3SReQlahLSMu+f8AO7W7XL9/5R515R9e
8Bg3jhI3gaSU4+tSkiREjAQg09T8ZgMZfd43eK+OXievE65menOnUycj2Vib3WKzHjFwFhWCAWGK
i6/01gAtYX8Ez9rxAuAWI/DAeqCVj0fmYar6ASfsRlFi93HdxYxyIaOw85xGILBo1g/ASbaKSgH5
5WHababQ5Y41KBapCFGpBubNtyKThVwkXVVL4k8ZbLI8Ii31DiLb7z7ak+2m2cpBaKdQOigJvFlF
Iu3QFVJrfUgpCPN49rLrNH28dTFWBshYApMGzfv2YNfNHNYVhiL+p5hsNeRu9afMuWtrvPtmL6l6
lRYHon0+6JgnMpwTlLhEVUmuNSikZE6kOzOzEhcyFIXYGx1gC0jjxgweHF8k83BXPMryj5jMXgmL
Kf2DJexCTVx0kdI9cnYjo3EDvMmnM5k2T8BJNo5LHn4JGY7lbkzFIG0KhKoCE2g9wFCuB6bhVwkI
RSEYk8YnE49WImjuC4ozAIyy6N+jhkdvGRXkdaQzcfcaGNh7Kxd7rFZMCSJ6vK9PolFBVf6a18LV
KfGPPdcQC4LEgpmQVArSGQzONxNrllWTCum+Emuu3rrNcX32vX1wNqLcncmoxxaRl6hLyMswYYaS
aSYsGPMp4nHlHLdkcOM26JrA2knzymjJEhw6ACb6Neta/p360+niC273lW9BfXHjjmPz+tOris58
A+nnn6TEXmu1s0ixb09f6ke2Huv5kaU+8MorWtYV03QoN2JdiIXm++2C+vcbd0404twiWkRany0i
LPX+W7W7XL99zKOJ15R8Q8BA3jhI2mbTU/ipSRIiRgIRKeqCMwCMPu8fvGenbxPXgddTTTnTmZOR
7Oxd8LFZDxi4CwrCAqjVRcA01gAtYX8Ez9rxAuAWI/DAeqBVj0emgbr6v5P2ISWxe7nu4sc7EdHI
id4jMEgsaxfgJNtFJQD86tDtNtN4gsdaEwlUBCjQg3Nm25FJwqumXUVL4k8ZbLI8CECbc7z7blG2
e2kpBaJ9QOiYNvFlVIs3QF1JrdUYpCPto9rNrNHm8dTGT/Y+fikwbN8/Zg11Es1xWGIr6nmGw12T
dgsXtrvOtmMal6mRcFon0+6JgXEpwwlK6IqpdcqlGIyJeQ7M7MSBxIUhlg7H2ELSOPB7B6ccZ4Zu
CueZTlDzGZPKwdgWDJ+xCT1z0kdJFcHYjo5ETmUmm83k2T+PyXaKSx9+CRmO5W5UyRBNCYOqQpRm
PcBQrgcm4UcJCUURHBPGJxOPAXDcEagEZY9G/RwyO3lIbyONYZuPuPC2rBlYm9tiMmBJI/XtenkC
ioqAabV8KVKcvHnunAFcFwQUUIrAVZBIJpG42XLKsmE9N0KNddvXaa+fPa9fft1R7k7kVGOLyMtU
ZaRlmI/HSTSTFgw5lPM5ylOTZmcNsm6JvA2knzykjJEhw6AFL1rX9O9Wn/Atvd5NvQbn/Dj/AGCf
1p1b1nPgHr68/wCbxaa7WzWLFv3/ANSPbD3Xp/EjSv3hhitq1hXTdCgvYl2Ihfn32w317lbum2nF
ukS8iK1AWkZZ8/y3a3b5fP8AyjideUfEPAgN44SN4GklOPrUpIkRIwGuaFT1PxmAxl93jd4z07eJ
+8Trmaac6dTJywZWJvdYjNgMWAWFYIFUYqLr/TWAC1hfwTP2vEC4BYj8MByoJWPR+Zjdqvq8k3Yh
JrG7uO7ixjkR0ciJ3iMwWCRnF+Ak20cmj5A6tDtNdNocsdaFAlUhCjUg3Nmm5BJdRwkXVVL4k8ZZ
LI9GoimxIa2e9C25RtntpKAWifUDooCbRZVSLNkBVSa3VKKRj7aPaz6zR5vHEhdgbIz8SkCZv3zM
GvmlmsKxxFfU8w2GvKXD4lyttd59txfUvUuLg9E+n7RME5lWCcpcIiql1yqUUjInMi2Z2ZkDiQpC
rA2OsAWmdeMGDs4vmnmuUzzKco+YzF4ZasCwZN2ISauOkjpIrk7ENHIgdyk04nEmyfx+S7RyUA/B
IzHcrcqYog2hQJVIUo0HuQwVyPTcKOExCKQjEnjE4nHgVwXDGYBG2PRz0cMTt4yK8TrWGbj7iwxs
wZWJvbYrNgSRP17Xp9AoqKr/AE2r8YqU+Ne8FMQTgFi/UUIrAVZBIZmFyyrJhXTdCjXXb12mvn12
v31yOqPcjcioRxaRlqiLSMsPYcaSaTYj2PMp854lGTZmcOM26JrE2kl6poyRIcPgAP1rT9O/WnHH
2Gt7vJt2DfRx/YJ/WnVxWc+AfRz/ALWi012smkWLen7nI9sPdfzI0p94YFa1rCum2FBexLsSC/Pv
thvrglbum+nFuki8hK1CWkZd8/53a3a5fv8Ayjzryj694DBvHCRvA0kpx9alJEiJGAh+UvxK96n/
AIn+aXr7v6/nd+9+O11594n/AMoH4nPbPYfOvkf4P9nvDPh/Cvlp/X+w+If1v7byj62j9KV/X8m7
EJLYvdx3cWMdiOjkRO4xmCQSM4vwEm2jkwB+dVh2m2m0OVONCoSqApRoRbmzbYik4VXSLqql8SeM
tlke5urQgTbfedbcp202zlALRPqA0TBtosopFm6AqpNbqkFIR5tH9ZtZY+2jyQqf7IWAJSBs379o
DXzSzWFY4i+UeYbDXgfMwYtrvPtyL6l6lRcDon0/aJgnEpwxlLhEVUuuNSikZE5kWzGzEgcyFIZY
OyFgDEjrxgweHHCiebgrnmV5R8xmLwk7wVgWDJuxCTVx0kdJFcnIho5EDmUnnE4k2RCPyXaOSx9+
CRmO5W5UxRBNSgSqQpRoPcBQrkem4UcJiEUhGJPGJxOPVgrguGMwCNsejno4YnbxkV4nWsM3H3Fh
jZgysTe2xWbAkifr2vT6BRUVX+m1fjFSnxr3gpiCcAsX6ihFYCrIJDMwuWVZMK6boUa67eu018+u
1++uR1R7kbkVCOLSMtURaRlh7DjSTSbEex5lPnPEoybMzhxm3RNYm0kvVNGSJDh8AB+tafp36044
+w1vd5Nuwb6OP7BP606uKznwD6Of9rRaa7WTSLFvT9zke2Huv5kaU+8MCta1hXTbCgvYl2JBfn32
w31wSt3TfTi3SReQlahLSMu+f87tbtcv3/lHnXlH17wGDeOEjeBpJTj61KSJESMBCDT1PxmAxl93
jd4r45eJ68TrmZ6c6dTJyPZWJvdYrMeMXAWFYIBYYqLr/TWAC1hfwTP2vEC4BYj8MB6oJWPR+Zg6
/r+TdiElsfu47uLGOxHRqIHeIzBYNGMX8fku0cmj786rDdNdNocqcaFQlUgyjUi3Nm25BJwo4SLq
ql8SeMtlkeDQcPbnehbkp2121lALRTp/0TBtosopFmyAqpNbqlFIR1tHtZtZ4+3jiYqwNkbAFJgm
b98zBr5pZrisMRX1PMOhrw1EU+hctbXehbUY1K1Ki4LRLp90TBryrHCVOEBdTa5VKLTkTmQbNbMS
FzIkhdg7IWCMSOvGDF4ccKYZuCueZXlHzGYvIa3bNg2DJew+S1x0kdJFcnIho5EDuUmnE4k2T8BJ
do5MAfgkpluTuVMkQTQoFqoKUaD3AUK4HJuFHCQlFIRiTxicTj5pi4LfjMBjTHo46OWRy8JFeR5r
DNx9x4Y2YMrF3tsVkPJIn69r08gTUFV/prX4xUp8Y94KYAVwXBBRQisBVkEhmlYpcsuyoT03Qk11
29dhr599r19e21HuRuPUQ0vIy1QlpGWHsMdJNJcWDHmU8TnynJuyOHGTdE1ibSS9UkZIkOHwAT4b
45rX9O/Wn/Atvd5VuwX14455AT+teris5+A/f6f3eLTXayaxYt/rI9qPdfzI0p94cbOta2hXTdCg
vYj2Iheb77Yb69xt3TjTi3SJaRFqgKyIs+f5bs7tckH3lHE64lHxDwEDeOEjaZtFT+KlJEiJCAhC
p+nozAYy+7xu8d8dvE/eJ11NNOtOpm5YM7E3wsVkwGKgLCsICuMVF19prABiwv4Jn7XiBcAsR2GA
5UCrHo/Mwdf1/JuxGTWN3cd3FjnIjo5ETuEZgkEjPD4BJdopNH355WHab6bQ5U40Kg6pBk2pFuaN
tiCThVwkXUVL4k8ZZLI+GhAi2+8+3JRtntpKQWivT/ooCbRbNSLN0BVSa3VKKQjraPazazR9vHEx
U/2QsAUkCZvnzMEsokosKwxFfU8w2GvAyWLW13n21FtS9TIuB0T6fNEwLmUpYSlwgKqXXKpRSMic
SHZjZiQOZEkMn+x1gDEjrxgweHV88FHBXPMpyj5jMnhP2FgWBJuxCTVz0k9JFcHIjo3EDmUnm84k
uT+PyXaOSx8gCRmO5W5UzRBtCgSqgxNoPcBgzgcmuoumIRREYk8YnE49bZxlq4LgjNfxpj0b9G7I
7eUivM62hm4+48MasGVi722KyHkkT1e14fRJqioBptX4lQpw8ee6cAlwPBBRQgsCVkEgmizGVuy7
KhPTfCTXXb12Gfn12u317dUW5O5NRjy8iLVGWkJcewx0l0lxHsfKeZzzKsmzM2cZt0jeBtJLnlJK
SJDh0AjUaQz92n6d+tPX7C293k27BufTjj2Cf1p1cVpPwP7+fpLRaa7WzSLF/wB/9SPaj3Pp/EjS
n3hlFa1rCum+FBuxLsRC8332w33ySt3TjTi3CJaRFqgLSMq9f87tbtcvn3lHnXEo5cPAQN44RNpm
0lP4qUkSIkYAHPU/T8agMZfd43eM+O3ifvE65menOnUzcsGli732IzYDFgFhWCAWFrC6/wBNYALW
F/BM/a8QLgFiPwwHKglY9H5pXNuvq+k3YjJrG7ue7ixjkR0biB3GMwWCRjF+Ak20clj5E6rDdNdN
ocqdalAlUAyjUg3NG25FJdRwkXVVL4k8ZZLI9Go2fQOItvvOtuT7Z7aSgFon1A6Jgm0WUUirduKq
PW+oxaEfbR7WXWWPN44kLsDZGfiUgbN+/Zg180c1xWGIr6nmGw14WZiGS5e2u8+2oxqXqVGAWifT
5omCcSrHCUroiql1zqUYjIl5BszsyfcyFMZYOx9gjEjrxgweHHGeGbgrnmV5R8xmTysFYdgSbsPk
tb9JHSRXJ2IaNxE7lJpzOJNk/j8l2jkkffgkZluVuVMUQTQoEqkIVaDnAUK5Hpr5uExCKQjEnjE4
nHgxcFwxmARtj0c9HDE7eMivE61hm4+4sMbMGVib22KzYEkT9e16fQKKiq/02r8YqU+Ne8FMQTgF
i/UUIrAVZBIZmFyyrJhXTdCjXXb12mvn12v31yOqPcjcioRxaRlqiLSMsPYcaSaTYj2PMp854lGT
ZmcOM26JrE2kl6poyRIcPgAP1rT9O/WnHH2Gt7vJt2DfRx/YJ/WnVxWc+AfRz/taLTXayaRYt6fu
cj2w91/MjSn3hgVrWsK6bYUF7EuxIL8++2G+uCVu6b6cW6SLyErUJaRl3z/ndrdrl+/8o868o+ve
AwbxwkbwNJKcfWpSRIiRgIQafqCMwCMvu8bvGfHbwP3ieczPTnTqZuR7Kxd7rFZjxi4CwrBALDFR
lf6awAWqL+CZ+14gXILEfgmPWBKx6PzMIn/UKdzP/ih9T/Z/nl+O/wB8+W9n+AfJT1/B/wDJL23z
Dwb8DfnX2d9Ph/dvmZ/X+SeXf1n7Wo7wVLoRAi2u8+3JRtptpKAWinT/AKKAm0WUzizdAVUmt9SC
kY63j2s2ssebx1MVYOx9gCUwTN8/aA11Es1xWGIr6nmHQ15l0IuXtvvPtqL6l6lRcFol0/aJgnEp
TwlK6AqpdcamFIyFxIdmNmZA5kSQuwNkLBFJHHrBg7OLqJ5rlc8yvKPmMxeBmwbAkvYhJq46SOki
uTkQ0biB3KTTmcybJ+Aku0clAPwKUy3K3LmSINoUB1QEJtB7kKFcj03CjhMQikI4J4xOJx4zLNv3
BGYBGmPRz0csTl4yG8jzWF7jbjQxsPZ2LvbYrJgTRP17Xp5AooKr3TWvxipT4x7wUxAuAOL9RQis
BVkMhmdZXLKsmFdN8KNdd3Xaa+fXa/fXI6o9yNyKiHFpEWqMtIiw9hjpJpLiwY+U+c+UZN2Zw4zb
omsTaSXqkjJEhw+AA/WtP079accfYa3u8m3YN9HH9gn9adXFZz4B9HP+1otNdrJpFi3p+5yPbD3X
8yNKfeGBWtawrpthQXsS7Egvz77Yb64JW7pvpxbpIvIStQlpGXfP+d2t2uX7/wAo868o+veAwbxw
kbwNJKcfWpSRIiRgIQaep+MwGMvu8bvFfHLxPXidczPTnTqZOR7KxN7rFZjxi4CwrBALDFRdf6aw
AWsL+CZ+14gXALEfhgPVBKx6PzMHX1fybsRk1jd3HdxYx2IaNxA7jGYLBYzi/j8l2jkwB+dVhumu
m0OWOtCgSqQZRqRbmjbcikuo4SLqql8SeMtlkeAECbb7z7blO2m2koBaKdP+iYJtFc1Iq3QFVHrf
UYpCPNo9rNrNH28dSF2BshYApIGzfv2YNdRLNYVhgK5R5h0NeB9Cxi2+862ovqVqXFgWifT7ooDc
yrBOVOUBVS641KLSkbmQbMbMSBxIUhdgbIWCMTOPGA94cXzwzXKZ5lOUfMZk8joLAsGUdiMmrjpH
6SK3NxHRuIncpNOJxJs34CS7RyUA/BJTLcrcuYpAmhQJVIQm1HOAoVwPTcKOExCSIjEnjE4nHiX0
K4LfjcAjTPo56OGJ28ZBeJ1rDNx9x4Y1HsrE3tsVmwIpH69r08gTVFwDTWvxapT4177pwAcAuCCi
hBYCrIZBNCrllWRCum+Emuu3rtNfPrtevr22pNydyakHFpEWqEtIyzBhhpJpJiPY8ynmc8yjlsyO
G2TdE3ibSS9UkZIkOHwC2zjDXpWn6d6tfp4g1vd5VvQX/u8fZ+f1p1b1nPo/+/n/ADaLTXayaRYv
/rI9swden8SNK/eHGhWtbQrpvhIbsS7Eg3N99sN9+4W7pzpxbhEtIy1PlpEWfvst2t2uSD/yjide
U/XvQYN44SN4G0lP4qUkSIkICEOnqejMBjL3vH7xnxy8D94nXU00606mbpgysTe+xWQ8YuBsKwgC
oxQXX+mtfi1hfwTL2vEC4BcD8MByoFWPR+aA6/r+TdiElsbu47ubHORHRyIHeIzBYJGeHwCTbRSY
A/OKw3TbTaHKnWpQJVAMozINzRtuRScKLpF1FS+JPGWSyPAhAm3O8+2pTtptpKAWifUBooCbRbJS
LN0RVSa3VKLQjraPay6yR9tHUxdg7I2AJwBM375kDXzSzcC8MRf1PMNhrsGWL213n21F9StS4uD0
T6fNEwLmVYYSpdAVUeuNSikZC5kOzOzB9xIkxlgbH2CLSOvWDB2cXzwzcFc8ynKPmMxeWtL6WdLF
gWDJ+xCS1x0kdI9dHIho3ED3MmnE5k+T+PyXaKSx9+CRmW5e5UxRBMyYaqAhNoOcBQrgemuoumIR
SEcE8YnE49AXBcEagEZYdHHRwyOXlIryPNYZuPuPDGo5lYu91ish5NE/Xten0SqoqAabV+LVKfGP
fdOAK4HggooQWAqyCQzQLtk2VCem6FGuu7rsN/Prtevr22o9ydyaiGFpEWqMrIyzFjjpJpLiwH8y
jznynJuyOHGbdI5ibST55TRkqQ4dAQXPNbfp3604558Gt3vJt+C/RxzyAn9adXFaT8Bx9Pr/AHaM
TXa2axcvx68/1I9qPdfzI0p94YYretoV03QoN2I9iIb599sF9e425pvpxbpItIitQFpEWfP+d292
vcX3Mo4nXlGS7wEDeOETaZtJT+KlJEiJCAkQ6eqCMwGMvu8bvGfHbxP3gdczPTnTqZuWDKxd77EZ
jxi4Cw7BArDFhdf6bQAWsL+CZe14gXALEfhgPVBKx6PzQXrTNf17J+xCTWN3c93FjHIho5EDuMYg
sFjOL+PybaKTR8geWhumum0OWONCgSqQZNqQbmjbYikuo4SLqql8SeMtlkets4y+ggPbnehbUo20
20lALRPqA0UBt4rmpF26AqpdcKlFIx5tHtZdZY82jiYuwdkLAFJg2b5+yBr5pZrisMRX1PMOhryN
guXtvvPtmL6l6lxcFon0+6Jgl5ThhKXCIqpNcqjFpSJeQbMbMH3EhSGWDsdYQtI68YMHhxwphm4K
55lOUfMZk8rEzYsKwpN2ISauekfpIrg5EdHIgd5k04nEmzIR+S7QyUA/BIzHcrcqZIgmpQJVYUm0
HLhgzgem4zXSEIpCMSeMTiceFFcNwRmAxhj0cdHDE7eUivE61hm5G48MbD2Vib22KxYEkT9e16eQ
KKCq+02r0YsU+MecFMQSwLEgooQVArSGQTMldFiyrJhXTdCjXXb12mvn12v31yOqPcjcioRxaRlq
iLSMsPYcaSaTYj2PMp854lGTZmcOM26JrE2kl6poyRIcPgAP1rT9O/WnHH2Gt7vJt2DfRx/YJ/Wn
VxWc+AfRz/taLTXayaRYt6fucj2w91/MjSn3hgVrWsK6bYUF7EuxIL8++2G+uCVu6b6cW6SLyErU
JaRl3z/ndrdrl+/8o868o+veAwbxwkbwNJKcfWpSRIiRgIQaep+MwGMvu8bvFfHLxPXidczPTnTq
ZOR7KxN7rFZjxi4CwrBALDFRdf6awAWsL+CZ+14gXALEfhgPVBKx6PzMOipyrznY7MbO7wO8ixj0
R0LrwkoJiMDi/BCPyfamRxp+bUhOlmmsQVPMyoSqxZhqQbGjTYik4VXRMKKl8CmMslsezymY23a4
9fDsf+qIkfkHvH5c2nvtnnvym8Q8Hq/x38p35ZeDflc/EfJLyrwLy77c+Q+5+zeVfwfFPHf7B+2f
aj6Lk8wLl7a7z7ai+pepcXBaJ9P2iYJxKcE5S4QFVLrjUopGROZDsxszIHEhSF2DsfYAtM48YD3h
xxmnmuVzzKco+YzF5tTsCwJL2IyeuOkjpIrk7EdG4gcyk04nEmyIAJLtHJY+/BJTHcrcqZog2pUJ
VAQm0HuQoVwPTcKOExCKIjEnjE4nHgLgt+NQGNMujno4YnLykV4nWsN3H3GhjYeysXeyxWY8kifr
2vTyBRUVX2mtfjFinxjz3TEC4A4v1FCKwFWQyGaBYsqyoV03wo3129dpv59dr19cjaj3J3JqIcVk
Zaoi0iLsGGOkmkuLBjzKfOfKeW7I4bZN0TeJtJL1SRkiQ4fAK5zFH61p+nfrTj/ca3u8m3YNx6cf
2Cf1p1cVnP4/z+/jn3aLTXayaRYt/rI9qPdfzI0r94YKta1hXTbCgvYl2JBeb77Yb74JW7pvpxbh
IvIitQlpGXfP8t2t2uX7/wAo4nXlHxDwGDeOEjeBpJTj61KSJESMBCFT1PxmAxl93jd4r45eJ68T
rmZ6c6dTJyPZWJvdYrMeMXAWFYIBYYqLr/TWAC1hfwTP2vEC4BYj8MB6oJWPR+Zg6+r+TdiMmsbu
47uLGOxDRuIHcYzBYLGcX8fku0cmAPzqsN0102hyx1oUCVSDKNSLc0bbkUl1HCRdVUviTxlssjwM
OJtvvQtqUbZ7ZygFop1AaJgm8VUUizZAVUmt9SCUI82j+sus0fbx1MVYGyFgCkwTN8/Zg11Es1xW
GIrlHmHQ14IiyLl7a7z7ai+pWpcWBaJ9P2iYJxKcE5S4RFVLrlUotGROZDszszIHMhTFWDsfYAxI
48YMHhxxmnm4K55lOUfMZi8FS3YFgSXsQk1cdJHSRXB2H6ORA7zJpvOZNk/AybaKSgHwJGY7lblT
JEE0KBKoBk2g9wECuB6bhRwmISREYk8YnE49HQ7gt+MwGNsejjo4YnbxkN4nWsN3H3GhbceysTe2
xWQ8kifryvD6JRUXX+m1fjVivxj33TgCuCxIKKEVgKsgkM0C3ZVlQjpvhRrrt67TPz67Xr59tqPc
jciohpWRlqhLSMuPH46SaS4sGHMp858p5bszhtk3RN4m0kueUkZIkOHQAH61r+ngrT90Ft7vJt6D
c88Y4+wT+tOris59H/38/SXi012tmsWL8evP9SPaj3X8yNKfeGDratYX03QkL2JdiIX59dsN9e42
7pxpxbhEtIy1QFZEWfPst2t2uXz/AMo4nXlP17wEDeOETaZtJT+KlJEiJCAhBp+n41AYw+7x+8Z8
dvE/eJ11NNOdO5m6YNLE3wsVkPGLgLCsEAsMWF1/prX4tYX8Gz9rxAuQWI/DAcqBVj0fmgOv6/k3
YjJrG7uO7ixzsQ0ciB3GMwSCRnh+Ak20Umj788tDdNtNocscaFQdUAyjUi3NG25FJwoukXUVL4k8
ZbLI9YmpudQxAi2+8+25RtptpKAWifUBooCbxZRWKt0hVSa31IKQjzaPazayx9vHUxU/2PsAUkDZ
v37MGvmlmuKwxF/U8w6GvIGXL213oW1GNS9SouC0T6fdEwTiU4YSlwgLqXXOpBaMhcyHZfZmQuZE
kLsHZCwRaRx4wYPDi+eGa5TPMpyj5jMXgKwLBk3YhJ656SekeuDsQ0diB3KTTecSbJ/H5NtFJY+Q
BJTLcrcqZIg2hMHVQUm1HOQoVwOTcKOExCKIjEnjE4nHtX6a62C4LgjUAjTLo36OGJ28ZFeJxtDN
x9x4Y2Hs7E3usVkwJon69r4+iVVFwDTavhapT4x57piAXBcEFFCCwFWQSCaZmugu2VZcJ6b4Sb67
eu0zzffa9ffttRbk7kVGOLyMvUZaRlmI/HSTSXEew5lPnPlGTZmcONG6RvA2kl6pIyRIcOgAL/FW
36d+tOP9xbe7yregvrxx6gJ/WnVxWc+j/Przz/m8Wmu1s2ipb6ef6ke1Huv5kaV+8Pfo8mh1tW0J
6boUG7EexIL8++2G+vcLc0304t4iVkJWny0iLPn2W7e7PL5/5R515Ty4eggbxwkbTNpqfxUpIkQI
wHA5+oKfjMAjL7vG7xnp28D14nHMz0506mTlgysTe+xWbEYsAsOwQCwxUZX2msAFrC/gmfteAFwC
xH4YDlQKsej80MzEblX1eyTsRktjd3HdxYx2IaORA7jGYJBY1w/ASbaOSx8geWhum2m0OWONCgSq
AhNoQbmjbcik4UcJF1VS+JPGWSyPWytPLYcTbfehbco2z20lALRTqA0UBNoqopFmyAqpdb6kFIR5
tH9ZtZo83jqYuwNkLAFJg2b5+zBrqJZrisMBXKPMOhryNNFi9s959sxjUvUyLgtEun3RME4lOKcp
XQFVJrlUgpGROpDsvsyfcyFIZYOx9gCkjrxgweHV88M3BXPMpyj5jMXgZsGwZN2HyWueknpGrk5E
dG4gcyk03nMnzIR+TbRSYAQBITHczcqZIgmhUJVYUozHrhQrgemuo4SEooiMSeMTikftsYyxb9wx
qARpl0cdHDE7eMivM62hm4248Naj2Vi722KyYE0j9e16fQKKi6/02r4YqU+NecFMQLgFiQUUIqgV
ZDIZmWJ1dDZdlQnpvhBnrt67TXz67Xr69uqHcncmohxaRFqiLyIswYY6S6S4sGPlPnHlPLdmcOMm
6JzA2knzymjJEhw+ARZ3h8vWtP079accfYa3u8m3YN9HH9gn9adXFZz4B9HP+1otNdrJpFi3p+5y
PbD3X8yNKfeHWBWtawrpthQXsS7Egvz77Yb64JW7pvpxbpIvIStQlpGXfP8Andrdrl+/8o868o+v
eAwbxwkbwNJKcfWpSRIiRgIQaep+MwGMvu8bvFfHLxPXidczPTnTqZOR7KxN7rFZjxi4CwrBALDF
Rdf6awAWsL+CZ+14gXALEfhgPVBKx6PzMHX1fybsRk1jd3HdxYx2IaNxA7jGYLBYzi/j8l2jkwB+
dVhumum0OWOtCgSqQZRqRbmjbcikuo4SLqql8SeMtlkeDp4EztHvMuSbbR7gywDoZ076WRTGDKPI
w3biah1nqwYlG0ItrXrTH28eSFz/AGLn49AGyIP2YNfJJRwKxxF/Ucw6HPM8r0mOi3Gr1z82Tq0/
xeNflWY/K74z8Bvgfupr0/KS+q+bPu3x3zz8b/Gz+MT7yffPZvjv8X9J5l779qf2mfFcZeAWBYEl
7EJNXHSR0kVwciGjkQO8yecTiTZP4/Jto5LHn4JKY7lblTNEG0KBKoCk2g9yFCuR6bhRwmIRSE4k
8YnE49poXBcMZgMaZdHHRwxO3lIryOtYbuPuPDGw9lYu9tjMmBJGQV7Xx5AooKr/AE2r8YqU+Me8
FMAK4HEgooRWAqyGQzQLtlWVCem+Emuu3rtM/Pntevr22o9ydyaiHFpGXqItIy7BhjpLpLiPY8yn
ic+Vct2Rw2zbom8DaSXPKSMkSHD4ACy+Wn6eCs+PXwW3u8q3oL68Y/Z+f1p1b1nPgHPpzz/m8Xmu
1s0ixb9/9SPbD3X8yNK/eGQq1rSFdN0KC9iPYiF5vzthvr3G3NN9OLdIlpGVqAvIiz59lu1uzy+f
+UedeUfEPAYN44SN4G0lP4qUkSIkIAKhBp+nozAIw+7x+8Z6cvA/eJ5zNNOtOpm5YM7F3usZmPGL
ALCsEAqMVGV9ptABawv4Nn7XgBcAsR+GA9UCrHo/M6zVFX1fybsRk1jd3HdxYx2IaNxA7xGYLBYz
i/j8l2jk0ffnlobprpvDlTjQoEqkGUaEW5s23IpLqOEi6qpfEnjLZZHiGIE233n21KNs9s5QC0U6
f9EwTeK5qRZsgKqTW+pBLePNo/rNrNH28dTFT/ZCwBSYJm+fswa6iWa4rDAVyjzDoa8BFi9td59t
RjUrUuLgtFOn7RME4lOGEpcICql1yqUWjInUi2Y2YkDmQpC7B2OsAWmdeMGDw44yTzXKZ5leUfMZ
i8LE0+lgWBJuxCS1x0kdJFcnIho3EDmUnnM4k2T8BJdopLH34JKY7lblTJEE0KA6oClGg9wGCuR6
bhRwmISSE4k8YnE49GxcFwRqAxpl0cdHDE7eMhvE62hm4248MbMGVi722KyYEkj1e14eRKKi6/01
r8YqU4ePfdMALgDi/UUIrAVZDIZoFyybJhPTfCTXXb12Gvnz2vX1wNqLcjcioxxeRlqiLSMsPYY6
SaS4j2PMp855lHLZmcNsm6RvE2mnzykjJEhw+AWo7x9fsDnmtf079accfYa3u8m3oLzzxjxyAn9a
dW9Zz8B6888/5vFprtbNYsW9PX+pHtR7r+ZGlPvDRXXYarWtYX03QoN2JdiQbm/O2G+/cbd0404t
wiWkZaoC0iLPn/O7W7XL9/zKOJ35Ryu8BA3jhI2mbSU/ipSRIiRgMECn6fjMBjD7vH7xnp28T94H
XMz0506mbkezsXfCxGY8YuAsKwgCw1YXX2mtfi1hfwTP2vEC5BYj8MByoFWPR+Z2o7x9fsHX9fyf
sQktjd3HdxYp2I6NxA7xGYLBIzi+j8l2iksffnlYbprpvDljrQqEqkGUakW5o23IpLqOEi6qpfEn
jLJZHlR3j6/YMQItrvPtqU7abaSgFop1A6KAm0WUUizZAVUmt9SCkI82j2s2s0fbx1MVYGyFgCkw
bN++Zg11Es1hWGArlHmHQ15JAXL233n21F9S9S4wC0T6ftFAbiU44SlwgLqXXKphSMicyHZjZiQO
JCmLsHY+wBSRx4wYPDjhTDNwVzzK8o+YzF4BYFgybsRk9cdJHSRXByI6NxA5lJpxN5Nk+j8l2jkk
fIA0pluVuZMkQbUoEqgKTZj3IUK5HpOFHCQlFERwTxicTj1qO8fX7AuC4Y1AIyy6OOjhgdvKRXkd
awzcfceGNmDOxN7rEZMCSJ+vK9PoFFRdf6a18MVKfGPOSnAFcDw/UUILAVZBIJoqO8fX7C7ZVlQn
pvhBrrt67TXF89r18+3VFuRuTUQ4tIy1RlpGVYD8dJNJsR7HmU+c+U8t2Zw4zbom8DaaXPKSUkSH
DoBAuflr+ngrT1+wtvd5NvQb6Mf7BP606t6zn8f9fXnjjkvFpptZNYsW9PX+qHtR7r+ZGlPvD36P
JoVbVvC+m+EhuxHsSC/Pvthvr3G3dONOLcIlpEWqAtIyz1/zu1u1y+feUcTrynlw9BA3jhE2mbSU
/ipyRIgRgPT2+PlaQ6fp+NQGMPe8bvFfHLwPXiddTTTrTqZOWLOxd8bEZDxq4GwrCALC1Rdf6awA
WsL+CZ8i+ALgFiPwwHqglY9H5pz9HlNGYDX8m7EZLY3dx3cWMdiOjkQO4xmCwSM4vwEm2jkwAidW
h2mum0OWONCgSqQZRqQbmjbcikuo4SLqql8SeMslkewk7NCBNt96FtyfbTbOUAtFOoDRIE3i2akW
bICql1vqUUhHm8e1m1mj7eOJi7A2Qn4pMEzfvmYNfNLNcVjiK5R5h0NeFBYtbXehbMY1L1Li4LRL
p90TBOZTinKl0BVSa5VMKRkTmQ7MbMn3EiTGWDsfYAxM48YMHhxxmnm4K55lOUfMZi8DVgWBJuxG
T1z0kdJFcHIho3ETmclnM3k2T8BJdopLH34JKY7k7kzJEE0KBKpCE2g5yFCOB6bhRwmJRREcE8Yn
E48Gbit+MwGNMujjo4YnbykN4nW0N3H3HhbYeysXe2xWTAkger2vjyRRQVXumtfi1Snxjz3TAE4B
cEFVCKwFWQyGaEWLJsqE9N0KNddvXaa+fXa/fPttR7k7kVEPLyMrURaRFR7DHSTSXFgy8p858p5b
Mzhxk3RN4G0kueUkpIkOHwCsRpL7cfLX9O/Wn08QW3u8q3YJl6cf2Cf1r1cVnP4/zx/i49eS8Wmm
1k1ixn9/9SPbMHX7lI0p94cb6vjW1awrpthQXsS7EgnN99sN98Erc0304twkWkJWoC0jLvn+W7W7
fL9/5RxOuJR9e8Bg3jhI3gaSU4+tSkiREjAawhU9T8ZgMZfd43eK+OXievE65menOnUycj2Vib3W
KzHjFwFhWCAWGKi6/wBNYALWF/BM/a8QLgFiPwwHqglY9H5mDr6v5N2Iyaxu7ju4sY7ENG4gdxjM
FgsZxfx+S7RyYA/Oqw3TXTaHLHWhQJVIMo1ItzRtuRSXUcJF1VS+JPGWyyPAxAm2+8+25RtntpKA
WinUBomCbRVRSLNkBVSa3VIKQjzaPay6zx9vHUxU/wBkLAFJgmb5+zBrqJZrisMRXKPMOhrwEXLW
13n21F9S9S4wD0T6ftFAa8pwwlThAVUmuNSikZE5kGzGzMgcyFMXYOx9gCkjj1iwdnF801FyueZT
6nmYzF4HsPzh/TQ/4fEPDb68E+G/Lu9/+qeeffIv6355fm6/XfIX3j52/iP+zPsXkXuny1/heH+2
fYn9lR2ju1cvI7huCMwGNsujjo4YnbxkF4nWsN3H3HhjYezsXe2xWY8kkfr2vj6BRUXX+m1fi1Sn
xj33TgCuBxIKKEVgKshkEzjS3ZVlQrpwhRrrt67DXz67Xb69tqPcfcmoxpaRlqiLSIsPH46SaSYs
GXlPnPMp5bszhtk3RN4mkkvVJGSJDh0AtaXY1z8tf079acevg1u95NvQbn0x/sE+rTq4rSfAOPXn
n093i012tmsWL/v9HI9qwdfR9ZGlfvDgzW1awrpuhQbsR7EgvN9dsN9e427pzpxbpEtIi1PlpCXe
v8t292uX7/yjideUfEPAYN44SNpm0lP4qUkSIkYAEOn6ejEBjL7vH7xnxy8D94nHUz0606mTpgzs
XfCxmTAYsBsKwgC4tYXX+msAFrC/gmXIvEC4BYj8MByoJWPR+ZgoBX0m7EZLY3dx3cWKciGjkQO8
RmCwSM4PwEl2ik0ffnVYbptptDljrQoEqkGUakW5o23IpLqOEi6qpfEnjLZZHiTrBCBFt959tSjb
TbSUAdFOn/RME3iyikWboCql1vqMS3jzaP6zazR5vHExdgbIT8WmCZvn7IGuolmuKwwF8o8w6GvL
bOMsly9td6FtRfUvUuLgtE+n7RMEvKcMZQ4QFVJrjUotGRupDsxsufcyFIXP9j7AFpnHjBg8OL5p
ZrlM8ynKPmMxeEasCfyXsRktcdJHSPXJ2IaORA5lJ5zOJNk/ASXaOSgCAJGY7lblzJIG0KA6pCk2
Y9yFCuR6ThRwkJRSEYk8YnE4/G6g7guGMwGNsujjo5YnLxkN4nW0M3G3GhjVgysbe2xWTAigfr2v
T6JNUXX2mtfDFifxr3gpiBcAcX6ipFUCrIZDNBcLtk2TCum+Emuu3rtNfPvtevr26o9yNyKjHGJG
WqItIi48fhpJpJiwY+U+c+U8tmZw2ybpG8TaSfPKSMkSHDoAUfdp+nerT6eINb3eTb0G55449QE+
rPq3rOfAPp5y9fdotNdrZrFi37/6ke1Huv5kZV+8MHWtawrpvhIXsR7EQ3N99sN9+5W7pzpxbpIt
Iy1QFpEVfv8Andndrkg+5k/E68p+veAgbxwkbTNpKfxUpIkRIQEIVP09GYDGX3eP3jvjt4nrxOup
pp1pzM3LBnYu99ish41YBYVggVxiwuv9Na/FrC/gmXteIFwCxH4YDlQKsej80Ar+v5P2ISWxu7ju
4sY5ENG4gdxjMFgkZxfx+TbRSaPvzysO0102hyxtoUCVSEKNSDc2bbkUnCrhIuqqXxJ4y2WR6+nv
P6f2AQItzvPtqUbabZygFop0/wCiYFvFlFYs3QFVJrfUgpCPNo9rLrNH28cTFz/ZCwBSYNm+fswa
6iSi4rDEVyjzDoa8envP6f2EXLW13oW1F9S9SowC0T6ftEwTiU44SlwiLqXXKpRaMjcSHZjZmQOJ
CmLsDZCwBaR14wHvDjjNNRwVzzKco+YzF5JroHYNgyXsQk1cdJHSRXJ2IaORA5lJpzOJNk/j8l2j
ksefg0pjuTuVMkQbQoDqgKUZj3AUK5HpuFHCQhJIRwTxicTj1uaroC37gjcAjLLo46OGJy8ZDeJx
tDdx9xoY2YMrF3ssVmPJInq9r0+gUVFV9prX4tUpw8ee6YgXAHggooRWAqyGQzR6e8/p/Yu2VZMK
6b4Sa67eu0zxfPa9fXttR7kbkVEOLSIvURaRlmDDHSTSTFgP5lHM58p5bMzhtk3SNYHEkueUkZIk
OHQB6e8/p/YOea0/Tv1p9Pg1vd5NvQb6MePYJ9WnVvWc+j/088/SWi012tmkWLenr/Uj2o91/MjS
n3h79vl4KKtq1hXTdCgvYj2Iheb77Yb69xt3TjTi3CJWRlqgLSMq+fc7tbtcv33Mn4nXlHK7wEDe
OEjaZtJT+KlJEiJGAvb5eCkCnqejMBjL3vH7xnp28T14nXMz0505mTkezsTfCxWY8auAsKwQK4xY
XX+msAFrC/gmXteIFyCxH4YDlgSsej807tN1/AJL2ISWx+7ju4sU7EdG4gdxjMFgkZxfx+TbRSUA
/PKw7TXTeHrHGhQJVIMo1ItzRtuRScKOEi6qpfEnjLZZHuHt8vDNEJE213n23Kds9tJQC0U6gNEw
TaKqqRZugKqTW6pRSEebR/WXWaPt44mLsDZCwBSYJm/fswa6iWa4rDAVyjzDoa85pOwLFra70baj
GpepUXBaJ9P2iYJxKcMJU4QF1LrjUgtGROZDsvszIHEhTFWFsfYIxI48YMHhxxmlmuVzzKco+YzF
4NxYNhSXsRktc9I/SPXJyIaORE7lJ5xN5Nk+j8l2iksffgkZhuVuXMkQbQoDqgITaD3AUK5HpuFH
CYhJIRiTxicTj3T2+XhaO4bgjVfxpl0cdHDI9eUivI62hm4+48MbD2Vib22KyYEkT9eV6eRKKi6+
01r4WqU+MecFMAK4HEgooQWArSGQzPPHjPLYW7LsmE9N8INddvXaa+fXa9fXttR7k7k1EOLSItUR
aRlx7DHSXSTFgyylPnPlPLZmcOMm6RvE2kn6pIyRIcOgEmMZqUmGPWtP071af8DW/wB5VvQbn04/
sE/rPq4rOfgefpy/zaLTXa2axYt6evo5Hth7r+ZGlPvDMPpWtbQrpthIbsR7Egvz77Yb64I27pxp
xbhAtIi1PlpEVfP+d2t2uX7/AMp4nPEpyXeAwbxwkbTNpKfxUpIiRIwFEXNQ6Ofp+oIzAIy+7xu8
Z8cvE/eR11NNOdOpk6HsrF3vsVmPGLgbCsEAsMVF1/prABawv4Jl7XgBcAsR+GA5UCrHo/NLOk05
zFaCvq/k3YjJrG7uO7ixjsQ0biB3GMwWCxnF/H5LtHJgD86rDdNdNocsdaFAlUgyjUi3NG25FJdR
wkXVVL4k8ZbLI9AxAm2+8+25RtntpKAWinUBomCbRVRSLNkBVSa3VIKQjzaPay6zx9vHUxU/2QsA
UmCZvn7MGuolmuKwxFco8w6GvAyXLW13n23F9S9S4uC0T6ftEwTiVY4SpwgKqXXKpRiMjcyHZjZm
QOJCkLsDY+wBaZ14wYOzi+aWa5TPMpyj5jMXgasCwZN2ISauOkjpIrk7ENHIgdyk04nEmyfx+S7R
yUA/BIzHcrcqYog2hQJVIUo0HuQwVyPTcKOExCKQjEnjE4nHg9W/L9/T/wD+535jX0+v5f8A81vq
I16/jU9Pnv8AmL+2/PX/AJGfT7l/q/GvFPT+u8n/AOO/2t6fL+Wq/dxFlWVCem+Emuu3rtNfPrtf
vr22o9ydyKiHFpGWqItIizBhxpJpLwwY+U+c+UZN2Zw4ybom8TiSXPKSMkSHDoBlocfLX9O/WnP+
4tvd5VvQXnnjj1AWBWfVxWk/Ac/Tzx/d4tNdrZrFiv7/AOpHtR7r0/iRpT7w7pfgKta2hPTdCg3Y
j2Ihvn12w33yRt3TjTm3SJaRFqgKyIs/f5bt7tcvyHlHE78o5XeAgbxwibTNpKfxUpIkRIwDft8v
BSFT9QRmAxp73jd4z07eMgvE46menGnUzcj2Vib3WIzYDFgFhWEAWGKi6/01gAxYX8Ez9rxAuAWI
5PAeqBVj0fml9rytHX8AknYfJrG7uO7mxzsQ0biBzCMwWCRnF9H5LtFJgD88rDtNtN4asdaFAtUg
yjUi3NG25BJdRwkXVVL4k8ZZLI9Pb5eE60BAq2u8+25RtntpKAWinUBooCbxXNSLNkBVSa4VIKQj
zeP6y6yx5vHEhdgbIWAKTBs3z9mDXUSzXFYYCuUeYdDXmcZxy6ALl7a7z7bi+pWpUXBaJ9P2igNx
KsMJSugKqXXGpRSMicSHZjZmQuZCkKsLZCwBiZx4wYPDjhRJRcpnmV5R8xmTzIxYFgyXsQk1cdI3
SPXB2IaNRI5lJpzOJNk/ASXaOSx9+CSmO5W5MyRBtCgaqQpRoPchQrgem4VcJiEkhGJPGJxOPaiL
mnM7guCNQCNMejjo3ZHbxkN4nWsM3G3GhjYezsbe2xWTAkjIK8r0+gTVFV9ptX4xUpw8e+6YgXAH
ggooRWAqyCQTSU1lC7ZVlQjpvhRrrs67TXN99r188jqj3I3IqMcXkZeoSsiLDx+Okmk2LBj5TxOf
KeW7M4bZN0TeBtJL1SSkiQ4fACaeWv8AFWn6eCtPp8Gt7vKt6Dc/R/iAT+tOritJ9H/38/SWi812
tmsWL/v/AKke1Huv5kaU+8Pft8vDdFW1bQvpuhQTsR7EQ3N99sN98Ebc0404twiWkRaoC0iLvn/O
7W7XL995R51zKeV3gIG8cJG0zSSnH1qUkSIkYDmOM9dPiIVPVBGYDGX3eP3jvjt5H7xOuZpp1pzM
3LBnYu+Fish4xYFYVggVhqwyv9Na/FrC/g2XteIFwCxH4YD1QSsej0017fLwUdf1/JuxGTWN3cd3
FjHIjo3ETuMZgkEjPD4BJto5KAIHlYbprpvD1jrQoEqkGUakG5o23IpOFHCRdVUviTxlksj2KjvH
1+wYkRbfefbco2020lILRTqA0TBNosqpFmyAupdb6kFIR5tH9ZtZ483jiYuf7IWAKTBM3z9mDXUS
zXFY4CuUeYdDXkCLl7a7z7Zi+pepcWB6KdPuiYJzKccJS4QFVLrjUopGROZDszsxIHMiTF2BsfYA
tI48YD3hxdRPNwVzzKco+YzF5rGcseoLAsKS9iEmrnpI6SK4OxHRyIHcpNOZzJuX8fku0clj78Ej
MdytypkiDalQdUBSbQc4ChXA5Nwo4TEJJCOCeMTice17fLwRrFncFwxmAxtl0cdHDE7eMhvE62hm
4+48MbD2di72WKyHkUT9e16fQKKiq+01r4YoU+Me+6YgVwXBBRQisBVkEgmj2+Xgpcsqy4T03wk1
129dpr59dr19e21HuTuTUQ0vIy9RlpEWHsMdJNJcWDHynic+U8t2Zw2zbom8TSSXPKSMkSHDoA9v
l4KHOVafp360+nwW3e8m3oN9HHHsE/rTq4rOfR/n6cv82i012tmsVK/v/qR7Ue6/mRpT7w3t8vBR
VtWsL6boUF7EexEN8++2G+vcbd0403twkWkZaoSsiLPX2W7W7PL595RxOvKOV3gIG8cIm0zaSn8V
KSJESMB7tINP09GYDGX3eP3jvjt4nrxOupppzp1M3LBnYm+Fish41cBYVggVxiwyv9Na/FrC/gmf
teAFwCxH4YDlQSsej80B1/X8m7EJNY3dx3cWMdiOjkQO8RmCwSM8P4/Jto5NH355WHababQ5Y41K
BKpBlGpBubNtyKThVwkXVVL4E8ZZLI8CEibb7z7blO2m2koBaKdQOigJvFVM4s2QFVLrfUgpCPNo
/rNrLHm8cTF2BshYApMEzfPmYNdRLNcVhgK5R5h0NeAzJa2e9C2YvqXqXFweifT9omCcSlPCUroC
qk1xqUWjInUg2Z2ZkDiQpjLA2PsEWmceMGDw4vmlmuUzzK8o+YzF55p4zx3YmNBPrBk3YfJq36R+
kiuDkR0ciJzKTzicSbl/H5LtHJQBAEjMdytyZkkCaEwlUBCbQeuFCuR6bhRwkISSEYk8YnE49Eie
gt+4I1AI0y6OejdicvGQ3icawzcbcaGNh7Oxd7LFZsCSJ+va9PoFFRVfabV8MVKfGPfdMQLgFi/U
UIrAVZDIZp6nRdsqyoT03wkz13ddpr59dr19e21FuVuTUY4vIy9RF5EWHsMdJNJcR7LmU+c+Uctm
Rw4zbom8DaSXqklJEhw6Ac+HGeN2kDnmtP08FZ8evgtvd5VvQb6Ppj8/rTq4rOfgOefXn193i012
smsWLfT/AO0j2o91/MjSn3h4/wDSK5X3Z5bM1tWsK6bYUG7EuxIL8++2G+/crd0304t0kXkRWoS0
jLvn/O7W7XL9/wASjzryj4h4DBvHCJvA0lnx9alJEiJGA4YRKeqCMwGMvu8fvFfHbwP3iddTTTnT
qZOWDOxN8LFZDxi4GwrCAKjFRdfaawAUsL+DZe14AXALEfhgPVAqx6PzTrx4TE26RrqUAr+TdiEm
sbu57uLGOxDRuIncYzBoLGsX4GTbRyaPEDysO0202hypxoUCVSDJtSLc0bbEEl1HCRdVUviTxlss
j2OWnKWOW4ECbb7z7blO2e2koBaKdQGiYJtFc1Is2QFVJrfUgpCPNo/rNrNH20cTFT/ZCwBSYJm/
fswa6iWa4rDEV9TzDoa8yjJctbXefbcX1L1Li4PRPp+0SBuJTjhKXCAqpNcqmFoyNzIdmNmJC5kK
QqwNkLAFpnXjBg7OL5pZrlM8ynKPmMxeA7AsCTdiElrjpI6R65Ow/RuIHc5NOZxJcn8eku0clAPw
SMx3K3KmKIJqUCVQEKNB7kMFcD03CrhMQikIxJ4xOJx4HcFwRmARtj0c9HLI5eMivE61hm424sMb
MGVib22KzYEkT9e18fQKKi6+01r8YqU+Me8FMQLgFi/UUIrAVZBIZmHq3/To0r/uj+YpRvnPp+Cz
yD3KWeA/nLf+OH4W/rvlR7P8i/w6/Zn33yHyr5l/wvafc/sT+ytPk1f7uG+7X9PBWnpz4LbveVbs
F+j/ACCfVp1cVpPo/wAfTl9JeLTXa2ZxYvxx6/1I9swdfzI0p94fT258MzFaSK0raGdN0KDdiXYi
G5vvthvv3G3dN9N7cJFpGVqAtIi71/lu3u37g+5lHE78p5XeAwbxwibwNpKfxUpIkRIQHpjx7Q6o
NP09GoBGX3eP3jPjt4n7xOuZppzp1MnLBlYm99iM2AxYDYVggFhiouv9Na/FLC/gmfteIFwCxHYY
DlQSsej000p19X8n7EJNY3dx3cWMdiOjkQO4xmCwWM4v4/JtopKAIHlodprpvD1jjUqEqgGUakG5
o22IpLqOEi6qpfEnjLJZHgBIi2+9C25TtptpKAWinUBooBbRZRSLNkhVS631GKQjzaPay6zR9vHk
xU/2Qn4lIGzfv2YNfNLNcVjgK5R5h0NeE1ryC5i2u8+24vqXqXFwWifT9ooDcSrDGVOEBVSa5VKL
RkbmQ7MbNH3EiTF2BshYAxI49YD3h1xmnm4K55lOUfMZi854zhj1Oh2DYMm7EJLXHSR0kVweh+jc
QOZSaczmTZPwEm2jkoB+CSmO5W5UySBtCoSqAhNqOcBgrgcm4VcJiEkhGJTGJxOPY9vl4Yygrft+
MwCNMujjo4ZHLxkN4nWsN3H3GhbdgzsXe6xWTAkifr2vTyJRQVX+mtfDFSnxjz3TEE4BYv1FCKwF
WQyGaa48JibYXbKsmFdN8KNddvXaa+fXa/fXt9R7k7k1EOLyItURaRFh7DHSTSTEew8p858oybMz
hxm3SNYm0kueUkZIkOHwBXPvBoXpW36d+tf3QW3u8m3oL9HH9gn9adXFZz+Pc/T6ce7xaa7WzWLl
f3/1I9qPdfzIyp94ecJLarWtIV03QoN2JdiIXm/O2G+vcrd0303t0iWkZaoC0jLPn2W7e7XL9/zK
OJ15T9e8Bg3jhI3gbSU/ipSRIiRgPVEGnqejMBjL7vH7xn528D94nHUz0705mTlgysXe+xGY8YuA
sOwgCwxUXX+msAFrC/g2XIvgC4BYj8MByoJWPR+Z405flMKK/r+T9iEmsbu47uLGOxHRuIncYxBo
LGcH4CTbRSaPkDy0O0102hypxoUCVSEKNSLc2bbkUl1HCRdVUviTxlssj2so6ahiRNud6FtyjbTb
OUAtE+oHRMG3iqmcWbJCqk1wqQUhHm0e1l1mjzePJi5/sfYApMEzfP2YNdRLNcVhiK5R5h0NeY9v
y1lIMF7b7z7ai2pWpcXBaJdP2igNzKsE5U4RFVJrhUgpGRuZDszsxIHMiTGWBsdYItM68YMHhxfN
PNcpnmV5R8xmLye3K3y7FYFgyXsQktc9JHSRXB2IaORE7lJpzOJNk/j8k2ikoAgCRmO5W5cxRBNC
gOqQpVoPchQzkek4UcJCEUhGJPGJxOPbxnH/ALd2vV4FwXBG4BGmPRz0cMjl4yK8jrWGbjbjw1sP
Z2LvZYrJgSRP19Xp9AoqKr/TWvhapT4x7wUxBOAWL9RQisBVkMgmm1iKil2yrJhHThCTXXb12mvn
z2vXz7bUW5O5FRjy0jLVGWkRZgwx0l0lxHseZT5z5Ty2ZnDjJuiaxNpJc8pIyRIcOgFU+ea1/Tv1
px68Qa3u8q3oL68Y8+wT+tOris5/HuOeOf3lotNtrZpFi/7/AOqHtR7r+ZGlfvDBVtWsK6boUF7E
uxELxffbDfXuNu6cac24SLSIrUBaRFnr/ndrdvl8+8o4nXlPK70GDeOEjaZtJT+KnJEiJGAhCp6n
4zAow+7x+8Z8dvE9eJ1zM9OtOZm5YM7E3vsVkPGLALCsECuMWGV/prX4tYX8Gz9rxAuAWI/DAeqB
Vj0fmgOv6/k3YhJrG7uO7ixTsQ0biB3iMwSCRnF/H5NtFJo+/PKw3TXTeHLHGhUJVIQo1ItzRtuR
ScKOEi6qpfEnjLZZHgBIi2+8+25TtptpKAWinUBooDbxVRSLNkBVSa31IKQjzeP6zayx5tHUxdgb
IWAKTBM3z5mDXUSzXFYYiuUeYdDXgIuWtvvPtqL6lalxcFon0+6JgnEpwwlThEVUmuVSikZG5kOz
OzMgcSFMZYGx9gC0jrxgwenXGSebgrnmV5R8xmLwCwLBk3YhJK56R+keuDsP0biJzKTzicybJ/H5
LtFJY+/BIzLcrcuZIgmpQLVAMo0HuAoZwPTXVcJiEUhGJPGJxOPc+fGeVUzy2O4LhjUAjTHo56N2
Jy8ZHeJ1rDNx9x4Y2HsbF3tsVkwJIn69r0+iTVFV/ptX4xUp8a84KYgXALF+ooRWAqyGQzTWMdoY
4brtlWTCum+Emuu3rtN8X12u317bUe5O5FRji0iL1EWkZcewx0k0lxYMfKeJxzKuWzI4cZt0TeBt
JL1SRkiQ4fANOo+7X9PBWv08Qa3u8m3YLzzxj/YJ9WnVvWc+Afv5/wA2i012tmsWLenr6OR7Ue6+
j6yNK/eGBW1awrpvhIXsS7EQnN99sN9e4W7pxpxbpEtIitQFpEWfP8t2t2uXz/yjic+U/XvAYN44
SN4GklOPrEpIkRIwHHPjPKqZmLhAp+n4zAIw+7xu8Z6dvE/eJ11NNOdOZk5YMrF3usRkwGLALCsE
AsMVF1/prABiwv4Jn7XiBcAsR+GA9UCrHo/M+Dm1X9fybsQk1j93HdxYx2I6ORA7jGYLBYzi/j8l
2ik0ffnlYdptpvDljjQoEqkGUakW5o22IpLqOEi6qpfEnjLJZHvS6cfxIUHtvvPtuUbabaSgDop1
AaJg28VUUizZATUmt9SikY83j+sus0fbxxMVP9kJ+KwBM3z5mDXUSzXFY4iuUuYdDnnDn+Us8o1s
i5e2+8+2YxqVqVFwWifT7omDcSnDCUuERVS641MLRkbmQ7MbLyFzIUhdgbIWALTOPGDB4cXzSzXK
Z5lOUfMZi8yyLAsCTdiMmrjpH6R65Ow/RuIHcpNOJxJcn8fk20clAPwSMx3K3JmKINoUCVSFKNB7
kMFcD03CrhMQikIxJ4xOJx8HcFwRmARtj0c9HLI5eMivE61hm424sMbMGVib22KzYEkT9e18fQKK
i6+01r8YqU+Me8FMQLgFi/UUIrAVZBIZmFyyrKhXTdCjfXb12muL77Xr69uqPcjciohpaRl6iLSE
sPYY6SaSYsGPlPnPlGTdkcOM26JrE0kl6pIyRIcPgAcP/wBMxur/ALufMKjfmj8jfevAfmrU3/Op
/wCKn5d3xnzP8h+ev4OvvK979l8S/wBF+N9k+1f7W9Pl/K/d3Va1rC+m6FBexLsSC/Prtivr3G3d
N9N7cIlpEVqArIir99lu1u1y/fcSjmdcSjld4CBvHCRtM2kp/ESkiREjAfQkTrcoNPU9GYBGXveN
3jvjt4n7xOuZppzpzM3TBnYu99is2A1YBYVhAVxiwuvtNa/FrC/gmfteIFwCxH4YDlQSsej80Okc
onSBAK/k3YhJrG7uO7ixjkQ0biB3iMwWCRrF+Ak20Ulj5A8rDdNtN4cscaFAdUAyjUi3NG25FJwo
4SLqql8SeMtlkeEconYCBFtd59tyjbPbSUAtFOoDRME2iqikWboiqk1uqQUhH20e1l1mj7aOJi5/
shYApIGzfPmYNdRLNYVhgK5R5hsNeVoyxe2+9C2ovqVqXGAWifT9omEcSrHGVOERVS65VKKRkTqR
bM7MSBzIUxdgbIWALSOPGDB2cXUSzWKZ5leUeZjMXhJmosrAn8m7EJNXHSR0kVyciGjkQOcyacTi
TZEI/Jdo5LH34JKY7lblzJEE1KBaoCE2g5wGCuR6ThVwmIRSEYk8YnE49EyjadCuG4oxAY0x6OOj
hkdvGRXicawzcbceGNh7Kxt7rFZsSKJ+va9PoFFRdf6bV+MVKfGPeCmIFcDiQUUIqgVpDIZmpjCV
+yrJhPTfCjXXZ12mvn12vX1wOqPcncioh5aRFqiLyIuPYY6S6SYsGPMp855lHLZmcOs26JvE2kn6
pIyRIcOgEYLnmtP079a/8C293lW7Bfo4/sE/rPq3rOfgOfp+nkvFpptZNIsW/wDQ6HtR7r+ZGlPv
Dzl4lWq3rWFdN0LDdiXYkG5vvthvv3G3dN9OLdIlpEVqErIyz59zu1u1yQfcyjideUfEPAYN44SN
4GklOPrEpIkRIwHSINPU9GYBGX3eN3jvzl4n7xOuppp1pzM3TBnYu99iMh4xYBYVhAFxiwuv9NYA
LWF/BMva8QLgFiOwwHKgVY9H5oCr6v5N2ISaxu7fu4sY7EdHIgd4jMGgsaxfgJNtHJgD88rDtNdN
4csdaFQlUgyjUi3NG25FJdRwkXVVL4k8ZbLI9yqO3JTDiLb7z7bk+2m2kpB6KdQGigNvFVM4sg3F
1JrfUgpCPNo/rLrNHm0cTFWBshYApMEzfP2YNdRLNcVjiK5R5h0Nebxnj+IDJe2u8+2ovqVqVFwW
ifT9omDcShPCUuERVSa41KKRkTmRbMbMyBzIUxdgbIWCLSOPGA94dXzSzXK55lOUfMZi80gsCwJL
2ISauOkjpHrg7ENHIid5k84nEmzfx+S7RyaPvwSMx3K3LmSIJqUB1SFKNB7kMFcjknCrhIQikIxJ
4xOJx623nJXBcEZgEbZdHPRwxOXjIbxOtYXuNuPDGw9nYm9tiMmBJE/XteHkCioqvtNq/FrFPjXv
umIFwB4IKKEVQKsgkM0NxyidIXrKsmEdN8KN9dnXaa4vrtevr26o9yNyajHFpEWqItIS49hjpJpN
iwZcynic8ynluzOHGbdI1ibST55SRkiQ4dAOWPPuznBc81p+nfrTjnnwW3+8m3oL6ccf2Cf1p1b1
pPo/+/njjgvFprtbNIsW/wBaHth7r+ZGlfvD7OhVrW0L6boWG7EexEN8++2G+vcrd0403twiWkRW
oC0iLP3+W7W7fL995R515T9e9Bg3rhI3gbSU/iJSVIiRgAQ6fp6MwGMPe8fvGfHbxPXiddTPTnTq
ZuWDOxd8LEZDxq4CwrCALC1hdf6awAWsL+CZ+14gXALEfhgPVAqx6PTQCv6/k3YjJrG7uO7exjsR
0biB3iNQSCxnF+Bk20cmj786rDtNtNocscaFAlUAybQi3NG25FJwoukXVVL4k8ZZLI8DECLb7z7b
lG2e2kpBaKdQGigJtFVFIs2QFVJrfUgpCPNo9rNrLH28cTF2BshYApME0fP2YNdRLNcVhiK5R5h0
NeAi5e2u8+2ovqVqXFwWifT9omDcSnBOUuEBVSa5VMKRkTmQbMbMH3EhSGWDsfYItM68YMHh1xml
muVzzKco+YzF4DsCwJL2ISauekjpIrk5ENHIgdyk85nEmyfx+TbRSWPkAiMx3K3LmSIJoUB1SEJt
B7kKFcj0nCjhISiiI4J4xOJx4FcFvxqAxpj0cdHDI7eMhvE62hm4+40MbD2dib22KzYEkT9e17IE
CiouvtNq+GKlPjHnBTEEuBxfqKEVgKshkM0jHOdKXbJsqE9N0KNddnXab+fPa9fXt1Rbk7kVEOLS
MtURaRFmA/HSTSTFgx5lPM58oybMzhtk3RN4m0k+eUkZIkOHQA1FVpsfrWv6d+tOPXwW3u8q3oN6
8cc8x+f1p1cVnPo/z688/wCbxaa7WzWLFv3+jke2YOv5kaV+8OqK2raF9N0LC9iXYiG+ffbBfXuN
uacacW4RLSIrT5WRFnr/AC3a3a5fvvKOJz5Ry4eAgbxwkbTNpKfxUpIkRIwEINP0/GYDGH3eP3iv
jt4H7xPOpnpzp3M3LBlYm99is2AxcBYdggFhioyvtNYALXF/BMva8QLgFiPwwHqgVY9H5oGa+r+T
diMmsfu37uLGORHRyIncYzBoLGcX4GTbRyaPkDy0O0102h651oUCVSDKNCDc2bbEEnCjhIuqqXxJ
4yyWR7yuLYgRbfehbcp2020lALRTqA0TAtosopFmyAqpNb6kFIR9vH9ZtZo+2jiYqf7IT8UkDZv3
7MGuolmuKwwFco8w2GvPS6cPxIwXtrvPtqLalalRcFon0/aKAXEqwwlK6IupdcalFoyJzIdmdmJA
6kKQuwNj7BFpHHjBg7OL5pZrlM8yvKPmMxecOf5ynKehWBYEm7EZNXHSP0j1ydh+jcQO5SacTiS5
P4/Jto5KAfgkZjuVuTMUQbQoEqkKUaD3IYK4HpuFXCYhFIRiTxicTj+WDuC4IzAI2x6OejlkcvGR
XidawzcbcWGNmDKxN7bFZsCSJ+va+PoFFRdfaa1+MVKfGPeCmIFwCxfqKEVgKsgkMzC5ZVlQnpuh
Rvrt67TXz77Xr75HVHuRuTUQ0vIy9RFpEWHsMdJNJcR7HmU+ceUct2Zw4zbomsTSSXqkjJEhw6AA
eta/p360/wCBbe7y7dgv0cc+w2BWfVvWc+Afv+n3aLzXayaxYt6f6SPbD3X8yNKfeGHhv5VXfH7B
7z7Ttb7j4P8AmqeKeaXh7/8APj5leCe8/E/U+K/mPeLfbT2P3L5i+M/wPrPIv7F+xr7vV6ep+MwC
MPu8bvFfHLxP3icdTTTnTmZOmDOxd7rEZDxi4GwrBArDFRdf6awAWsL+CZ+14gXALEfhgOVAqx6P
zP0OYgFfyXsQk9jd3HdxYx2I6NxA7jGYLBYzi/ASbaOSx9+eVh2mum0NWONCoSqAZRqQbmjbYgku
q4SLqql8SeMslkerXGam5MSHtrvPtqUbabZygFon0/6KAm0WUUirdEVUmt9Si0I83j+s2s0ebx1M
XP8AZCfikgbN8+Zg1s0s1hWGIrlHmHQ14XjyiNyLl7a7z7ai2pWpcWBaJdPuiYJzKsMJS4QFVLrj
UotGROZDszszIHMhTF2BshYItI48YD3hxfNLNcrnmU5R8xmLwl+Z/wCfMWBP5L2ISauOkfpHro7D
9Gogd5k05nEmzfgJLtFJY+/BozHcrcqYogmhQJVAUo0HuQwVwPTcKuExCKQjEnjE4nHjU8omKFwX
BGoBGWXRz0cMTt4yG8jraGbj7jQ1swZWLvbYrNgRSP17Xp9AmoKr7TavxaxP4x7wUwAuAOL9RQis
BVkMgmZm7n1LllWTC+m+EmuuzruN/Pntevv26o9yNyKjGlpEWqEvISzAfxpJpLiPY8yniccynluz
OHGTdE1gbSS9UkpIkOHQAlz3P7tP079af8C2/wB5VuwXj04/sE/rTq3rOfAPX6fpLxaa7WTWLFv9
ZHtx7r6PrI0r94cQVpWsK6boWG7EuxILzffbBfXuNuab6cW6RLSIrUBeRlnz/ndrdrJ+/wDKOJ15
R9e8Bg3jhE3gaSU4+tSkiREjAQhU9T8ZgMYfd4/eK/OXifvE66mmnOnUzdD2Vi732IzYDFgFhWCA
WGKi6+02r8WsL+DZe14AXALEfhgOVBKx6PzMCv6+kvYhJrG7uO7ixjsQ0aiB3GMQWDRrF+Bku0cm
j5A8rDtNdNocucaFAlUgyjUg3NG25FJdRwkXVVL4k8ZZLI8DECba7z7alO2m2cnBaKdP+igJvFVF
Is2QFVJrfUglvHm0e1m1mjzeOJi5/shYApMEzfP2YNZRLNcVhiK5R5h0NeAi5a2+8+2ovqVqXFwW
ifT9ooCXlOGEpXQFVJrlUwtGROpDsxszIHEhTFWDshYAtM68YD3hxxmlmuUzzKco+YzF4DsCwZN2
HyauOkjpIrk7ENG4id5k05nEl5fx+SbRyUC/BJTHcncqZIg2hQLVIUo0HOQoVyPScKuExCKQnEnj
E4nHgVwXBGYDGmXRz0csjt4yG8TjWGbjbjQxqwZWNvZYrMeSSkFe16eQJqi6+01r8WqU+Me+6YgX
AHF+ooRWAqyGQzQLllWTCum+EmeuzrtNfPvtevrkbUe5W5NRji0jLVEWkJYewx0k0lxYMeZV5z5R
y3ZnDjNuibwNpJc8ppSRIcPgFamdbg/Wtf071aevpBrd7yrdgvrxx/YJ/WnVxWc/AfTzz/m8Wmu1
k1i5f/WR7Ye6/mRlT7w4yVbVpCum6EhOxHsRC/PvthvvglbunGnFukS0iK0+VkRZ8/53a3a5IP8A
mUedeU/EPQYN44RNJm0lP4iUkSIkYBbdc4Qafp+MwGMvu8fvGfnbxP3iddTPTnTmZOmDOxN77FZD
xiwCw7BALDFRlf6a1+LWF/BMva8QLgFiOwwHqgVY9H5oauO7VfV9JuxGTWP3cd29jHIjo3EDvEZg
sFjOL8BJdo5NH355WG6a6bQ9Y20KhapBlGpFuaNtyKS6jhIuqqXxJ4y2WR4XHchAi2+8+25Rtntn
KAeinUBomCbRdRSLNkBVS631IKRjzaPazazx9tHExU/2QsAUmCZvnzMIuolmuKxxFco8w6GvBcdw
YLW13oW1F9S9S4wC0U6fdFAbiU44SlwgKqXXKphaMjcyHZjZmQOZCkKsDY+wBSRx4wHvTjjNJRcp
nmU5R8xmLwxHLjGwsCwJN2ISauekjpIrk5D9G4gd5k04nEmyIAJNtFJQBAEjMdytypkkCaFAlUhC
jUe5ChXA9Nwo4TEJJCMSeMTiceWucC4LfjMAjTHo46OWRy8ZDeR1rDNxtx4Y1YM7F3ssVkwJIH69
r0+gTVFV9ptX4xUpw8e+6YgVwOJBRQisBVkMhmizOF6ybKhXTfCTXXb12mvn12vX17bUW5O5FRji
8iLVEWkJYeww0k0m4YMPKfOfKMm7M4cZt0TeBtJP1SSkiQ4dADPLlE7F92n6d+tPp8Ft3vKt+C8e
nH9gn1Z9XFZz8D/97n/N4vNdrZpFi3p6/wBSPbD3X8yNK/eGWOURFHWtawvpthQXsR7EQvF9dsN9
e425pvpxbpEvIitQlZEWevud2t2uX77yjic+U/EPAQN44RNpm0lP4qUkSIkYAtc4Qaep6MwGMvu8
bvGfHLxPXgddTPTnTqZuWDOxd77FZDxq4GwrCArDFhdfaa1+LWF/Bs/a8QLgFiPwwHqgVY9H5pWx
X1fyXsQk1jd3HdxYx2IaORA7jGYLBYzi/ASbaKTR8gdVhumum8OWONCgSqQZNsQbmjbcikuoukXU
VL4k8ZZLI8AIEW33oW5KNtNtJQC0U6f9EwTeK55xZsgKqTW+pBSEebR/WbWaPt46mLn+yFgCkwTN
8/Zg11Es1xWGArlHmHQ15xx9fhwBctbXefbcX1K1Li4LRPp+0TBuJTgnKnCAqpdcalFoyJzINmNm
JA4kKYuwNj7BFpHHjAe8OL5p5uCueZTlHzGYvOrrx/EWDYMl7EJLXHSP0kVydiGjcQOZSacTmTZP
4/Jdo5LH34JKYblblTJEG0KBKpDFGo5yFCuB6bhVwmISSEYk8YnE49w5/lLEzc2LguCMwGNMejro
5YnbxkV5HWsM3G3FhjZgysXe2xWbAkifr2vjyJRUXX2mtfjFSnxj33TEC4BYv1FCKwFWQSGZ5Rcs
qyoT03wo3129dpri++16++RtR7kbk1GOLSItUJaRFx7DHSTSXFgy8p858p5bMjhxm3RNYm0kvVJG
SJDh8ABc81r+nerTj6YLb3eVbsF49OPs/P606t6zn8f9OOefXkvFprtZNIuW9P8ASR7Ye6/mRpT7
wwdbVrCum6FBuxLsSC/Pvthvrglbum+nFuki8iK1CWkZd8+53a3a5fv/ACjzryj694EBvHCJvA0k
px9ajJEiJGAh+U/xJ96f/if5pefu/r+d17547XXn3iX/ACgfic9s9i86+RvhH2d8M+H8K+Wn9f7F
4j/W/sa+79I1/X8l7EJLY3dz3cWMdiOjcQO4xmDQaM4vwEm2ik0ffnlYdprptDlTrUoEqgIUakG5
o23IpLquEi6qpfEnjLJZHvQ5mIE233n23Kds9tJQD0U6gNEwTaLKKRZuiKqTW+pBSEebx/WXWaPt
o4kLn+yE/FJgmb5+zBrqJZrisMRXKPMOhrwAuWtrvQtuL6lalxcFon0/aKA3EpwwlK6AqpNcalFp
SNzIdmNl5A4kKQuwNkLBFpHXg8e8OL5pZuCueZTlHzGYvAVgWDJuxCS1x0kdI9cHYho3ETuUmnE4
k+T8BJdo5IAfgkZjuVuXMUgbQoFqgGUaDnIUK5HpuFXCQhFITiTxicTjwauC4YzAY2y6OOjlidvG
QXkdbQzcfcWGtWDKxd7bFZDySB+va9Pok1RVf6bV8MWJ/GveCmAFwCxIKKkVQKshkE0C3ZVlQnpu
hJnrs67DPN9dr19e3VFuTuRUI8tIy1RFpEVYMMdJNJMWDHmUcTjynluzOHGbdE1ibSS55SRkiQ4d
AAfPNafp36049eINbveVbsG+j6AM/rTq3rSfgOf3+vJeLTXa2aRYt6f6SPbD3X8yNK/eGCrWtYT0
3QkL2I9iQX599sF9+425pvpvbhEtIitPlpEWfPst2t2sn7/ynzrynlw8Bg3jhI2maSU4+tSkiREj
AAhU9T8ZgMYfd4/eM9O3gfvE66mmnWnUzcsGVi732KyYDFgFh2CAWGKi6+02gAtYX8Gz9rxAuAWI
/DAcqCVj8fmYFfV/JexGTWP3cd3FjHYho3ET2MZg0GjWL4DJdo5NHyB5aHaa6bQ5Y40KBKpBlGpB
uaNtiKS6jhIuqqXxJ4yyWR4GHE233n21KdtNtJQC0U6gNEwTeK5qRVugKqTW+oxSEebx7WbWaPN4
6mKn+yFgCkwTN+/Zg11Es1xWGAr6nmHQ14GS5e2+8+2ovqVqXFwWiXT9ooCcynDCVOERdS64VKKR
kbqQbMbMSBzIUxc/2PsEWmdeMB7w4vmlmuUzzKco+YzF4DsCwZN2ISauOkfpHrk7ENG4gdzk05nE
l5IR6S7RyUA/BIzHcncqZIgmpQJVAUq0HuQwVwPTcKuExCSQjEnjE4nHgLhuCMwCNMejjo4ZHLyk
N4nW0M3G3GhjVgzsTe2xWbAklIK9r0+iUVFV9ptX4xUp8a94KYgXAHF+ooRVAqyGQTQLtlWTCum+
FGuu3rtNfPrtevrgdUW5O5FRDy8jLVEWkRVgw40k0l4HseZVxOfKOW7M4cZtkTeJtJP1SRkiQ4dA
AOflp+nfrT1+wtv95VvQT144/sE/rTq4rSfx7n05/wBsRaa7WTWLlf8AWR7Ye5/mRpT7wwVa1pCu
m6FhuxHsSC/Prthvv3G3dN9OLcJF5EVqArIiz9/lu1u1kQf+UcTryj694CBvHCJtM0kp/ESkiREj
Aa1U1fRDp6n4zAYy+7xu8V8cvA/eJ11M9OdOZm5YMbF3usRmwGLAbCsECsMVF1/prABawz4Nl7Xi
BcAsR+GA5UErHo/MyV03Kvq/kvYhJrG7uO7exjkQ0biB3GMwaDRrh+Ak20clAPzysO0102hyx1oU
CVSDKNSDc0bbkUl1XCRdVUviTxlksjxfT5MQItvvPtuUbabaSgFop1AaJgm8VUzizdEVUmt9SCkI
82j2s2s8ebRxMVP9j7AFJgmb5+zBr5pZriscBXKPMOhrwenyRctbfefbUY1K1Ki4LRPp90TBuJTh
hKXCAqpNcakEoyNzIdmNmJA4kKQuwNkLAFJnXjAe8OL5pZrlM8yvKPmMxeD0+WrAsGTdiElrjpI6
SK4ORDRqIncpNOZzJ8n4CS7RyUA/BJTDcrcuYog2hQLVIUo0HOAoVyPScKuExCSQjEnjE4nHoyLh
uGNQCNMejjo4ZnbxkN5HW0M3G3GhjUezsXe2xmTAiier2vT6JRUXX2m1fC1Snxr3gpiBcAsSCihF
UCrIZDNAuWVZUJ6b4Ub67eu038++1++uB1RbkbkVGOLyMrURaQlx7HHSTSXEew5lPnHlHLZmcOM2
6JrE2kl6pIyRIcPgAL7s/wBO/Wn/AALb3eVb0G+jj+wT+tOris5+A5455+nkvF5rtZNYsX/1ke2H
uf5kaU+8MHW1awrpuhQXsS7Egvz67Yb69xtzTfTe3SJeRFahLSMu+f5bt7tcv3/Eo865lH17wGDe
OEjeBpJTj6xGSJESMACFT1PxqAxl93jd4z47eJ+8TzqZ6dadTJywZWLvdYjMeMXAWFYIBYYqLr/T
WAC1hnwbP2vEC4BYj8MByoJWPR+aAV/X0m7EZNY/dx3cWMdiGjcQO4xmDQaM4vwEl2jk0ffnlYdp
rpvDljjQoEqkGUakW5o23IpOFHCRdVUviTxlksj1b48q32IQItrvPtuU7abaSgFop1AaKAm0WUUi
zdEVUmt9SCm8ebx/WbWaPt44mKsDZCwBSQNm/fswayiWa4rDEVyjzDoa8N5eJMwWtvvPtuL6l6lR
cFon0/aJgnEowwlLhAVUuuNSikZE6kOzOy8hcyFMXYGyFgC0zrxgPeHF8081ymeZTlHzGYvM16r8
ORWDYMl7EJNXPSR0kVwdiOjcQO5yecTmTZkAEk2jkoB+CSmW5W5UxRBNCoOqAZRoOchQrgem4UcJ
iUUhGJPGJxOPadeP4lcNwRmAxtl0c9HDI7eMhvE61hm4240NbMGVi722KzHkkD9e16fQKKiq+01r
8YqU+Ne8FMQLkDi/zUIqgFZDIZpw5Tc25LdlWVCem+FG+u3rtM/Pvtevv26otyNyKjHF5EXqItIi
w9hjpJpLiPY+UcTnmUct2Zw4ybom8TSSfqkjJERw+AZVv1rX9O/Wn/A1vd5VvQXj6P7BP606t6zn
4D6Ofp93i012tmsXLf6yPbD3P8yNKfeGGa2raFdN0LDdiXYiF5vrtgvv3K3dN9OLcJFpEVqAtIy7
5/lu1u1y/f8AlHnXlHxDwIDeuETeBpJTj61GSJESMBCFT9QRmARl93jd4z07eB+8TrqZ6c6dTJyw
ZWJvdYrJgMWAWFYIBUYqLr/TWAC1hfwTL2vAC4BYj8MByoJWPR+ZhE/6hTuW/wDFD6rn2j55/ju9
8+W1oeA/JT/k/wDkj7Z5f4N+Bnzj7Pf4fh/dvmX/AHDyPy7+t/ZXXw1914OItvvPtqU7abaSgHop
1AaJgm8VzUizdEVUmt9SCkI82j2s2s0ebxxIVP8AY+wBSYJm+fswa6iWa4rDEVyjzDoa89DmZctb
fefbUY1K1Ki4PRTp+0UBuJSnhKXKIqpdcKmFIyNzIdmNmJA5kKQuwdj7AFpnXjAe8OL5p5rlM8yn
KPmMxeAWBYEm7EZLXHSR0kVwdiGjcQO5SacTmTZPwEk2jkoEgCSmO5W5UxRBtCgOqQhRoPcBArkc
m4VcJiEkhGJPGJxOPAXBcEagMaY9HPRwxOXjIbyPNYZuLuLDGzBlYu9tis2BJA/Xten0CiouvtNa
/GKlPjHvBTEC4BYkFFCKwFaQyGZhcsqyYV03wkz12ddprm++2C+vbaj3J3IqIcXkRaoS0hLD2GOk
mk2LBjzKeJzxKeW7M4cZN0TWBtJPnlJKSJDh0AA+7T9O/Wn/AANb3eVb0F/d/YJ/WnVvWk+Aenr/
ALXi012tmsXLen+kj2w91/MjSn3hgVtWsJ6boSG7EexIJ8+u2G+/cbd0303twkXkRWoS0jLPn2W7
e7WT99xKeJ1zKOXDwIDeOEjeBpJTj6xGSJESMBCFT1PxmAxh93j94z47eJ+8Trqaac6dTNwwZWLv
dYjNgMXA2FYIBUYqLr/TaADFhfwTP2vEC4BYj8MB6oFWPR+Zg6+r+TdiMmsfu47uLGOxHRuInsYz
BoLGcX4CTbRyaPkDy0O0102hyx1oUCVSDKNCDc2bbkUl1HCRdVUviTxlksjwAgTbfefbUp2020lA
LRTp/wBEwTeK5qRZsgKqTW+pBKEebR/WbWaPN46mLn+yFgCkwTN8/Zg11Es1xWGIrlHmHQ14CMFr
b70Lai+pepUXBaJ9P2iYNxKcMJS4RFVLrjUopGRuZDsxsvIHMhSF2BsfYAtM48YD3hxfNLNcpnmU
5R8xmLwCwLAk3YhJa46SOkeuTsQ0biJ3KTTicyfJ+Aku0clAPwSUx3K3LmSQNoUC1SEKNBzgIFcj
knCrhIQkkJxJ4xOJx4HcFwRmARtj0c9HLE5eMhvI61hm4240MbMGVi722KyYEkD1e16eQKKiq+02
r8YqU+Ne8FMALgDiQzzIqgFZDIZoFyyrJhPTdCTXXZ12Gvn32wX37dUW5O5FRji8jLVAWkRdgwx0
k0mxYMeZTxOeJRy3ZnDjNuiaxNpJ88ppSREcPgAP1rT9O/Wf0eC293k27Bvo459gn9adW9Zz4B6f
T/m8Wmu1k1ixX/0OR7Ye5/mRpT7wwVbVrCum6FBuxHsSC8X32wX37lbum+m9ukS8iK1AVkZZ8+53
a3a5IP8AmUcTnyn4h4DBvHCRvA0kpx9ajJEiJGAhCp6n4zAYy+7x+8Z8dvE/eJ11NNOdOpm5YM7F
3usVkPGLALCsEAsMVF1/prABaov4Nl7XiBcAsR+GA5UErH4/M63lGNdRX9fyfsRk1j93HdzYx2I6
NxA9jGYLBY1i/AyXaKTR8geWh2mum0OWONCgWqQZRoQbmjTcikuo4SLqql8SeMslkeJdTfECBFt9
6FtSnbTbSUAtFOn/AETBt4rmpFm6ImpNb6kFIR5tH9ZtZo83jqYqwNj7AFJgmb5+zBrqJZrisMRX
KPMOhryMmXLW33n21F9StSouC0U6ftEwbmVYYSlwiKqXXGphaMidSDZnZiQOJCkLsDZCwBaZx6wY
PDi6iWa5TPMryj5jMXgKwLAk3YhJq46SOkeuTsP0aiJ3KTTmcybJ/H5LtHJQD8ElMdytypkiDaFQ
tUhijQc4ChXI9Jwq4TEJJCMSeMTiceB3BcEagMaY9HHRyyO3jIbxOtYZuLuLDGw9lYu9tis2BJA/
XtfH0Caoqv8ATWvxipT417wUwAuQOL/NQiqAVkMhmgXLKsmE9N0JNddnXYa+ffbBfft1Rbk7kVGO
LyMtUBaRF2DDHSTSbFgx5lPE54lHLdmcOM26JrE2knzymlJERw+AAfdp+nfrT1+wtvd5VuwX6OP7
BP606uK0nwD/APeXi012smsWLen+lD2w9z/MjSn3hgVtWkK6boSG7EexIL8++2G++CVuab6cW6RL
yIrUJaRlnz7Ldvdrl+/8o4nXlHxDwGDeOEjeBpJTj6xKSJESMBCFT1PxmAxh93j94z47eB+8Trqa
adadTNywZWLvdYjNgMWA2FYIBUYqMr/TaAC1hfwbL2vEC4BYj8MByoJWPx+Zg6/r+TdiMmsfu47u
LGOxDRuIHcIzBYLGcX8fk20cmj788rD9NdNocsdaFAlUBCjUi3NG25FJdRwkXVVL4k8ZbLI8CECL
b70LblG2m2koB6KdP+iYJvFc84s2QE1JrfUglCPNo9rNrNH20cTFz/Y+wBSYJm/fswa+aWa4rDEV
yjzDoa8ALlba7z7ai+pWpcXBaKdP2iYNxKcMJS4QFVLrjUgtGROpDsxsxIHMhSF2BshYAtI68YMH
hxfNLNwVzzKco+YzF5bbzk7AsGTdiElrjpI6SK5Ow/RqInOZNOJzJcn8fkm0clAPwaMx3J3JmKIN
oVCVQFKNBzgKFcj03CrhMQkkIxJ4xOJx6MFcFvxqARpj0cdHDE5eMhvI61hm4240NbMGVi722KyY
EkD1e16fQJqiq+01r4YqU+Me8FMQLgFiQUUIqgVpDIZnXSOURFLdlWVCum+FGuu3rtNfPvtfvrkb
UW5W5FRjS0iLVCWkRdgP40k0lxHsuZR5z5Ty2ZnDjNuibxNJJ88pIyRIcOgHmZHrWn6d+tP+Bbe7
yreg37ueAE/rTq4rOfR/0+n6S0Wmu1k1i5X/AFke2Huf5kaU+8MCta0hPTdCg3Yl2JBub77YL79x
t3TfTe3CJeQlagLyMu+f5btbtcvyHlHnPlHxDwGDeOETeBpLPj6xGSJESMBCFT1PRmAxl93jd4z0
7eJ+8TrqaadadTNywZWJvdYrJgMWA2FYIFYYqLr/AE2gAtYX8Ez9rxAuAWI/DAeqBVj0fmYOv6/k
3YhJbH7uO7ixjkR0biJ7iMwaCxrF/H5LtFJo+QPLQ7TbTaHLHGhQJVIQm0INzRtuRSXUcJF1VS+J
PGWSyPBc/wCpWP8AkHvP5f2qnt/nPys8U8SrT2D8rP5ZeB/lnfEfJbyjwbyj7ae/+5ey+S/wPFvH
/wCxftb0+X8tdfn/AAimC1t959tRfUvUqLgtE+n7RME4lGGEpcIC6l1xqQWjI3Mh2Y2XPuZCkLsD
ZCwBaZx4wYPDi+aWa5TPMpyj5jMXndzFgWDJuxCTVx0kdJFcnYho3EDvMmnE5k2T8BJNo5KAfg0Z
juVuTMUQbQqEqgKUaD3AUK5HpuFXCYhJIRiTxicTjwFwXBGoDGmPRx0csjt4yG8TrWGbi7iwxsPZ
WLvbYrNgSQP17Xx9AmqKr/TWvxipT417wUwAuQOL/NQiqAVkMhmgXbKsqFdN8KNddnXaa+ffa/fX
t1R7k7kVGNLyMtUBaRF2DDjSTSXFgx5lPE58o5bszhxk3RNYmkk+eUkZIkOHwAF61p+nfrP6PBbe
7yrdg3/b7BP606uKzn4H/t+kvFprtZNYsW9P9KHth7n+ZGlPvCAratYV03QoL2JdiQX599sN9cEb
c0304t0iXkRWoC0jLvn2W7e7XL9/xKPOvKPiHgQG8cJG8DSSnH1qMkSIkYCEKnqfjMBjD7vH7xnx
28D94nXU00606mblgysXe6xGbAYsBsKwQCoxUZX+m0AFrC/g2XteIFwCxH4YDlQSsfj8zB19X8m7
EZNY/dx3cWMdiOjcRPYxmDQWM4vwEm2jk0fIHlodprptDljrQoEqkGUaEG5s23IpLqOEi6qpfEnj
LJZHgBAm2u8+2pRtptnKAWinT/omCbxVRSLNkBVSa31IJQjzaP6zazR5vHExc/2PsAUmCZvn7MGu
olmuKwxFco8w2GvADBa2+8+2ovqXqVFwWifT9omCcSjDCUuEBdS641ILRkbmQ7MbLn3MhSF2BshY
AtM48YMHhxfNLNcpnmU5R8xmLwCwLBk3YhJq46SOkiuTsQ0biB3mTTicybJ+Akm0clAPwaMx3K3J
mKINoVCVQFKNB7gKFcj03CrhMQkkIxJ4xOJx4C4LgjUBjTHo46OWR28ZDeJ1rDNxdxYY2HsrF3ts
VmwJIH69r4+gTVFV/prX4xUp8a94KYAXIHF/moRVAKyGQzQLtlWVCum+FGuuzrtNfPvtfvr26o9y
dyKjGl5GWqAtIi7BhxpJpLiwY8ynic+Uct2Zw4ybomsTSSfPKSMkSHD4AC9a0/Tv1n9Hgtvd5Vuw
b/t9gn9adXFZz8D/ANv0l4tNdrJrFi3p/pQ9sPc/zI0p94QOta1hPTdCwvYl2JBfn32wX17jbmm+
m9uEi8hLVAWkZd8+y3b3a5fv/KPOvKPiHgQG8cJG8DSSnH1iMkSIkYCEGnqfjMBjD7vH7xnx28D9
4nXU00606mblgysXe6xGbAYsBsKwQCoxUZX+m0AFrC/g2XteIFwCxH4YDlQSsfj8zB19X8m7EZNY
/dx3cWMdiOjcRPYxmDQWM4vwEm2jk0fIHlodprptDljrQoEqkGUaEG5s23IpLqOEi6qpfEnjLJZH
gBAm2u8+2pRtptnKAWinT/omCbxVRSLNkBVSa31IJQjzaP6zazR5vHExc/2PsAUmCZvn7MGuolmu
KwxFco8w2GvADBa2+8+2ovqXqVFwWifT9omCcSjDCUuEBdS641ILRkbmQ7MbLn3MhSF2BshYAtM4
8YMHhxfNLNcpnmU5R8xmLwCwLBk3YhJq46SOkiuTsQ0biB3mTTicybJ+Akm0clAPwaMx3K3JmKIN
oVCVQFKNB7gKFcj03CrhMQkkIxJ4xOJx4C4LgjUBjTHo46OWR28ZDeJ1rDNxdxYY2HsrF3tsVmwJ
IH69r4+gTVFV/prX4xUp8a94KYAXIHF/moRVAKyGQzQLtlWVCum+FGuuzrtNfPvtfvr26o9ydyKj
Gl5GWqAtIi7BhxpJpLiwY8ynic+Uct2Zw4ybomsTSSfPKSMkSHD4AC9a0/Tv1n9Hgtvd5Vuwb/t9
gn9adXFZz8D/ANv0l4tNdrJrFi3p/pQ9sPc/zI0p94QOta1hPTdCwvYl2JBfn32wX17jbmm+m9uE
i8hLVAWkZd8+y3b3a5fv/KPOvKPiHgQG8cJG8DSSnH1iMkSIkYCEGnqfjMBjD7vH7xnx28D94nXU
00606mblgysXe6xGbAYsBsKwQCoxUZX+m0AFrC/g2XteIFwCxH4YDlQSsfj8zB19X8m7EZNY/dx3
cWMdiOjcRPYxmDQWM4vwEm2jk0fIHlodprptDljrQoEqkGUaEG5s23IpLqOEi6qpfEnjLJZHgBAm
2u8+2pRtptnKAWinT/omCbxVRSLNkBVSa31IJQjzaP6zazR5vHExc/2PsAUmCZvn7MGuolmuKwxF
co8w2GvADBa2+8+2ovqXqVFwWifT9omCcSjDCUuEBdS641ILRkbmQ7MbLn3MhSF2BshYAtM48YMH
hxfNLNcpnmU5R8xmLwCwLBk3YhJq46SOkiuTsQ0biB3mTTicybJ+Akm0clAPwaMx3K3JmKINoVCV
QFKNB7gKFcj03CrhMQkkIxJ4xOJx4C4LgjUBjTHo46OWR28ZDeJ1rDNxdxYY2HsrF3tsVmwJIH69
r4+gTVFV/prX4xUp8a94KYAXIHF/moRVAKyGQzQLtlWVCum+FGuuzrtNfPvtfvr26o9ydyKjGl5G
WqAtIi7BhxpJpLiwY8ynic+Uct2Zw4ybomsTSSfPKSMkSHD4AC+7T9O/Wn0eDW93lW9Bf/0Cf1p1
cVpPgH7+f82i812smsXLen+kj2w91/MjSn3h4whbKtq0hPTdCg3Yl2JBfn32w317jbum+m9ukS8i
K1AWkZd8/wAt292uX7/yjideUfXvAYN44SN4GklOPrEZIkRIwFhBaFT1PRqAxl93jd4z47eB+8Tr
qZ6c6dTN0wZWJvdYjJgMWA2FYIFYYqLr/TWAC1RnwbL2vEC4BYj8MByoJWPR+Z5wks6+r6TdiMms
fu47uLFOxHRuIHcYzBoLGsX4CTbRyaPkDy0O0103hyx1oUCVSEKNSDc0bbkUl1HCRdVUviTxlksj
2sILIOItvvPtqUbabaSgFop0/wCiYJvFc1Is2QFVJrfUYpCPNo9rNrPH28cTFz/ZCfikwTN+/Zg1
1Es1xWGIrlHmHQ14wjyW9W/Od6k/93fytvut9fwNeB+5Gv8Ayk//ABT+K+N+enjv45fxjfeT7v7L
8b/ovmnvf2q/bnjP1p0+7yuwLCkvYhJq46SOkiuTsQ0ciB3mTTmcSbJ+Akm0clAEASMx3K3JmKIN
oVCVQFKNBzgKFcj03CrhMQkkIxJ4xOJx7u5FcNwRqAxpj0cdHDE7eMgvE61hm4240MasGdib22Ky
YEkD9e18fQJqi6/01r8WqU+Ne8FMALgDi/zUIqgFZDIZoF2yrKhXTfCTPXb12m/n32vX37bUe5G5
FRjS8iLVCWkRdgwx0k0lxYMfKfOfKOW7M4cZN0TWJtJPnlJGSIjh8AA+7X9O/Wf0eC293l27BfX0
59gn9adXFZz8D+/n15Lxaa7WTSLlvT/Sh7Ye6/mRpXj5hgVrWsJ6boWF7EuxMJzffbBffBG3dN9N
7cIlpEVqAtIyz59lu3u1y/f+UcTvyj4h4EBvHCRvA0kpx9ajJEiJGAhCp6n41AYw+7x+8V8dvE/e
J11NNOtOpk5YMrF3usRmwGLAbCsEAsMVF1/ptX4tYZ8Gy9rxAuAWI/DAcqCVj8fmYFfwCTdiElsf
u47ubGOxHRqIHsYzBoNGcH4GS7RyaPkDy0O0202hyxxoVCVSEJtCDc0bbkUl1HCRdVUviTxlksjw
AgVbXefbUo2020lALRTp/wBEwTeK5qRZsgKqTW+pBKEebR/WbWaPto4mKn+yE/FJgmb9+zBrqI5r
isMRX1PMOhrwAuXtvvPtuL6lalxcFon0/wCiYNxKMMJU4RF1LrjUgtGRuZDsxsxIHEhTFWBshYIt
M48YMHhxfNPNcpnmU5R8xmLwHYFhSXsQk1cdJHSRXJ2IaORA7zJpzOJNk/ASTaOSgCAJGY7lbkzF
EG0KhKoClGg5wFCuR6bhVwmISSEYk8YnE48BcFwRqAxpj0cdHLI7eMhvE61hm4u4sMbD2Vi722Kz
YEkD9e18fQJqiq/01r8YqU+Ne8FMALkDi/zUIqgFZDIZoF2yrKhPTfCjXXZ12m/n32v31yOqLcnc
ioxpeRFqhLyIuwYY6R6S4sGPMp858o5bszhxk3RNYm0k/VJGSJDh8ABetafp36z+jwW3u8u3YN+7
n2Gf1p1cVnPwHr9P+bxaa7WTWLFvT/Sh7Ye5/mRpXj5hgVrWsJ6boWF7EuxIL8++2G+/cbc0303t
wiXkRaoC0iLPn+W7e7WT5/5RxOvKPiHgQG8cJG8DSSnH1qMkSIkYCEKn6fjUBjL7vG7xnx28T95H
Xcz0506mTlgysXe+xWQ8YuBsKwQKwxUXX2m0AFqjPg2fteABwCxH4YD1QSsfj8zKK+r+TdiEmsfu
47uLGOxHRuIncYzBoNGcX4CTbRyYAQPLQ7TXTaHLHWhUJVIQo1INjRtuRSXUcJFlVS+JPGWSyPEM
QJtrvPtqUbabZygFop1AaJgm8VUUizZAVUmt9SCUI82j+sus0ebxxMVP9kLAFJgmb5+zBrqJZris
MRXKPMOhrwEXL213n21F9S9S4uC0S6ftFATiU4YSpwgLqXXCpBSMjcyHZjZg+5kKYqwNkLBFpnHj
Bg8OL5p5rlM8yvKPmMxeBqwLBk3YhJq46R+kiuTsQ0ciB3KTTmcSbJ+Akm0clAEASMy3J3KmSINo
UCVQFKNRzgKFcj03CrhMQkkIxJ4xOJx4M3DcEagMaY9HHRwxO3jILxOtYZuNuNDGrBnYm9tismBJ
A/XtfH0Caouv9Na/FqlPjXvBTAC4A4v81CKoBWQyGaBdsuyoV03Qk1129dpr599r19e3VHuTuRUY
0tIy1QFpEXYMMdJNJcR7HmU8TnyjluzOHGTdE1iaST55SRkiQ4fAAOea0/TvVn/wLb3eXb0F4+jn
2Cf1p1cVnPwP7+fpLxaa7WTWLlvT/SR7Ye6/mRpXj5hgVrWsK6boWF7EuxMJzfXbDffBG3dN9N7d
JFpCWp8tIy719lu3u1y/f+Ued+UfEPAgN44SN4GklOPrUZIkRIwEIVPU/GoDGX3eN3ivjt4n7xOu
pnp1p1MnLBlYu91iM2AxYDYVggVhiouv9NYALWGfBs/a8QLgFiPwwHKgVY9H5mDr6v5N2Iyax+7j
u4sY7EdG4iexjMGg0ZxfgJNtHJo+QPLQ7TbTaHLHWhQJVIQo1INzRtuRSXUcJF1VS+JPGWSyPACB
Ntd59tSjbTbSUAtFOn/RMC2iyikWbICqk1vqQUhHm8e1m1mj7eOJCp/shYApMGzfP2YNdRLNcVhg
K5R8OhrwEXLW33oW3F9StSouC0T6ftEwTiUYYypwiKqTXGpBaMjcyHZjZeQOZCkLsHZCwRaZx4wH
uzi+aea5TPMpyj5jMXgasCwZN2ISauOkfpIrk7ENHIgdyk05nEmyfgJJtHJQBAEjMtydypkiDaFA
lUBSjUc4ChXI9Nwq4TEJJCMSeMTiceBXBcEagMaY9HHRyyO3jIbxOtYZuLuLDGw9lYu9tis2BJA/
XtfH0Caoqv8ATWvxipT417wUwAuQOL/NQiqAVkMhmgXLKsuFdN8KM9dvXaa+fXa9fft1R7kbkVGN
LSItUJaRFx7DHSTSXFgy5lPnHlHLdmcOM26JvE2knzykjJEhw+AAeta/p3qz/wCBbe7y7egvHPpz
7BP606uKzn4H9/PryXi012smkWLf60PbD3P8yNK8fMMCtq1hXTdCgvYl2Jheb67Yb74JW7pvpvbh
EtIS1PlpGWfPud2t2uX7/iUcTvyj4h4EBvHCRvA0kpx9alJEiJGAhCp6n41AYy+7xu8Z8dvA/eJ1
1M9OdOpm6YMrE3usRkwGLAbCsECsMVF1/prABaoz4Nl7XiBcAsR+GA5UErHo/MwK+r+TdiMmsfu4
7uLGOxHRuIHsYzBoLGsX4CTbRyaPkDy0P0102hyxxoVC1SDKNCDc0abkEl1XCRdVUviTxlksjwMQ
ItvvQtqUbabaSkHop0/6JAm8VzzizdAVUmt9SCkI82j+s2s0ebRxMXP9j7AFJgmb9+zBrqJZrisM
RXKPMOhrwEXLW33n23F9StS4uC0T6ftFAbiUJ4ypwiLqXXGpRSMjcyHZjZeQOZCmKsDZCwRaZx4w
HvDi+aea5TPMpyj5jMXgeqfNP9Ml/ul4DePg3/l7+Q/DOvP/AJKf+Nn5vf13yC94+en4ivsx7N5D
7t8s/wCF4d7Z9if2x/pvKPrbyy4LgjMAjLHo56OWR28ZDeR1tDNxdxoY2Hs7F3usVkwJJH6+r0+g
UVFV/prX4tUp8a94KYgnIHEhnmRVALSGQzPbC9ZVlQrpvhJrrs67TXz77YL79uqPcncioxxeRFah
LSIuwYcaSaTcMGPlHE45lPw7M4cZN0TWBtJPnlJKSIjh0ADP3Z/p3q04/wBxbd7ybegvHPpz7DP6
06uKzn4D0/8ASXi012smkXLf6yObD3X8yNK8fMMCtq0hXTdCQvYl2Jhfn32wX37jbum+m9ukS0hL
VAVkZZ8+y3a3ayfv/KfOuZR8Q8CA3jhI3gaSU44VSkiREjAAhU/T8agEZfd43eM+O3ifvE66menW
nMycsWVi73WKyYDFgNhWEAVFqi6+01gAtUZ8Ez9rxAuAWI/DAcqBVj0fmYOv6/k3YjJbG7uO7ixj
sR0biJ3iMwWDRrB+Ak20cmAEDy0O0202hqx1oUCVSEKNCDc0abkUnCrhIuqqXxJ4yyWR4AQItvvO
tqU7abaygFop1AaJgm8VzUizdEVUmt9RikI82j2s2s0ebRxIVP8AZCfikwTN+/Zg11Ec1xWGIr6n
mHQ14AXLW13n23F9StSosB0T6ftFAa8pwwlLhEVUuuNSi0ZE5kGzGzEgcSFIVP8AY+wBaZx4wHvD
i+aWa5TPMr9T5jMXgKwLBk3YhJq46R+kiuTsP0biJzKTzmcybJ+Bk20UlAEASUx3J3KmKINoUC1S
EKNBzgKFcj0nCrhMQkkIxJ4xOJx4HcFwRmARpl0c9HLI5eMhvI62he424sMbD2Vi722KyYEkj9e1
6eRKKC6+02r8WqU+Ne8FMATkDiQzzIqgVpDIZmF2yrKhXTfCjXXZ12mvn32v317dUe5O5FRjS0iL
VAWkRZgwx0k0lxYMeZT5x5Ry3ZHDjJuiaxNpJ+qSMkSHD4AC5+Wn6d+teeefBbe7ybegvp9PICf1
p1cVlPwH7vTnktFprtZNYsW4/wDxI9sPdfzI0rx8wwda1pCum6FBuxLsTC/Pvtgvz3G3dN9N7cIl
pEVqAtIiz59zu3uzyQf5Sjzryjlw8Bg3jhI3gaSU4+tSkiREjAAg0/T0YgMZe94/eM+OXjILxOup
npzp1M3LBnYm99iMh41YDYVggFxiouvtNa/FrC/g2XteIFyCxH4YDlQKsej8zB19X8m7EZNY/dx3
cWMdiOjUQO4xmDQWNYvwMm2ik0fIHlYdprprDljjQqEqkGUakG5o03IJOFHCRdVUviTxlksjpSEC
Lb7z7blG2m2koBaKdQGiYNvFc84s2QFVJrfUopvHm0f1m1mj7eOJC7A2Qn4pMEzfv2YNdRLNcVhi
K+p5h0NdkMwWtvvPtuL6l6lRcFon0/6Jg15ThhKXCIupNcKkFoyNzIdmdmJA5kKQqf7IWCLTOPGA
94cXzSzXKZ5leUfMZi8BWBYEm7EJLXPSR0j1ydiGjcQO5SaczmT5PwEl2jkoB+CRmO5W5cySBNCg
WqQhRoOchQrkcm4VcJCEkhOJPGJxOPA7guGMwCMsujno4ZHLxkN4nWsL3G3FhbdgzsXe2xWbAkif
r6vT6JNUVX2m1fC1inxr3griBcAcSGeZFUCtIZDMwvWVZMK6b4Sa67Ou018++1++vbqj3J3IqMeW
kRaoC0iLD2HGkekuLBlzKfOeZTy2ZnDjJuiawNpJ88pJSREcPgAZ+7T9O/Wf0eC273lW7Bv/ANBn
9adXFaT6P+n0+vJeLTXayaxcr/6HQ9sPc/zI0px8wgK2rWFdN0KDdiXYkF+fXbBfXBG3NNdNrcJF
pEWqAtIyz5/lu3u3k/f8SnideUfXvAYN44SN4GklOOFUZIkRIwAIdP09GYFGX3eP3jPjt4yC8Drq
Z6c6dTNywZWJvdYjJgMWA2FYIFYYqLr/AE2r8WqL+CZ+14gXALEfhgPVAqx6PzMFX9fybsQktj93
HdxYxyIaNxA7jGYLBY1i+ASbaKTR8gdVh2mum0NVOtSgSqQhRqQbmjTcik4UcJF1VS+JPGWSyPAx
Ai2+9C2pRtptpJwWinUBomCbRVRSLNkBVS63VIJQjzePazazR5vHEhc/2Qn4pMEzfv2YNdRHNcVh
iK5R5h0NeAi5a2u9C24tqVqXFweifT/ooDcSnDCVOERVS641KLRkTmQbMbMSBzIUxc/2QsEWmceM
B7w4vmlmsVzzKco8zKYuwdgWBJuxCS1x0kdJFcnYho3DzuUmnM5k2T8BJNo5KAfgkZjuTuVMUQTQ
oEqkIUaD3IUK4HpuFXCYhJITiTxicTjwFwXBGYBGWPRz0csjt4yC8jraF7i7jQxuPZ2LvdYjIeSR
P19Xh9EoqLr7TavhapT4x7wUxAuAOJBTMiqBWkMhmYXbKsqFdN8KNddnXab4vvtfvr26o9ydyKjH
FpEWqAtIi48fjpJpLiwZeU+c+U8tmZw4ybomsTaSXPKSMkSHD4AB6Vp+ngrP/gW3u8m3oLxz9PsE
/rTq5rOfgP8A1+7Raa7WzWLlvT/SR7Ye6/mRpXj5hgVtWsK6boUF7E+xIL8+u2C+/cbd0203t0iW
kRaoC0jLvn+W7W7WT595RxOvKOV3gMG8cJG8DSSnH1iUkSIkYAEGnqfjMAjL3vH7xnx28ZBeJ51M
9OdOpk5YM7E3usRmwGLAbDsICsMVF1/prX4tYX8Gz9rxAuAWI/DAeqBVj0fmgOvq/k3YhJbH7uO7
ixjsQ0biJ7GMwWCxvF+Ak20Umj5A6tDtNdNocscaFQlUhCjUg3NGm5FJwo4SLqql8SeMslkeAECb
b7z7blO2m2knB6J9QGiYJvFVM4s2QF1LrhUgpCPNo9rPrNH20cTFT/Y+wBSYJm/fswa+aWa4rDAV
yj4dDXYIwXtvvPtqL6l6lRcFon0/6JgnMpwwlS6IqpNcKkFoyJzINmNmJA5kCQuf7IWCLTOPGA94
cXzSzXKZ5leUfMZi8AsCwZN2IyauOkjpHrk7ENG4icyk05nMmyfx+S7RyUAQAozLcrcqYog2hQLV
IQo0HOAoVyPScKuExCSQjEnjE4nHg9V/L9/T/f7nfmNf/wDP75rfDxv/AJ1PT57/AJjPt3z0/wCR
n0+5f6vxvxX/AE7yf/jv9uf+lcRZdlQvpvhRrrt67Tfz77YL79uqPcjcmoxxaRFagKyIswH46SaS
YsGHlPE58p5bszZxm3RNYG0k/wCEjJEhw6AdEH3afp360/4Gt7vJt2Df/oE/rTq5rSfgP/R6l4tN
drJrFyvp/pI9sPc/zI0p94QFbVrCum6FBexLsSC/Prtgvvgjbum+m9ukS0iLVCXkRd8/53b3ayfv
/KOJ15R8Q8CA3jhI3gaSU44VSkiREjAAhU/T8agMZfd43eM+O3jILxOupnp1pzMnLBlYu91iMmAx
YBYVhAVhiguv9Na/FqjPg2fteIFwCxHp4DlQSsej8zAr+v5L2ISaxu7nu4sY7ENGogdxjMFgsa4I
AJNtHJQBA6tDtNtNoaqcaFAlUhCjQg3NGm5BJwq4SLqql8SeMslkdAECLb7z7blG2m2koB6KdQGi
YJvFc84s3RFVLrdUgpCPNo9rNrNH20cTFz/ZGwBSYJm/fswa6iOa4rDAVyjzDoa8ALlrb7z7bi+p
epcYBaKdP+iYNxKcMZS4RFVLrjUgtCROZBsxsxIHEhSFWBshYAxM48YD3hxfNLNcrnmV+p5mMxdg
7AsGTdiElrjpH6SK5Ow/RqIncpNOpzJsn8fk20clAPwSMx3J3KmKIJoUC1SEKNB7gKFcj0nCrhMQ
kkIxJ4xOKR4FcFwRmARlj0c9HLE5eMgvI81hm4u40MbD2di722IyHkkT9fV4eRKKiq/02r4WqU+M
ecFMQLkDiQzzIqgFpDIZmF2yrKhXTdCjXXb12m/n32wX3yOqPcncmohpaRFqgKyEsPH46SaSYsGP
lHE58p5bsjZxm3RNYGkk+fqkZIiOHQAD1rT9O/Wn/Atvd5Nuwbj/ALAE/rTq4rOfgOf/AFlovNdr
JrFyv/ocj2w9z/MjSn3hg61rWE9N0KDdifYmE5vrtgvvgjbum2m9uES0iLVAXkRZ8+53b3byfvvK
POvKPiHgMG8cJG8DSSnH1iUkSIkYAEGn6fjUBjL7vG7xnx28ZBeJ11M9OtOZk5YMrF3usRkwGLAL
CsICsMUF1/prX4tUZ8Gz9rxAuAWI9PAcqCVj0fmYFfV/JexGTWN3cd3FjHYho1ETuMZgsGjWL8BJ
to5NHyB5aHababQ5Y40KhKpClGpBuaNNyKThVwkXVVL4k8ZZLI8DEiLa70LblO2m2knBaKdP+iYJ
vFc84q3RFVLrdUYpCPt4/rNrPH28cSF2BsjPxKYJm/fswa6iOa4rDEV9T4dDXgIuWtvvPtuL6lal
RgFon0/6KA15ThjKXCAqpdcKkFoyJzINmNmJA4kKQqwNkLBGJnHjBg8OL5pZrlc8yv1PMxmLwHYF
gybsQktcdI/SRXJ2H6NRE7lJp1OZNk/j8m2jkoB+CRmO5O5UxRBNCgWqQhRoPcBQrkek4VcJiEkh
GJPGJxSPArguCMwCNMejno5YnLxkF4nWsM3F3FhjYezsXe6xWTAkker6vT6BRQVX+m1fC1Snxr3g
piBcgcSCmZFUAtIZDMwuWXZMK6boUb67euw18++2C+/bai3J3JqMcWkRaoCsiLj2GOkmkuLBj5Tx
OfKeW7M2cZt0TeBtJPn6pGSJDh0AB+tafp360/4Gt7vJt2Dcf/kE/rTq4rSfgef3+vu0Xmu1k1ix
X0/c5Hth7r+ZGlPvCAratYV03QkL2JdiYT59dr99e427ptpvbhEtIS1QF5EWfPud292sn7/yjzry
j4h4DBvHCRvA0kpxwqlJEiJGAhCp+n4zAYy+7xu8Z8dvGQXiddTPTrTmZuWDKxd7rEZMBiwGwrCA
rC1Bdfaa1+LVGfBMva8QLgFiPwwHqglY9HpmBX9fyXsRktj93HdxYx2IaNxE7jGYLBY1i/ASbaOT
ACB1aHababQ5Y60KBKpCFGhBuaNNyCThVwkXVVL4k8ZZLI8DECLb7z7blG2m2koB6KdP+iYJvFc8
4q3RFVLrdUgpGOt4/rNrPH28dTFz/ZGfiUgTN+/Zg11Es1xWGIr6nmHQ14AXLW13n23F9S9S4uC0
T6f9EwTiU4YSlwgKqTXGpRaMicyDZnZiQOJCkKn+yFgi0zjxgweHF80lFyueZX6nmYzF4GbAsCTd
iElrnpH6R65OxDRqIncpNOZzJ8iACTbRSUA/BIzLcncqYog2hQNVIQo0HOAoVyPScKuExCSQnEnj
E4nHgdwXBGYBGWPR10csTl4yG8TrWF7i7iwxsPZWLvdYjIeSRP19Xp5AooKr/TavhapT4x77piCc
gcSGeZFUCtIZDMwu2XZUL6b4Ua67eu038++2C+/bqj3I3JqMcWkRWoCsiLMB+OkmkmLBh5TxOfKe
W7M2cZt0TWBtJP8AhIyRIcOgAHrWn6d+tP8Aga3u8m3YNx6/5BP606uazn4Dn/8Ay8Xmu1k1i5X0
/wBJHth7r+ZGlPvCAratoX03QoL2JdiYX599r99e427ptptbpIvIStQFpGWfP8t292snz/yjidcy
n4h4DBvHCRvA0kpx9alJEiJGABCp+n4zAYy+7xu8Z8dvGQXiddTPTrTmZuWDKxd7rEZMBiwGwrCA
rC1Bdfaa1+LVGfBMva8QLgFiPwwHqglY9HpmBX1fybsQktjd3HdxYxyIaNxE7jGILBoziQASbaOS
gCB1aHababQ1Y61KBKpCk2hBuaNNyCThRwkXVVL4k8ZZLI6DEiLb70LalO2m2koB6K9QGiYJvFc8
4s3QFVLrdUgpvHm0f1m1mj7aOJi5/shYApMEzfv2YNdRHNcVjiK5R5h0NeAi5a2u8+24vqVqVFwW
ifT/AKJgnEpTwlThEXUuuNSi0ZE5kGzOzEgcyFIXP9kLBFpnXjBg8OLqJZrlc8yvKPMxmLwCwLAk
3YhJq46SOkiuTsQ0biBzKTTqdSbkgAk20clAPwaMy3J3LmKINoUDVSEKNBzgKFcj0nCrhMQkkIxJ
4xOJx4C4LgjMAjLLo56OWR28JBeR1rDNxdxoY3YM7G3usVkwJIn6+r0+gUUF19ptXwtUp8Y94KYg
XIHEhnmRVALSGQzMPV/+nQpT/dH8xSjPOvX8FfkPuMr8B/OX9Pnd+Fv6/wCU/s/yN/Dp9l/fPIvK
vmV/B9o9z+xH7c/9K4T9P98tflruJ+Ffwb88nwaRfgW/ED7D8tPln7AL+Z34c/ffsr+K3xbyb18m
/t/t3tnp9mvO/wBuiOH6Kvkr+P8AvX8YPp+Zt9svwN/jT8s+Sv5gXucv+N/FJ8T96Hzz+aHtfsfx
39V7t7r9T9sfGP2DhdP/AJa/nanP+oL85+YHnRPzv5y+wfLT8RP1sa+U/wCIT4b7MfhT8W9PZvY/
sH8B499d9hPjv2A3A+Zf52wT/qDPOfAPOhnnnyZ9/wDlr+Hb66UfKf8AD78P9qPwp+Uf4PePZPt5
7f5B9b9vPjv2Duu9T51fmAUX+MH1/LJ+xv4G/wAFniXyW/L+9yiHx34Wfi/uu+enys9s97+O/pvd
vaPqfsb4x+wdz+oC+Zfy005/Cv4L+Rv4LHvwL/h+9/8Alp8zPYSvzO/Eb739qPxWeU+Tf4fJv6/4
D3P0+0vnf7B3faD6/lKar/lN/wDk7erH8V3sfp+Jb8anvAz4n8xH2v8A93PZPhfsn7/8F9V/aflt
+wL/APZ/ufyfv/ri/wDzrevjfPsf+VfR+Bn/ABeS/wCRfT4n/wC2f/FD9gXV/wD4vyldqfym/wDz
if6/8Vvvf/Mt+Cr3kn8V+Xd7Z9Hp6+Oe9/Cfaz3/AON+q/u/y2/YOF/T/fLX5a7ifhX8G/PJ8GkX
4FvxA+w/LT5Z+wC/md+HP337K/it8W8m9fJv7f7d7Z6fZrzv9g4foq+Sv4/71/GD6fmbfbL8Df40
/LPkr+YF7nL/AI38UnxP3ofPP5oe1+x/Hf1Xu3uv1P2x8Y/YOF0/+Wv52pz/AKgvzn5gedE/O/nL
7B8tPxE/Wxr5T/iE+G+zH4U/FvT2b2P7B/AePfXfYT479gNwPmX+dsE/6gzznwDzoZ558mff/lr+
Hb66UfKf8Pvw/wBqPwp+Uf4PePZPt57f5B9b9vPjv2Duu9T51fmAUX+MH1/LJ+xv4G/wWeJfJb8v
73KIfHfhZ+L+6756fKz2z3v47+m929o+p+xvjH7B3P6gL5l/LTTn8K/gv5G/gse/Av8Ah+9/+Wnz
M9hK/M78Rvvf2o/FZ5T5N/h8m/r/AID3P0+0vnf7B3faD6/lKar/AJTf/k7erH8V3sfp+Jb8anvA
z4n8xH2v/wB3PZPhfsn7/wDBfVf2n5bfsC//AGf7n8n7/wCuL/8AOt6+N8+x/wCVfR+Bn/F5L/kX
0+J/+2f/ABQ/YF1f/wCL8pXan8pv/wA4n+v/ABW+9/8AMt+Cr3kn8V+Xd7Z9Hp6+Oe9/Cfaz3/43
6r+7/Lb9g4X9P98tflruJ+Ffwb88nwaRfgW/ED7D8tPln7AL+Z34c/ffsr+K3xbyb18m/t/t3tnp
9mvO/wBg4foq+Sv4/wC9fxg+n5m32y/A3+NPyz5K/mBe5y/438UnxP3ofPP5oe1+x/Hf1Xu3uv1P
2x8Y/YOF0/8Alr+dqc/6gvzn5gedE/O/nL7B8tPxE/Wxr5T/AIhPhvsx+FPxb09m9j+wfwHj3132
E+O/YDcD5l/nbBP+oM858A86GeefJn3/AOWv4dvrpR8p/wAPvw/2o/Cn5R/g949k+3nt/kH1v28+
O/YO671PnV+YBRf4wfX8sn7G/gb/AAWeJfJb8v73KIfHfhZ+L+6756fKz2z3v47+m929o+p+xvjH
7B3P6gL5l/LTTn8K/gv5G/gse/Av+H73/wCWnzM9hK/M78Rvvf2o/FZ5T5N/h8m/r/gPc/T7S+d/
sHd9oPr+Upqv+U3/AOTt6sfxXex+n4lvxqe8DPifzEfa/wD3c9k+F+yfv/wX1X9p+W37Av8A9n+5
/J+/+uL/APOt6+N8+x/5V9H4Gf8AF5L/AJF9Pif/ALZ/8UP2BdX/APi/KV2p/Kb/APOJ/r/xW+9/
8y34KveSfxX5d3tn0enr45738J9rPf8A436r+7/Lb9g4X9P98tflruJ+Ffwb88nwaRfgW/ED7D8t
Pln7AL+Z34c/ffsr+K3xbyb18m/t/t3tnp9mvO/2Dh+ir5K/j/vX8YPp+Zt9svwN/jT8s+Sv5gXu
cv8AjfxSfE/eh88/mh7X7H8d/Ve7e6/U/bHxj9g4XT/5a/nanP8AqC/OfmB50T87+cvsHy0/ET9b
GvlP+IT4b7MfhT8W9PZvY/sH8B499d9hPjv2A3A+Zf52wT/qDPOfAPOhnnnyZ9/+Wv4dvrpR8p/w
+/D/AGo/Cn5R/g949k+3nt/kH1v28+O/YO671PnV+YBRf4wfX8sn7G/gb/BZ4l8lvy/vcoh8d+Fn
4v7rvnp8rPbPe/jv6b3b2j6n7G+MfsHc/qAvmX8tNOfwr+C/kb+Cx78C/wCH73/5afMz2Er8zvxG
+9/aj8VnlPk3+Hyb+v8AgPc/T7S+d/sHd9oPr+Upqv8AlN/+Tt6sfxXex+n4lvxqe8DPifzEfa//
AHc9k+F+yfv/AMF9V/aflt+wL/8AZ/ufyfv/AK4v/wA63r43z7H/AJV9H4Gf8Xkv+RfT4n/7Z/8A
FD9gXV//AIvyldqfym//ADif6/8AFb73/wAy34KveSfxX5d3tn0enr45738J9rPf/jfqv7v8tv2D
/KZ/9v8A/H/7P3//AMv/AF/tz/0r/9k=" transform="matrix(2.2691 0 0 2.2691 -80.7158 -5.3658)">

			</image>

		</g>

	</g>

	<g>

		<g id="XMLID_170_">

			<path id="XMLID_171_" class="st0" d="M109.8,408.5c0,0.8-0.3,1.4-0.8,2c-0.5,0.6-1.1,0.9-1.9,0.9c-0.3,0-1-0.2-1.9-0.6

				c-1-0.4-1.7-0.7-2.2-1c-0.5-0.2-1.1-0.3-1.7-0.3s-1.2,0.2-1.6,0.5s-0.6,0.8-0.6,1.4c0,0.6,0.7,1.2,2,1.8s2.7,1.2,4.1,1.8

				c1.5,0.6,2.7,1.4,3.7,2.4c1,1,1.5,2.4,1.5,4.1c0,1.5-0.4,2.8-1.2,3.9s-1.9,2.1-3.3,2.8c-1.4,0.7-3.1,1-4.9,1

				c-1.3,0-2.6-0.2-4.1-0.7c-1.4-0.4-2.6-1.1-3.6-1.8c-1-0.8-1.5-1.6-1.5-2.6c0-0.6,0.3-1.2,0.8-1.9s1.2-1,2-1

				c0.6,0,1.1,0.1,1.6,0.3c0.4,0.2,1,0.5,1.7,0.9s1.3,0.7,1.8,0.8s1.1,0.3,1.8,0.3c1,0,1.6-0.2,2-0.5c0.4-0.3,0.6-0.8,0.6-1.4

				s-0.6-1.2-1.9-1.8c-1.3-0.5-2.6-1.1-4.1-1.8s-2.8-1.4-3.8-2.5c-1-1-1.5-2.4-1.5-4.1c0-1.5,0.4-2.8,1.1-4c0.8-1.2,1.8-2.1,3.2-2.7

				c1.3-0.6,2.9-1,4.6-1c1.2,0,2.4,0.2,3.7,0.5c1.3,0.4,2.3,0.9,3.2,1.6S109.8,407.5,109.8,408.5z"/>

			<path id="XMLID_173_" class="st0" d="M135.1,408.3v16.2c0,1.3-0.2,2.3-0.5,3.1c-0.4,0.8-1.2,1.2-2.6,1.2c-0.9,0-1.6-0.2-2.1-0.7

				c-0.5-0.5-0.8-1.2-1.1-2.1c-1.7,2-3.8,3-6.2,3c-2,0-3.8-0.5-5.5-1.6s-3-2.6-3.9-4.5s-1.4-4.1-1.4-6.5c0-2.3,0.5-4.5,1.4-6.4

				c1-1.9,2.3-3.5,3.9-4.6c1.7-1.1,3.5-1.7,5.4-1.7c1.2,0,2.4,0.3,3.5,0.8c1.1,0.5,2,1.2,2.7,2.2c0.1-0.8,0.4-1.5,1-2

				c0.6-0.5,1.3-0.8,2.1-0.8c1.4,0,2.3,0.4,2.6,1.2C134.9,406,135.1,407.1,135.1,408.3z M118.3,416.5c0,1.1,0.2,2.2,0.6,3.2

				c0.4,1,1,1.8,1.8,2.5c0.8,0.6,1.7,0.9,2.8,0.9c1.6,0,2.8-0.7,3.7-2c0.9-1.3,1.3-2.9,1.3-4.6c0-1.1-0.2-2.2-0.6-3.2

				c-0.4-1-1-1.9-1.7-2.5s-1.7-1-2.8-1c-1.1,0-2,0.3-2.8,1c-0.8,0.6-1.4,1.5-1.8,2.5C118.5,414.3,118.3,415.4,118.3,416.5z"/>

			<path id="XMLID_176_" class="st0" d="M138.7,425.1v-17.8c0-1,0.3-1.8,1-2.4c0.7-0.6,1.5-0.9,2.3-0.9c2.1,0,3.2,0.9,3.2,2.7h0.1

				c0.7-1,1.3-1.7,2-2.1c0.7-0.4,1.5-0.6,2.6-0.6c0.8,0,1.6,0.3,2.2,0.9s1,1.5,1,2.6c0,0.7-0.2,1.1-0.5,1.5

				c-0.3,0.3-0.8,0.6-1.4,0.9c-0.6,0.2-1.3,0.5-2,0.8c-1.3,0.6-2.3,1.2-3,1.8c-0.7,0.6-1,1.5-1,2.6v10c0,0.7-0.1,1.3-0.4,1.9

				c-0.3,0.6-0.7,1-1.2,1.3s-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1C139,427.1,138.7,426.2,138.7,425.1z"/>

			<path id="XMLID_188_" class="st0" d="M176.3,408.3v16.2c0,1.3-0.2,2.3-0.5,3.1c-0.4,0.8-1.2,1.2-2.6,1.2c-0.9,0-1.6-0.2-2.1-0.7

				c-0.5-0.5-0.8-1.2-1.1-2.1c-1.7,2-3.8,3-6.2,3c-2,0-3.8-0.5-5.5-1.6s-3-2.6-3.9-4.5s-1.4-4.1-1.4-6.5c0-2.3,0.5-4.5,1.4-6.4

				c1-1.9,2.3-3.5,3.9-4.6c1.7-1.1,3.5-1.7,5.4-1.7c1.2,0,2.4,0.3,3.5,0.8c1.1,0.5,2,1.2,2.7,2.2c0.1-0.8,0.4-1.5,1-2

				c0.6-0.5,1.3-0.8,2.1-0.8c1.4,0,2.3,0.4,2.6,1.2C176.1,406,176.3,407.1,176.3,408.3z M159.5,416.5c0,1.1,0.2,2.2,0.6,3.2

				c0.4,1,1,1.8,1.8,2.5c0.8,0.6,1.7,0.9,2.8,0.9c1.6,0,2.8-0.7,3.7-2c0.9-1.3,1.3-2.9,1.3-4.6c0-1.1-0.2-2.2-0.6-3.2

				c-0.4-1-1-1.9-1.7-2.5s-1.7-1-2.8-1c-1.1,0-2,0.3-2.8,1c-0.8,0.6-1.4,1.5-1.8,2.5C159.7,414.3,159.5,415.4,159.5,416.5z"/>

			<path id="XMLID_191_" class="st0" d="M180,434.8v-27.1c0-1.1,0.3-2,0.9-2.6s1.4-1,2.4-1c1.7,0,2.8,0.9,3.3,2.7

				c0.6-1,1.5-1.7,2.6-2.2c1.1-0.5,2.3-0.8,3.6-0.8c2.2,0,4.1,0.6,5.7,1.8s2.8,2.8,3.6,4.8c0.8,2,1.2,4.1,1.2,6.3

				c0,2.1-0.4,4.1-1.3,6c-0.9,1.9-2.2,3.4-3.8,4.6s-3.5,1.8-5.6,1.8c-1,0-2.1-0.2-3.1-0.6c-1.1-0.4-2-1-2.9-1.7v8

				c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1S180,435.9,180,434.8z M186.6,416.2

				c0,1.8,0.4,3.3,1.3,4.7c0.9,1.4,2.1,2.1,3.8,2.1c1.1,0,2-0.3,2.8-1c0.8-0.7,1.4-1.5,1.8-2.5c0.4-1,0.6-2,0.6-3.1

				c0-1.1-0.2-2.2-0.6-3.2c-0.4-1-1-1.9-1.7-2.5s-1.7-1-2.8-1c-1.1,0-2.1,0.3-2.8,0.9c-0.8,0.6-1.4,1.4-1.7,2.4

				C186.8,414.1,186.6,415.1,186.6,416.2z"/>

			<path id="XMLID_194_" class="st0" d="M206.1,425.1v-28c0-1.1,0.3-2,0.9-2.6s1.4-1,2.4-1c0.6,0,1.2,0.2,1.7,0.5

				c0.5,0.3,0.9,0.7,1.2,1.3c0.3,0.6,0.4,1.2,0.4,1.9v8.9h0.1c0.6-0.8,1.5-1.3,2.6-1.7c1.1-0.4,2.2-0.6,3.4-0.6

				c1.7,0,3.3,0.4,4.6,1.1c1.4,0.7,2.4,1.7,3.2,3c0.8,1.3,1.2,2.9,1.2,4.7v12.5c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3

				c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1c-0.6-0.7-0.9-1.5-0.9-2.6v-11.4c0-1.5-0.4-2.6-1.2-3.3c-0.8-0.8-1.8-1.1-3.1-1.1

				c-0.9,0-1.6,0.2-2.2,0.6c-0.6,0.4-1.1,0.9-1.4,1.6c-0.3,0.7-0.5,1.4-0.5,2.3v11.4c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3

				c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1C206.4,427.1,206.1,426.2,206.1,425.1z"/>

			<path id="XMLID_196_" class="st0" d="M234.5,394.2c0.7,0,1.3,0.2,1.8,0.5c0.6,0.3,1,0.8,1.4,1.3c0.3,0.6,0.5,1.1,0.5,1.7

				c0,1-0.4,1.9-1.1,2.7c-0.7,0.7-1.6,1.1-2.6,1.1c-1,0-1.8-0.4-2.6-1.2c-0.7-0.8-1.1-1.6-1.1-2.6c0-0.9,0.4-1.8,1.1-2.5

				C232.7,394.6,233.6,394.2,234.5,394.2z M231.3,425.1v-17.4c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.6,0,1.2,0.2,1.7,0.5

				c0.5,0.3,0.9,0.7,1.2,1.3c0.3,0.6,0.4,1.2,0.4,1.9v17.4c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3c-0.5,0.3-1.1,0.5-1.7,0.5

				c-1,0-1.8-0.3-2.4-1C231.6,427.1,231.3,426.2,231.3,425.1z"/>

			<path id="XMLID_199_" class="st0" d="M241.5,425.1v-17.4c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.8,0,1.5,0.2,2.1,0.6

				c0.5,0.4,0.8,1,1,1.8c1.3-1.8,3.4-2.7,6.3-2.7c1.7,0,3.3,0.4,4.6,1.1s2.4,1.7,3.2,3c0.8,1.3,1.2,2.9,1.2,4.7v12.5

				c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1c-0.6-0.7-0.9-1.5-0.9-2.6v-11.4

				c0-1.5-0.4-2.6-1.2-3.3c-0.8-0.8-1.8-1.1-3.1-1.1c-0.9,0-1.6,0.2-2.2,0.6c-0.6,0.4-1.1,0.9-1.4,1.6c-0.3,0.7-0.5,1.4-0.5,2.3

				v11.4c0,0.7-0.1,1.3-0.4,1.9s-0.7,1-1.2,1.3s-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1C241.8,427.1,241.5,426.2,241.5,425.1z"/>

			<path id="XMLID_201_" class="st0" d="M289.1,408.3v16.2c0,1.3-0.2,2.3-0.5,3.1c-0.4,0.8-1.2,1.2-2.6,1.2c-0.9,0-1.6-0.2-2.1-0.7

				c-0.5-0.5-0.8-1.2-1.1-2.1c-1.7,2-3.8,3-6.2,3c-2,0-3.8-0.5-5.5-1.6s-3-2.6-3.9-4.5c-0.9-1.9-1.4-4.1-1.4-6.5

				c0-2.3,0.5-4.5,1.4-6.4c1-1.9,2.3-3.5,3.9-4.6c1.7-1.1,3.5-1.7,5.4-1.7c1.2,0,2.4,0.3,3.5,0.8c1.1,0.5,2,1.2,2.7,2.2

				c0.1-0.8,0.4-1.5,1-2c0.6-0.5,1.3-0.8,2.1-0.8c1.4,0,2.3,0.4,2.6,1.2C288.9,406,289.1,407.1,289.1,408.3z M272.3,416.5

				c0,1.1,0.2,2.2,0.6,3.2c0.4,1,1,1.8,1.8,2.5c0.8,0.6,1.7,0.9,2.8,0.9c1.6,0,2.8-0.7,3.7-2c0.9-1.3,1.3-2.9,1.3-4.6

				c0-1.1-0.2-2.2-0.6-3.2c-0.4-1-1-1.9-1.7-2.5s-1.7-1-2.8-1c-1.1,0-2,0.3-2.8,1c-0.8,0.6-1.4,1.5-1.8,2.5

				C272.5,414.3,272.3,415.4,272.3,416.5z"/>

			<path id="XMLID_204_" class="st0" d="M305,425.1v-17.4c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.8,0,1.5,0.2,2.1,0.6

				c0.5,0.4,0.8,1,1,1.8c1.7-1.8,3.7-2.7,6.1-2.7c2.8,0,5,1.1,6.8,3.3c0.9-1,2-1.8,3.3-2.4c1.3-0.6,2.5-0.9,3.6-0.9

				c1.8,0,3.4,0.3,4.7,1c1.3,0.7,2.4,1.7,3.1,3c0.7,1.3,1.1,2.9,1.1,4.8v12.5c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3

				c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1c-0.6-0.7-0.9-1.5-0.9-2.6v-11.4c0-0.9-0.2-1.7-0.5-2.3c-0.3-0.7-0.7-1.2-1.3-1.6

				c-0.6-0.4-1.3-0.6-2-0.6c-1.2,0-2.1,0.4-2.8,1.2c-0.7,0.8-1,1.9-1,3.2v11.4c0,0.7-0.1,1.3-0.4,1.9s-0.7,1-1.2,1.3

				s-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1c-0.6-0.7-0.9-1.5-0.9-2.6v-11.4c0-0.9-0.2-1.7-0.5-2.3c-0.3-0.7-0.7-1.2-1.3-1.6

				c-0.6-0.4-1.3-0.6-2-0.6c-0.9,0-1.6,0.2-2.2,0.6c-0.6,0.4-1,0.9-1.3,1.6c-0.3,0.7-0.4,1.5-0.4,2.4v11.4c0,0.7-0.1,1.3-0.4,1.9

				c-0.3,0.6-0.7,1-1.2,1.3c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1C305.3,427.1,305,426.2,305,425.1z"/>

			<path id="XMLID_206_" class="st0" d="M346.9,394.2c0.7,0,1.3,0.2,1.8,0.5c0.6,0.3,1,0.8,1.4,1.3c0.3,0.6,0.5,1.1,0.5,1.7

				c0,1-0.4,1.9-1.1,2.7c-0.7,0.7-1.6,1.1-2.6,1.1c-1,0-1.8-0.4-2.6-1.2c-0.7-0.8-1.1-1.6-1.1-2.6c0-0.9,0.4-1.8,1.1-2.5

				C345.1,394.6,345.9,394.2,346.9,394.2z M343.6,425.1v-17.4c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.6,0,1.2,0.2,1.7,0.5

				s0.9,0.7,1.2,1.3c0.3,0.6,0.4,1.2,0.4,1.9v17.4c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3s-1.1,0.5-1.7,0.5

				c-1,0-1.8-0.3-2.4-1C343.9,427.1,343.6,426.2,343.6,425.1z"/>

			<path id="XMLID_209_" class="st0" d="M372.6,407.9c0,0.7-0.2,1.4-0.7,2.1c-0.5,0.7-1.1,1-1.8,1c-0.5,0-1.3-0.2-2.4-0.6

				c-1.1-0.4-2-0.6-2.8-0.6c-1.2,0-2.2,0.3-3,0.9c-0.8,0.6-1.5,1.5-1.9,2.5s-0.6,2.1-0.6,3.3c0,1.7,0.5,3.3,1.5,4.6c1,1.3,2.3,2,4,2

				c0.5,0,1-0.1,1.5-0.3s1.2-0.4,1.9-0.7c0.8-0.3,1.2-0.4,1.4-0.4c0.5,0,1,0.1,1.4,0.4s0.8,0.6,1,1c0.3,0.4,0.4,0.9,0.4,1.4

				c0,1-0.5,1.9-1.4,2.5c-0.9,0.7-2,1.2-3.3,1.5c-1.3,0.3-2.4,0.5-3.3,0.5c-2.3,0-4.3-0.6-6.1-1.7c-1.8-1.1-3.2-2.7-4.2-4.6

				c-1-1.9-1.5-4-1.5-6.3c0-1.6,0.3-3.2,0.9-4.7c0.6-1.5,1.4-2.9,2.4-4.1c1-1.2,2.3-2.1,3.7-2.8c1.4-0.7,3-1,4.7-1

				c1,0,2.1,0.1,3.4,0.4c1.3,0.3,2.3,0.7,3.2,1.3S372.6,406.9,372.6,407.9z"/>

			<path id="XMLID_211_" class="st0" d="M374.8,425.1v-28c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.6,0,1.2,0.2,1.7,0.5

				c0.5,0.3,0.9,0.7,1.2,1.3c0.3,0.6,0.4,1.2,0.4,1.9v8.9h0.1c0.6-0.8,1.5-1.3,2.6-1.7c1.1-0.4,2.2-0.6,3.4-0.6

				c1.7,0,3.3,0.4,4.6,1.1c1.4,0.7,2.4,1.7,3.2,3c0.8,1.3,1.2,2.9,1.2,4.7v12.5c0,0.7-0.1,1.3-0.4,1.9s-0.7,1-1.2,1.3

				s-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1c-0.6-0.7-0.9-1.5-0.9-2.6v-11.4c0-1.5-0.4-2.6-1.2-3.3c-0.8-0.8-1.8-1.1-3.1-1.1

				c-0.9,0-1.6,0.2-2.2,0.6c-0.6,0.4-1.1,0.9-1.4,1.6c-0.3,0.7-0.5,1.4-0.5,2.3v11.4c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3

				c-0.5,0.3-1.1,0.5-1.7,0.5c-1,0-1.8-0.3-2.4-1C375.1,427.1,374.8,426.2,374.8,425.1z"/>

			<path id="XMLID_213_" class="st0" d="M422.5,408.3v16.2c0,1.3-0.2,2.3-0.5,3.1c-0.4,0.8-1.2,1.2-2.6,1.2c-0.9,0-1.6-0.2-2.1-0.7

				c-0.5-0.5-0.8-1.2-1.1-2.1c-1.7,2-3.8,3-6.2,3c-2,0-3.8-0.5-5.5-1.6s-3-2.6-3.9-4.5c-0.9-1.9-1.4-4.1-1.4-6.5

				c0-2.3,0.5-4.5,1.4-6.4c1-1.9,2.3-3.5,3.9-4.6c1.7-1.1,3.5-1.7,5.4-1.7c1.2,0,2.4,0.3,3.5,0.8c1.1,0.5,2,1.2,2.7,2.2

				c0.1-0.8,0.4-1.5,1-2c0.6-0.5,1.3-0.8,2.1-0.8c1.4,0,2.3,0.4,2.6,1.2C422.3,406,422.5,407.1,422.5,408.3z M405.7,416.5

				c0,1.1,0.2,2.2,0.6,3.2c0.4,1,1,1.8,1.8,2.5c0.8,0.6,1.7,0.9,2.8,0.9c1.6,0,2.8-0.7,3.7-2c0.9-1.3,1.3-2.9,1.3-4.6

				c0-1.1-0.2-2.2-0.6-3.2c-0.4-1-1-1.9-1.7-2.5s-1.7-1-2.8-1c-1.1,0-2,0.3-2.8,1c-0.8,0.6-1.4,1.5-1.8,2.5

				C405.9,414.3,405.7,415.4,405.7,416.5z"/>

			<path id="XMLID_216_" class="st0" d="M445.4,418.6h-13.9c0.2,1.6,0.9,2.7,2,3.5s2.5,1.2,4.1,1.2c1,0,1.8-0.2,2.5-0.5

				s1.6-0.8,2.6-1.3c1-0.6,1.8-0.9,2.3-0.9c0.4,0,0.9,0.1,1.3,0.4c0.4,0.3,0.8,0.6,1,1.1c0.3,0.4,0.4,0.9,0.4,1.4

				c0,0.7-0.3,1.4-0.9,2c-0.6,0.7-1.5,1.3-2.5,1.8c-1,0.5-2.2,0.9-3.5,1.3c-1.3,0.3-2.5,0.5-3.7,0.5c-2.4,0-4.5-0.5-6.4-1.6

				c-1.9-1.1-3.3-2.6-4.3-4.5c-1-1.9-1.5-4.1-1.5-6.5c0-1.6,0.3-3.2,0.9-4.7s1.4-2.9,2.4-4.1c1-1.2,2.3-2.1,3.7-2.8

				c1.5-0.7,3-1,4.7-1c1.7,0,3.3,0.4,4.8,1.1c1.5,0.7,2.7,1.6,3.7,2.8c1,1.2,1.8,2.4,2.4,3.8c0.6,1.4,0.9,2.8,0.9,4.2

				C448.3,417.6,447.3,418.6,445.4,418.6z M431.3,414.2h10.4c-0.2-1.5-0.7-2.7-1.6-3.6s-2.1-1.4-3.6-1.4c-1.4,0-2.6,0.5-3.5,1.4

				S431.5,412.8,431.3,414.2z"/>

			<path id="XMLID_219_" class="st0" d="M450.5,425.1v-28c0-1.1,0.3-2,0.9-2.6c0.6-0.7,1.4-1,2.4-1c0.6,0,1.2,0.2,1.7,0.5

				c0.5,0.3,0.9,0.7,1.2,1.3c0.3,0.6,0.4,1.2,0.4,1.9v28c0,0.7-0.1,1.3-0.4,1.9c-0.3,0.6-0.7,1-1.2,1.3s-1.1,0.5-1.7,0.5

				c-1,0-1.8-0.3-2.4-1C450.8,427.1,450.5,426.2,450.5,425.1z"/>

		</g>

		<path class="st0" d="M153.7,151.3c0,0-31.2,25.7-14.9,67.9c0,0,10.9,27.1,25,44.4c0,0,30.9,44,36.2,57.9c0,0,18.4,36-1.9,60.8

			c0,0,26.3-25.8,17.2-66.8c0,0-4.1-24-29.8-55.6c0,0-26.9-32.3-37.4-54.9C148,205,130.9,175.4,153.7,151.3z"/>

		<path class="st0" d="M224.7,360.4c6-11.9,8.2-24.4,9.1-33.5c0.9-9.9-0.8-19.8-4.9-28.8c-10.7-23.1-33.1-48.6-33.1-48.6

			c-33.4-40.1-33.4-62.5-33.4-62.5c-5.5-66.8,61.7-79.8,61.7-79.8c-0.4,0.2-0.7,0.4-1.1,0.6c-20.7,12.5-28.3,38.9-17.3,60.5

			l36.6,72.2c0.3,0.6,1.1,0.6,1.5,0l63.5-121.7l51.1,210.4h-47.4l-17.2-95.5c-0.1-0.6-0.8-0.7-1.1-0.2l-49.8,94.1"/>

		<polygon class="st0" points="311.1,331.7 358.4,331.7 358.9,333.8 311.5,333.8 		"/>

		<polygon class="st0" points="311.7,335.3 359,335.3 359.5,337.4 312.1,337.4 		"/>

		<polygon class="st0" points="312.4,338.8 359.7,338.8 360.2,340.9 312.8,340.9 		"/>

	</g>

</g>

<g id="XMLID_221_">

</g>

<g id="XMLID_222_">

</g>

<g id="XMLID_223_">

</g>

<g id="XMLID_224_">

</g>

<g id="XMLID_225_">

</g>

<g id="XMLID_226_">

</g>

</svg></span>
    </div>
  </div>
  </div>
  <div class="player">
  <div class="playlistIcon">
    <i class="icons ion-android-list plIcon"></i>
  </div>
  <div class="clearfix"></div>
  <div class="trackDetails ">
    <span class="artist"></span>
    <span class="splitTxt"> - </span>
    <span class="title"></span>
  </div>

  <div class="controls">
    <div class="rew">
      <i class="icons ion-ios-skipbackward"></i>
    </div>
    <div class="play">
      <i class="icons ion-ios-play"></i>
    </div>
    <div class="pause">
      <i class="icons ion-ios-pause"></i>
    </div>

    <div class="fwd">
      <i class="icons ion-ios-skipforward"></i>
    </div>
  </div>
  <div class="volumeIcon">
    <i class="icons ion-android-volume-up"></i>
    <div class="volume"></div>
  </div>

  <div class="tracker"></div>
</div>
<ul class="playlist" id="playlist">
  <li audioURL="assets/production/sounds/sara-single.mp3" artist="SM">Work In Order (Out Soon)</li>
  <li audioURL="http://picdn.net/shutterstock/audio/429215/preview/preview.mp3" artist="Ulas Pakkan">Technology</li>
  <li audioURL="http://picdn.net/shutterstock/audio/397292/preview/preview.mp3" artist="JAM Studio">Gateway</li>
  <li audioURL="http://picdn.net/shutterstock/audio/423269/preview/preview.mp3" artist="JAM Studio">Lovely Day</li>
</ul>
  </div>
  
<div class="grid grid-pad">
<div class="col-1-1">
<div class="content sm-profile-grid">

<p><?php $default = "Open Mind Africa a non-governmental organization that models human excellence through Neuro Linguistic Programming (NLP), Social Emotional Learning (SEL), Multiple Intelligences Education (MI) and Grit (G).

We provide the African Child with a range of tools for expanding self-awareness and awareness of others.

We coach students to manage their emotions better, and to replicate patterns of excellence found in the most successful people in the world.";
		?>
		<?=CMS::render('oma_slider_content_4a', CMS::MULTI, $default, $editmode)?>
<!--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sollicitudin eros, et dapibus ante. Integer nec libero elementum, dapibus quam in, semper nibh. 
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in sollicitudin eros, et dapibus ante. Integer nec libero elementum, dapibus quam in, semper nibh.--></p>

<div class="sm-profile-image">
<div class="sm-profile">
  <img src="assets/production/images/sm-profile-picture.jpg" width="482" height="654"  alt=""/> </div>

<div class="sm-profile-btm"></div>
<h2><a href="#" onmouseenter="playAudio()"><span class="icon___1m4Ry"><img src="assets/production/images/arrow-black.svg"></span>SM Profile</a></h2>
</div>
</div>
</div>

</div>

<div class="grid grid-pad AppSection">


<div class="col-1-3"><div class="content">
<div class="Itab"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 187.2 172.7" style="enable-background:new 0 0 187.2 172.7;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_87_">

	<g id="XMLID_256_">

		<path id="XMLID_267_" class="st0" d="M146.7,121.4c-1.9-0.5-3.9,0.6-4.4,2.6c-0.5,1.9,0.6,3.9,2.6,4.4c1.9,0.5,3.9-0.6,4.4-2.6

			C149.8,123.9,148.7,121.9,146.7,121.4z"/>

		<path id="XMLID_266_" class="st0" d="M42.6,128.4c1.9-0.5,3.1-2.5,2.6-4.4c-0.5-1.9-2.5-3.1-4.4-2.6c-1.9,0.5-3.1,2.5-2.6,4.4

			C38.7,127.8,40.7,128.9,42.6,128.4z"/>

		<path id="XMLID_257_" class="st0" d="M182.4,116.9c2.7-9,4.1-17.4,4.1-25.7c0-24.1-9.8-46.8-27.6-64c-17.4-16.8-40.7-26.5-65-26.5

			C43.8,0.7,1.1,41.1,1.1,91.2c0,8.3,1.3,16.7,4.1,25.7c-3.3,2.6-4.9,7.1-3.8,11.3l9.4,35c1.5,5.8,7.5,9.2,13.3,7.7l11.1-3

			c2.5,3.7,7.2,5.6,11.8,4.3l14-3.8c5.8-1.5,9.2-7.5,7.7-13.3l-13.1-49c-1.6-5.8-7.5-9.2-13.3-7.7l-14,3.7c-1.5,0.4-2.8,1.1-3.9,2

			c-0.7-3.4-1.2-6.7-1.3-10c4-1.3,7-4.8,7.4-9.1c3-30.6,31.4-55.5,63.4-55.5s60.4,24.9,63.4,55.5c0.4,4.4,3.4,7.9,7.4,9.1

			c-0.2,3.3-0.6,6.6-1.3,10c-1.1-0.9-2.4-1.6-3.9-2l-14-3.7c-5.8-1.5-11.8,1.9-13.3,7.7l-13.1,49c-1.5,5.8,1.9,11.7,7.7,13.3l14,3.8

			c4.5,1.2,9.2-0.6,11.8-4.3l11.1,3c5.9,1.6,11.8-1.9,13.3-7.7l9.4-35C187.3,123.9,185.7,119.5,182.4,116.9z M30,109.3l14-3.7

			c1.9-0.5,3.9,0.6,4.4,2.6l13.1,49c0.5,1.9-0.6,3.9-2.6,4.4l-3.5,0.9L49,138c-0.5-1.9-2.5-3.1-4.4-2.6c-1.9,0.5-3.1,2.5-2.6,4.4

			l6.6,24.5l-3.5,0.9c-1.9,0.5-3.9-0.6-4.4-2.6v0l-13.1-49C26.9,111.8,28.1,109.8,30,109.3z M32.7,161.1l-10.5,2.8

			c-1.9,0.5-3.9-0.6-4.4-2.6l-9.4-35c-0.5-1.8,0.5-3.7,2.3-4.4c0.1,0,0.2-0.1,0.2-0.1l10.3-2.8C22.3,121.8,20.4,115.8,32.7,161.1z

			 M8.4,94.8h7.2c0.3,5.8,1.4,11.6,3.2,17.5l-7,1.9C9.9,107.4,8.7,101,8.4,94.8z M164.4,84.4c-3.4-35.1-35.3-62.1-70.6-62.1

			c-35.4,0-67.2,27-70.6,62.1c-0.2,1.7-1.7,3.1-3.6,3.1H8.4C10.4,44,48.7,7.9,93.8,7.9c45.1,0,83.3,36.1,85.3,79.6h-11.2

			C166.1,87.5,164.6,86.2,164.4,84.4z M179.1,94.8c-0.3,6.2-1.5,12.6-3.5,19.4l-7-1.9c1.8-5.9,2.9-11.8,3.2-17.5H179.1z

			 M146.9,162.7C146.9,162.7,146.9,162.7,146.9,162.7c-0.5,1.9-2.5,3.1-4.4,2.6l-3.5-0.9l6.6-24.5c0.5-1.9-0.6-3.9-2.6-4.4

			c-1.9-0.5-3.9,0.6-4.4,2.6l-6.6,24.5l-3.5-0.9c-1.9-0.5-3.1-2.5-2.6-4.4l13.1-49c0.4-1.6,1.9-2.7,3.5-2.7c0.3,0,0.6,0,0.9,0.1

			l14,3.7c1.9,0.5,3.1,2.5,2.6,4.4L146.9,162.7z M179.2,126.4l-9.4,35c-0.5,1.9-2.5,3.1-4.4,2.6l-10.5-2.8

			c12.3-45.3,10.3-39.3,11.4-41.9c11,3,10.4,2.8,10.6,2.9C178.6,122.6,179.6,124.5,179.2,126.4z"/>

	</g>

</g>


</svg>

<h5>listen to sara’s <br>
music</h5></div></div></div>

<div class="col-1-3"><div class="content">
<a href="#" id="myBtn"><div class="Itab"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 174.7 174.2" style="enable-background:new 0 0 174.7 174.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

	.st1{fill:none;stroke:#FFFFFF;stroke-width:4;stroke-miterlimit:10;}

</style>

<g id="XMLID_118_">

	<polyline id="XMLID_120_" class="st0" points="65.5,57.4 118.5,86.7 65.5,115.9 	"/>

	<circle id="XMLID_119_" class="st1" cx="86.7" cy="86.7" r="84.7"/>

</g>


</svg><h5>watch sara’s<br>
videos</h5></div></a></div></div>

<div class="col-1-3"><div class="content"><div class="Itab"><a href="tour.php"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 187.2 162.5" style="enable-background:new 0 0 187.2 162.5;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_123_">

	<g id="XMLID_213_">

		<path id="XMLID_219_" class="st0" d="M185.1,51.3L80.3,1.2C79,0.7,77.6,1.2,77,2.4L54.2,50.1h12.4L83.4,15l90.9,43.4L145.8,118

			l-16-7.6v31.5l9.8,4.7c1.2,0.6,2.7,0.1,3.3-1.2l43.3-90.7C186.9,53.4,186.4,51.9,185.1,51.3z"/>

		<path id="XMLID_216_" class="st0" d="M120.1,55.9H3.9c-1.3,0-2.5,1.1-2.5,2.4v100.6c0,1.3,1.1,2.5,2.5,2.5h116.2

			c1.4,0,2.5-1.1,2.5-2.5V58.4C122.6,57,121.5,55.9,120.1,55.9z M113.4,133H106c-4.3-11-9.6-26.4-17.6-24.3

			C79,111,74.4,133,74.4,133s-4.8-24.8-18.1-38C43,81.7,30.2,133,30.2,133H12.6V67h100.8V133z"/>

		<circle id="XMLID_215_" class="st0" cx="28.9" cy="81.2" r="8.2"/>

		<path id="XMLID_214_" class="st0" d="M84,87c1.8,0,3.6-0.2,5.1-0.5c1.8,0.8,4,1.2,6.3,1.2c6.1,0,11-3,11-6.6c0-3.6-4.9-6.6-11-6.6

			c-2.3,0-4.4,0.4-6.1,1.1c-0.7-0.7-1.7-1.1-2.8-1.1H85c-2,0-3.6,1.5-3.9,3.5c-4.9,0.5-8.6,2.3-8.6,4.4C72.6,84.9,77.7,87,84,87z"/>

	</g>

</g>


</svg>

<h5>tours<br>
& Shows</h5></a></div></div></div>


<div class="col-1-3"><div class="content">
<a href="book.php"><div class="Itab"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 122.2 142" style="enable-background:new 0 0 122.2 142;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<path id="XMLID_118_" class="st0" d="M118,116.6l-14-14V2.4c0-1.3-1.1-2.4-2.4-2.4H30.9c0,0-0.1,0-0.1,0c-0.2,0-0.4,0.1-0.6,0.1

	c-0.1,0-0.1,0-0.2,0.1c-0.3,0.1-0.5,0.3-0.7,0.5L0.9,29c-0.2,0.2-0.4,0.4-0.5,0.7c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.2-0.1,0.4-0.1,0.6

	c0,0.1,0,0.1,0,0.1v89.7c0,1.3,1.1,2.4,2.4,2.4H84l14,14c2.6,2.7,6.3,4.2,10,4.2c5.7,0,10.9-3.4,13.1-8.7

	C123.3,126.8,122.1,120.7,118,116.6L118,116.6z M98,130L61.3,93.3l13.4-13.4l36.7,36.7L98,130z M59.3,88.6l-3.2-13.9L70,77.9

	L59.3,88.6z M28.5,8.1v20.3H8.3L28.5,8.1z M5,118V33h26c1.3,0,2.4-1.1,2.4-2.4v-26h66.1v93.2l-23-23l0,0c-0.2-0.2-0.4-0.3-0.7-0.4

	c-0.1,0-0.1,0-0.2-0.1c-0.1,0-0.2-0.1-0.3-0.1l-21.7-5c-0.8-0.2-1.6,0.1-2.2,0.6c-0.6,0.6-0.8,1.4-0.6,2.2l5,21.7

	c0,0.1,0.1,0.2,0.1,0.3c0,0.1,0,0.1,0.1,0.2c0.1,0.2,0.3,0.5,0.5,0.7l0,0l23,23H5z M114.7,133.3c-1.8,1.8-4.2,2.8-6.7,2.8

	c-2.5,0-4.9-1-6.7-2.8l1.1-1.1l12.2-12.2c1.8,1.8,2.8,4.2,2.8,6.7C117.4,129.1,116.4,131.5,114.7,133.3L114.7,133.3z"/>


</svg>

<h5>
events<br>booking</h5></div></a></div></div>

<div class="col-1-3"><div class="content">
<div class="Itab"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 122.2 162.5" style="enable-background:new 0 0 122.2 162.5;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_120_">

	<g id="XMLID_221_">

		<path id="XMLID_223_" class="st0" d="M34.8,78.3c-1.1-1.1-2.8-1.1-3.9,0c-1.1,1.1-1.1,2.8,0,3.9l27.4,27.4

			c0.5,0.5,1.2,0.8,1.9,0.8c0,0,0,0,0.1,0c0,0,0,0,0.1,0c0.9,0,1.7-0.5,2.2-1.2L89.7,82c1.1-1.1,1.1-2.8,0-3.9

			c-1.1-1.1-2.8-1.1-3.9,0l-22.7,22.7V3.3c0-1.5-1.2-2.7-2.7-2.7s-2.7,1.2-2.7,2.7v97.8L34.8,78.3z"/>

		<path id="XMLID_222_" class="st0" d="M79.6,44.5c-1.5,0-2.7,1.2-2.7,2.7s1.2,2.7,2.7,2.7h35.7v107H5.5V50h35.7

			c1.5,0,2.7-1.2,2.7-2.7s-1.2-2.7-2.7-2.7H0v118h120.8v-118H79.6z"/>

	</g>

</g>


</svg>

<h5>downloads
sara’s <br>music</h5></div></div></div>

<div class="col-1-3"><div class="content"><a href="contacts.php"><div class="Itab"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 170.7 162.5" style="enable-background:new 0 0 170.7 162.5;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

</style>

<g id="XMLID_177_">

	<g id="XMLID_248_">

		<g id="XMLID_249_">

			<g id="XMLID_250_">

				<path id="XMLID_254_" class="st0" d="M162.3,0.2H43.9c-4,0-7.2,3.2-7.2,7.2v32.7L31.3,32c-2.5-3.7-7.5-4.9-11.3-2.5L8.8,36.1

					c-3.6,2.1-6.3,5.6-7.5,9.7c-4.1,14.8-1,40.3,36.4,77.7c29.8,29.7,51.9,37.7,67.2,37.7c3.5,0,7.1-0.4,10.5-1.4

					c4.1-1.1,7.5-3.8,9.7-7.5l6.7-11.1c2.3-3.8,1.2-8.8-2.5-11.3l-26.7-17.8c-3.7-2.4-8.6-1.6-11.3,1.8l-7.8,10

					c-0.8,1.1-2.3,1.4-3.5,0.7l-1.5-0.8c-4.9-2.7-10.9-6-23.1-18.2c-1.3-1.3-2.5-2.6-3.6-3.7h110.4c4,0,7.2-3.2,7.2-7.2V7.5

					C169.5,3.5,166.3,0.3,162.3,0.2z M75.9,128.9l1.4,0.8c3.6,2,8.1,1.1,10.7-2.2l7.8-10c0.9-1.1,2.5-1.4,3.7-0.6l26.7,17.8

					c1.2,0.8,1.6,2.4,0.8,3.7l-6.7,11.1c-1.4,2.4-3.7,4.2-6.3,4.9c-13.4,3.7-36.7,0.6-72.2-34.9S3.2,60.7,6.8,47.3

					c0.8-2.7,2.5-4.9,4.9-6.3l11.1-6.7c1.2-0.7,2.9-0.4,3.7,0.8l17.8,26.7c0.8,1.2,0.5,2.8-0.6,3.7l-10,7.8

					c-3.3,2.5-4.2,7.1-2.2,10.7l0.8,1.4c2.8,5.2,6.3,11.6,19.1,24.4C64.3,122.6,70.8,126.1,75.9,128.9z M163.8,94.9

					c0,0.8-0.7,1.5-1.6,1.5H46.8c-3.7-4.2-6.8-8.8-9.4-13.7l-0.8-1.5c-0.6-1.2-0.3-2.6,0.7-3.5l10-7.8c3.5-2.7,4.2-7.6,1.8-11.3

					l-6.7-10.1V7.5c0-0.4,0.2-0.8,0.4-1.1c0.3-0.3,0.7-0.5,1.1-0.4h118.4c0.4,0,0.8,0.2,1.1,0.4c0.3,0.3,0.5,0.7,0.4,1.1L163.8,94.9

					L163.8,94.9z"/>

				<path id="XMLID_253_" class="st0" d="M152.6,13.3l-47.2,35.8c-1.3,1-3.1,1-4.5,0L53.6,13.3c-1.2-0.9-3-0.7-4,0.5

					c-0.9,1.2-0.7,3,0.5,4l47.2,35.8c3.4,2.5,7.9,2.5,11.3,0L156,17.8c0.6-0.5,1-1.1,1.1-1.9s-0.1-1.5-0.5-2.1

					C155.6,12.6,153.8,12.4,152.6,13.3z"/>

				<path id="XMLID_252_" class="st0" d="M79,53.9L50,84.9c-1.1,1.1-1,2.9,0.1,4c1.1,1.1,2.9,1,4-0.1l28.9-31c1-1.2,0.8-2.9-0.3-3.9

					C81.8,52.8,80.1,52.8,79,53.9z"/>

				<path id="XMLID_251_" class="st0" d="M127.4,53.9c-1.1-1.1-2.9-1.2-4-0.1c-1.1,1.1-1.2,2.9-0.1,4l28.9,31c1.1,1.1,2.9,1.2,4,0.1

					c1.1-1.1,1.2-2.9,0.1-4L127.4,53.9z"/>

			</g>

		</g>

	</g>

</g>


</svg>

<h5>contact<br>
management</h5></div></a></div></div>






</div>
</div>

<div class="grid grid-pad sectionApp">
    <div class="col-1-1">
       <div class="content">
           <div class="news-blog">
           <h1>Broadcast</h1>
           
           
           
           <div class="col-1-1">

	<ul class="tabs">
		<li class="tab-link current" data-tab="tab-1">Sara's News</li>
		<li class="tab-link" data-tab="tab-2">Sara's Events</li>
	</ul>

	<div id="tab-1" class="tab-content current">
    <h3><a href="#">Saraphina Michael hangout with friends</a></h3>
    <span class="news-date">2019.04.07</span>
		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
	</div>
	<div id="tab-2" class="tab-content">
		 Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
	</div>
</div><!-- container -->
           
           
           
           
           
           <!--Start-->
           
           <!--<div class="masonry">
    <ul class="tabs">
      <li><a href="#tab1">Tab 1</a></li>
      <li><a href="#tab2">Tab 2</a></li>
    </ul>
    <div class="tab_container">
      <div id="tab1" class="tab_content">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ratione provident fugit consequuntur tempora esse repellendus, pariatur labore quos qui.
      </div>
      <div id="tab2" class="tab_content">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex suscipit, cupiditate voluptatum facere, quis quibusdam, velit impedit nisi veritatis explicabo cumque at. Facere blanditiis quisquam dolorum, itaque consequuntur, sequi distinctio assumenda!
        Nisi laboriosam fugit, aliquid esse amet itaque consequuntur ratione?
      </div>
      
    </div>
  </div>-->
  
  <!--End-->
           
           
           
           
           
           
           
           
 
           
           </div>
       </div>
    </div>
    </div>
    
    
    
    
    
    
    <div class="grid grid-pad AppSection-3 bgimg-1_ home-video-banner-section" style="background-image: url(assets/production/images/single-poster-bg.jpg)"">
    <div class="col-1-1">
    <div class="content">
    <div class="page">
    <div class="single-poster">single-poster</div>
    
    <!--Pop Video<p><a href="#media-popup" data-media="//www.youtube.com/embed/YoXa2Pl7Hk0"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 174.7 174.2" style="enable-background:new 0 0 174.7 174.2;" xml:space="preserve">

<style type="text/css">

	.st0{fill:#FFFFFF;}

	.st1{fill:none;stroke:#FFFFFF;stroke-width:4;stroke-miterlimit:10;}

</style>

<g id="XMLID_118_">

	<polyline id="XMLID_120_" class="st0" points="65.5,57.4 118.5,86.7 65.5,115.9 	"/>

	<circle id="XMLID_119_" class="st1" cx="86.7" cy="86.7" r="84.7"/>

</g>


</svg><h5>watch sara’s<br>
videos</h5></a></p>-->
    
    <div class="popup" id="media-popup">
        <iframe width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
    </div>
    </div>
    </div>
     
     
     <div>
     
    <div id="myModal" class="modal">
<div class="grid-pad grid">
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    
   
    
    <div class="col-1-1">
    <div class="music-box">
    <div class="content">
    <?php include('includes/login.php');?>
    <span>Saraphina Michael</span><span>Bounce Back</span><span>4:30</span></div></div>
    </div>

  </div>
  
</div>
</div></div>



  <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<footer><div class="grid grid-pad AppSection-4">
<div class="col-1-3"><span class="tz-flag"><svg title="Tanzania" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="-364 140 910 910" style="enable-background:new -364 140 910 910;" xml:space="preserve">

<style type="text/css">

	.tz{fill:#FFDA44;}

	.tz1{fill:#6DA544;}

	.tz2{fill:#338AF3;}

</style>

<g>

	<circle class="tz" cx="91" cy="595" r="455"/>

	<path d="M-230.7,916.7c19.1,19.1,39.5,36.1,60.8,51.1l633.7-633.7c-15-21.4-32-41.7-51.1-60.8c-19.1-19.1-39.5-36.1-60.8-51.1

		l-633.7,633.7C-266.8,877.3-249.8,897.6-230.7,916.7z"/>

	<path class="tz1" d="M-230.7,273.3c-143.5,143.5-171.1,359.1-82.7,530.4l613.1-613.1C128.4,102.2-87.2,129.7-230.7,273.3z"/>

	<path class="tz2" d="M412.7,916.7c143.5-143.5,171.1-359.1,82.7-530.4l-613.1,613.1C53.6,1087.8,269.2,1060.3,412.7,916.7z"/>

</g>

</svg></span>

</div>

<div class="col-1-3"><div class="content">&copy; <?php echo date("Y"); ?> Saraphina Michael<span class="sm-footer-brand"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 266 321.5" style="enable-background:new 0 0 266 321.5;" xml:space="preserve">

<style type="text/css">

	.sm-footer-logo{fill:#D51C5A;}

</style>

<g>

	<path id="XMLID_5_" class="sm-footer-logo" d="M25.5,50.8c0,0-36,29.6-17.3,78.4c0,0,12.6,31.3,28.9,51.3c0,0,35.7,50.8,41.8,66.9

		c0,0,21.2,41.6-2.1,70.3c0,0,30.4-29.8,19.8-77.1c0,0-4.8-27.7-34.4-64.2c0,0-31-37.3-43.2-63.4C18.9,112.9-0.9,78.7,25.5,50.8z"/>

	<path id="XMLID_4_" class="sm-footer-logo" d="M107.4,292.3c6.9-13.7,9.5-28.2,10.5-38.6c1-11.4-0.9-22.9-5.7-33.3

		c-12.4-26.6-38.2-56.1-38.2-56.1C35.3,118,35.3,92.1,35.3,92.1C29,15,106.6,0,106.6,0c-0.4,0.2-0.8,0.5-1.2,0.7

		c-23.9,14.5-32.6,44.9-20,69.8l42.3,83.3c0.3,0.7,1.3,0.7,1.7,0l73.3-140.6l59,242.9h-54.8L186.9,146c-0.1-0.6-1-0.8-1.3-0.2

		l-57.5,108.7"/>

	<polygon id="XMLID_3_" class="sm-footer-logo" points="207.2,259.2 261.8,259.2 262.4,261.6 207.6,261.6 	"/>

	<polygon id="XMLID_2_" class="sm-footer-logo" points="207.9,263.3 262.5,263.3 263.1,265.7 208.3,265.7 	"/>

	<polygon id="XMLID_1_" class="sm-footer-logo" points="208.7,267.4 263.3,267.4 263.9,269.9 209.1,269.9 	"/>

</g>

</svg>

</span></div>

</div><div class="col-1-3"><a href="#" title="Hub Communications"><span class="code">design+code<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

	 viewBox="0 0 106 318.2" style="enable-background:new 0 0 106 318.2;" xml:space="preserve">

<style type="text/css">

	.hub{fill:#F2A61D;}

</style>

<path id="XMLID_602_" class="hub" d="M29.5,80c0,0,5.2-51.2,64.6-50.1c0,0,10.3-0.5,11.9-14c0,0,1.4-14.6-18.1-15.8

	c0,0-17.3-1.8-34.9,6.4c0,0-15.8,5.4-30,21.1c-0.3,0.3-0.5,0.6-0.7,0.9C19.7,31,6.4,42.2,1.2,73C0.4,77.7,0,82.5,0,87.3l0,150.4

	c0,0-2.9,44.8,50.1,73.4c0,0,38.2,16,47,0c0,0,5.1-10.4-2-17.3c-2.5-2.5-6-3.7-9.5-3.7c-4.9,0-14.7-0.7-23.8-6.1

	c0,0-36.3-17.3-33.2-60.2c0,0-0.8-27,31-46.3c0,0,13.1-8.2,32.9-6.2c1.3,0.1,2.6,0,3.8-0.5c3.9-1.5,11.6-6.1,8.6-18.2

	c0,0-1.6-7.9-9.1-9.5c0,0-33.2-7.9-66,21.2c0,0-1.2,0.7-1-3.4C28.8,160.8,27.6,84.8,29.5,80z"/>

</svg></span></a></div></div></footer>
  
</section>
  <script>
    $(document).ready(function() {

  var song;
  var tracker = $('.tracker');
  var volume = $('.volume');

  function initAudio(elem) {
    var url = elem.attr('audiourl');

    var title = elem.text();
    var artist = elem.attr('artist');

    $('.player .title').text(title);
    $('.player .artist').text(artist);

    // song = new Audio('media/'+url);
    song = new Audio(url);

    // timeupdate event listener
    song.addEventListener('timeupdate', function() {
      var curtime = parseInt(song.currentTime, 10);
      tracker.slider('value', curtime);
    });

    $('.playlist li').removeClass('active');
    elem.addClass('active');
  }

  function playAudio() {
    song.play();

    tracker.slider("option", "max", song.duration);

    $('.play').addClass('hidden');
    $('.pause').addClass('visible');
  }

  function stopAudio() {
    song.pause();

    $('.play').removeClass('hidden');
    $('.pause').removeClass('visible');
  }

  // play click
  $('.play').click(function(e) {
    e.preventDefault();
    // playAudio();

    song.addEventListener('ended', function() {
      var next = $('.playlist li.active').next();
      if (next.length == 0) {
        next = $('.playlist li:first-child');
      }
      initAudio(next);

      song.addEventListener('loadedmetadata', function() {
        playAudio();
      });

    }, false);

    tracker.slider("option", "max", song.duration);
    song.play();
    $('.play').addClass('hidden');
    $('.pause').addClass('visible');

  });

  // pause click
  $('.pause').click(function(e) {
    e.preventDefault();
    stopAudio();
  });

  // next track
  $('.fwd').click(function(e) {
    e.preventDefault();

    stopAudio();

    var next = $('.playlist li.active').next();
    if (next.length === 0) {
      next = $('.playlist li:first-child');
    }
    initAudio(next);
    song.addEventListener('loadedmetadata', function() {
      playAudio();
    });
  });

  // prev track
  $('.rew').click(function(e) {
    e.preventDefault();

    stopAudio();

    var prev = $('.playlist li.active').prev();
    if (prev.length === 0) {
      prev = $('.playlist li:last-child');
    }
    initAudio(prev);

    song.addEventListener('loadedmetadata', function() {
      playAudio();
    });
  });

  // show playlist
  $('.playlistIcon').click(function(e) {
    e.preventDefault();
    $('.playlist').toggleClass('show');
  });

  // playlist elements - click
  $('.playlist li').click(function() {
    stopAudio();
    initAudio($(this));
  });

  // initialization - first element in playlist
  initAudio($('.playlist li:first-child'));

  song.volume = 0.8;

  volume.slider({
    orientation: 'vertical',
    range: 'max',
    max: 100,
    min: 1,
    value: 80,
    start: function(event, ui) {},
    slide: function(event, ui) {
      song.volume = ui.value / 100;
    },
    stop: function(event, ui) {},
  });

  $('.volumeIcon').click(function(e) {
    e.preventDefault();
    $('.volume').toggleClass('show');
  });

  // empty tracker slider
  tracker.slider({

    range: 'min',
    min: 0,
    max: 10,
    start: function(event, ui) {},
    slide: function(event, ui) {
      song.currentTime = ui.value;
    },
    stop: function(event, ui) {}
  });
});
    </script>
    
    
    
    <script type="text/javascript">
	$("[data-media]").on("click", function(e) {
    e.preventDefault();
    var $this = $(this);
    var videoUrl = $this.attr("data-media");
    var popup = $this.attr("href");
    var $popupIframe = $(popup).find("iframe");
    
    $popupIframe.attr("src", videoUrl);
    
    $this.closest(".page").addClass("show-popup");
});

$(".popup").on("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    $(".page").removeClass("show-popup");
});

$(".popup > iframe").on("click", function(e) {
    e.stopPropagation();
});
	</script>
    
    
    
    <script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>
    
    
    <script>
    var audio1 = document.getElementById("audioID");

//Example of an HTML Audio/Video Method

function playAudio() {
  audio1.play();
}
    </script>
    
  <script>
  $(document).on("click", ".language-toggle:not(.unclickable)", function() {
		    var $this = $(this)
		        $this.addClass("unclickable")
		        
		        $(this).toggleClass('on');
			  	$('.language-switch').toggleClass('on');
			  setTimeout(function(){
          $this.removeClass("unclickable");
        }, 150);
		        
		});
  </script>  
  
  <!--<script>
  $(document).ready(function() {
 
  // Fakes the loading setting a timeout
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 3500);
 
});
  </script>-->
    
    <script>
    $(document).ready(function(){
	
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	})

})
    </script>
    <!--Tab Jump-->
    <!--<script>
	$(document).ready(function($) {
  $('.tab_content').hide();
  $('.tab_content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab_content').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });
});
	</script>-->
    
    <!--Pops Page Controller-->
    
   
    
    <!--<script src="assets/production/jquery-1.11.0.min.js"></script>-->
		<script src="assets/production/slideshow.min.js"></script>
		<script src="assets/production/launcher.js" ></script>
        
       
       <?php include('includes/page-header.php');?>
        
        <!-- The Modal -->
  
</body>

</html>
