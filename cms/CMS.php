<?php
include_once __DIR__."/../config/connect.php";

$editmode = false;
if(in_array($_SESSION['id'], explode(",", EDITORS))){
	$editmode = true;
}

if(isset($_FILES["awesomecmsimage"])){
	$newImage = uploadImage();
}
if(isset($_POST["cmsupdatebutton"])){
	$type = $_POST["snippettype"];
	unset($_POST["snippettype"]);
	unset($_POST["cmsupdatebutton"]);
	unset($_POST["placeholder"]);
	foreach ($_POST as $key => $value) {
		if($newImage){ $value = $newImage; }
		$sql = "REPLACE INTO snippets VALUES ('".mysqli_escape_string($con, $key)."', '".mysqli_escape_string($con, htmlentities($value))."', '".mysqli_escape_string($con, $type)."', '".gmdate("Y-m-d H:i:s")."')";
		$rst = mysqli_query($con, $sql);
	}
}else if(isset($_POST["cmsresetbutton"])){
	$type = $_POST["snippettype"];
	unset($_POST["snippettype"]);
	unset($_POST["cmsresetbutton"]);
	$placeholder = $_POST["placeholder"];
	unset($_POST["placeholder"]);
	foreach ($_POST as $key => $value) {
		$sql = "REPLACE INTO snippets VALUES ('".mysqli_escape_string($con, $key)."', '".mysqli_escape_string($con, htmlentities($placeholder))."', '".mysqli_escape_string($con, $type)."', '".gmdate("Y-m-d H:i:s")."')";
		$rst = mysqli_query($con, $sql);
	}
}

function uploadImage(){
	$target_dir = "cuploads/";
	$target_file = $target_dir . time().'_'.basename($_FILES["awesomecmsimage"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
  $check = getimagesize($_FILES["awesomecmsimage"]["tmp_name"]);
  if($check !== false) {
      $uploadOk = 1;
  } else {
      $uploadOk = 0;
  }

	// Check if file already exists
	if (file_exists($target_file)) {
	    $uploadOk = 0;
	}
	// Check file size
	if ($_FILES["awesomecmsimage"]["size"] > 500000) {
	    $uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
	    $uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
	    return false;
	// if everything is ok, try to upload file
	} else {
	    if (move_uploaded_file($_FILES["awesomecmsimage"]["tmp_name"], $target_file)) {
	        return $target_file;
	    } else {
	        return false;
	    }
	}
}

class CMS{
	const TEXT 	= 0;
	const HTML 	= 1;
	const MULTI 	= 2;
	const IMAGE 	= 3;
	const GALLERY 	= 4;
	const ALBUM	= 5;
	const VIDEO	= 6;	

	static function init($includeJQ = true){
		if($includeJQ){
			echo '<script type="text/javascript" src="//code.jquery.com/jquery-3.1.0.min.js"></script>';
		}
		echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.6.7/jquery.tinymce.min.js"></script>';
		echo '<script type="text/javascript">
			$(function(){
				var editbutton = ".superawesome-cms-TX47635439585376895";
				$(document).on("click", editbutton, function(){
					switch ($(this).attr("type")) {
					 	case "0":
					 	case "6":
					 		var type = "text";
					 		var content = "<input type=\"text\" name=\""+$(this).attr("uid")+"\" value=\""+$(this).attr("val")+"\" style=\"width: 98%;padding:2% 1%;font-size: 15px;\" ><input name=\"snippettype\" type=\"hidden\" value=\""+type+"\" ><textarea name=\"placeholder\" style=\"display:none\">"+$(this).attr("default")+"</textarea>";
					 		break;
					 	case "1":
					 		var type = "html";
					 		var content = "<div id=\"fake_textarea\" contenteditable class=\"cmshtmledit\" style=\"width: 98%;padding:2% 1%;font-size: 15px; min-height:100px; background: #fff;\">"+$(this).attr("val")+"</div><input name=\"snippettype\" type=\"hidden\" value=\""+type+"\"><textarea name=\"placeholder\" style=\"display:none\">"+$(this).attr("default")+"</textarea><input type=\"hidden\" id=\"fake_textarea_content\" name=\""+$(this).attr("uid")+"\" />";					 		
					 		break;
					 	case "2":
					 		var type = "multi";
					 		var content = "<textarea name=\""+$(this).attr("uid")+"\" style=\"width: 98%;padding:2% 1%;font-size: 15px; min-height:200px; background: #fff;\">"+$(this).attr("val")+"</textarea><input name=\"snippettype\" type=\"hidden\" value=\""+type+"\"><textarea name=\"placeholder\" style=\"display:none\">"+$(this).attr("default")+"</textarea>";					 		
					 		break;
					 	case "3":
					 		var type = "image";
					 		var content = "<img src=\""+$(this).attr("val")+"\" style=\"display:block; max-height:200px;\" /><input name=\"awesomecmsimage\" type=\"file\"  /><input name=\"snippettype\" type=\"hidden\" value=\""+type+"\"><input type=\"hidden\" name=\""+$(this).attr("uid")+"\" ><textarea name=\"placeholder\" style=\"display:none\">"+$(this).attr("default")+"</textarea>";					 		
					 		break;
					 	case "4":
					 		var type = "gallery";
					 		var content = "<img src=\""+$(this).attr("val")+"\" style=\"display:block; max-height:200px;\" /><input name=\"awesomecmsimage\" type=\"file\"  /><input name=\"snippettype\" type=\"hidden\" value=\""+type+"\"><input type=\"hidden\" name=\""+$(this).attr("uid")+"\" ><textarea name=\"placeholder\" style=\"display:none\">"+$(this).attr("default")+"</textarea>";				 		
					 		break;
					}
					$("cmseditcontainer").html("<form id=\"awesomecmsform\" method=\"post\" enctype=\"multipart/form-data\" style=\"margin-bottom:0\"><h1 style=\"text-align:center;color:#40e0d0;padding-bottom: 10px;font-size: 32px;\">Update Content</h1>"+content+"<button name=\"cmsupdatebutton\" style=\"font-size:14px;margin-top: 10px;padding: 7px 15px;background: #40e0d0;border: none;cursor: pointer;\">Update</button></form>"); 
					$("cmsoverlay, cmseditcontainer, cmseditclose").fadeIn(300);
					if($(this).attr("type") == 1){
						$("#awesomecmsform").submit(function(){
						  $("#fake_textarea_content").val($("#fake_textarea").html());
						});
					}
				});
				$(document).on("click", "cmseditclose", function(){
					$("cmsoverlay, cmseditcontainer, cmseditclose").fadeOut(300);
				});
				addmodal = function(){
					$("body").append("<cmsoverlay style=\"background:#000; opacity:0.2; position:fixed; width:100%; height:100%; top:0; left:0; z-index:100000; display:none\"></cmsoverlay><cmseditcontainer style=\"background: #555;position:fixed;width:540px;padding:50px 30px;top:200px;left:calc(50% - 300px);z-index:100001;border-radius: 10px;border: 2px solid #FFF; display:none\"></cmseditcontainer><cmseditclose style=\"position: fixed;padding: 4px 10px;left: calc(50% + 280px);z-index: 100002;border-radius: 20px;border: 2px solid rgb(255, 255, 255);top: 190px;color: rgb(255, 255, 255);font-weight: bold;font-size: 17px;font-family: Arial;line-height: 22px;cursor: pointer;background: #000;font-style: italic; display:none\" title=\"close\">X</cmseditclose>");
				}
				addmodal();				
			});
		</script>';
	}

	static function render($identifier, $type, $placeholder, $edit = false, $width = 'auto', $height = 'auto', $dataOnly = false){
		if($type == self::GALLERY)		
			echo self::galexec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
		else if($type == self::ALBUM)	
			if(isset($_GET['aweID']))
				echo self::galexec($_GET['aweID'], $type, $placeholder, $edit, $width, $height, $dataOnly);
			else	
				echo self::albexec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
		else
			echo self::execute($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
	}

	static function get($identifier, $type, $placeholder, $edit = false, $width = 'auto', $height = 'auto', $dataOnly = false){
		if($type == self::GALLERY)		
			return self::galexec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
		else if($type == self::VIDEO)		
			return self::videxec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
		else
			return self::execute($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly);
	}

	private static function execute($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly){
		global $con;
		$sql = "SELECT * FROM snippets WHERE uid='".mysqli_escape_string($con, $identifier)."'";
		$rst = mysqli_query($con, $sql);
		$data = mysqli_fetch_assoc($rst);
		$content = ($data["uid"]) ? $data["content"] : $placeholder;
		return CMS::display($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly);
	}
	
	private static function galexec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly){
		global $con;
		$sql = "SELECT * FROM vw_gallery_photos WHERE gallery_id='".mysqli_escape_string($con, $identifier)."' AND status='active'";
		$rst = mysqli_query($con, $sql);
		while($row = mysqli_fetch_assoc($rst)){
			$data[] = $row;
		}		
		$content = ($data[0]["gallery_id"]) ? $data: $placeholder;
		return CMS::galDisplay($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly);
	}
	
	private static function albexec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly){
		global $con;
		if($identifier == '*')
			$sql = "SELECT * FROM vw_gallery_photos WHERE status='active' group by gallery_id order by id asc";
		else
			$sql = "SELECT * FROM vw_gallery_photos WHERE status='active' AND gallery_id IN (".$identifier.") group by gallery_id order by gallery_id asc, id asc";
			
		$rst = mysqli_query($con, $sql);
		while($row = mysqli_fetch_assoc($rst)){
			$data[] = $row;
		}		
		$content = ($data[0]["gallery_id"]) ? $data: $placeholder;
		return CMS::albDisplay($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly);
	}
	
	private static function videxec($identifier, $type, $placeholder, $edit, $width, $height, $dataOnly){
		global $con;
		$sql = "SELECT * FROM snippets WHERE uid='".mysqli_escape_string($con, $identifier)."'";
		$rst = mysqli_query($con, $sql);
		$data = mysqli_fetch_assoc($rst);
		$content = ($data["uid"]) ? $data["content"] : $placeholder;
		return CMS::display($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly);
	}

	private static function display($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly){
		if($edit){
			$data = '';
			if(!$dataOnly){
				$data = '<cmsimg class="superawesome-cms-TX47635439585376895" title="edit" style="width:32px !important; height:32px !important; cursor:pointer; vertical-align:middle; position:absolute; z-index:10;border:none; background: url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REFCMjYyNjA2NTlGMTFFNjk4RDg4N0E1QTgzOUY0MTAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REFCMjYyNjE2NTlGMTFFNjk4RDg4N0E1QTgzOUY0MTAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpEQUIyNjI1RTY1OUYxMUU2OThEODg3QTVBODM5RjQxMCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpEQUIyNjI1RjY1OUYxMUU2OThEODg3QTVBODM5RjQxMCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Ph9XdpwAAAlRSURBVHja1FoLcFTVGf7ua5+EJYQkvBICSdBmQQXbOoKVAiVSkQJC0bFOKR3sgw507Ehl0E6oTLVKO9MRLQULtXSUvghWQHCg2NoCM2Uo8kiASHiFQCTksRuyz3v39j/nJnHv3buQZJfOeHZ+jm7uPef7n+c//7+Cruv4LA85Wwutc784kqbxRH6iEqI8SdacEHRBi8sx+v82oktEtUQnloVX1WdjXyETDRDoMpoeI5pDdA+RkvIQW16wff0k0S6ircTMsf8rAwT8CzQ9Q/RolrS4h2gtMbL/tjJAwAtpepHo27fJpLcTreiLefWaAQI/j6bXiYale0bXdOhxDQloZDkJ80b0ESFBUIgk8WZbdRA9TUxsyhoDBH4NTc/b/jGhQ4vFOGjF5YG7LBfu8sFwjBwIaYDCgWvhOGJXbiB8thXhj1sR6+jk30sK+bgkpNt2PTGxNGMGCPwmW5Oh19RohMFA7rQS5C/0Y9D0UZwBidxCsPFlpptoQwCBDy+h+c+1aN1TDzUWheJwAaItI3+jheYvC63S+sUAgd9M02KLLSARVgmKivw5n0Pxs5Phu78YIgdIJsT+TbemYJgRGRFnqONEExp+cQhNW47xhSW3YnBqHu+SJub0mQEC/zOaVlm/VyMROIf7UP7qTBTO9xvfIW638S10D2JE4Yxf/3s96n7wHjrPXCMzdNs9vZGY+G6vGSDwc7sigmnEIyHkPjgG/q0L4B4+kGD3A7gNIwp9YsEITi3ajmvvnCQmvHZPLrFz7BQGCHw+TaeJBlvB58+uwLhtCyEqMjQ9ntX4KQoyd+zaxdtx9c0jPCBYRpiogpi4YHrPZq1fmcALhtnkTinF+OqFPAxmBTz5gyQonLhf6SoPvRW/m4eCueNIYGHrEc5s69c31QBJfyJNR0xRMqbCMXQA7j32FJxDcrICXhBEHr0Ch85DdMkYOKHIODt0jTShQOuM4si9v0XozHVILof19enJJ7ZVA6utsS+RUFG+8WG4swiexaH6Z3bj6KQNODpxA04vqaYQptPfJK4Jh9eFsZsfMRSQSHGyKlsTIumX0vSIKeJQnC+Y50fhrDsRp4UzBs/CZ0hD3fIdaPjlP6F4PJA8DjRuOoT6p3dzxri/kaDyJpVg6LcmIB6LWJd5kLDeZaeBb5qMjjgXJRnFVQ90JQV6hpIXuKNGGtpw5fX/QJLINFhKQQeY052DK68dRrDmCn+Gb0/7FT83CbLLxVMUy1hsx8A8k/QpPcidPga+u0eQdcYzNptujebcUYjx+xZBcJG5hGPcmQ0njpPtx7mW2NBI4zml+cibVQ4tHrUuOftV10tCDwOkkqKui0iS+Wso+IbfWE7P3ObPLX8XJyavRzQQQuHUMozb+SQEJ0W0UBTxUAijnp+KQV8sNvkZ2zb/iQp+vltGqSDo5ckauCdZG0xliscL39RRPD3I2GF/vANX1x1A55HLqH14E0LERMGXS+EnJuAEildMQdmaSh6JTBGQPr4vFcPpy4GuJqzLT0hmwCz9uArP2MFwFflSFu0z+Gd3oHHtB1ByvZDzBqDj4EWDibZODCEmPn9sGUa/UknpiJqSQzEGnPk58FTkIaGmBBF/MgMl5hc1uIgBHhX6cWNLlnzjKwZ4bou0lpznRfDgBZysfAPRYAjesgJuLLY5GX3HEj/32LyU+wWNUcmX+lyz/etwjsiBkInkV+7skTx31O6hGUB800rpkFK4s97KyRwjBtj5weAeDUiK6rD+XXLLmUn+5f2p4Ck0x9s7Ubx6Bkpfnk3IhF5FCNnnsnvO/akJZaE0ZAJvJ3kOPoSi1ZUoqZpJRqqi1/fxmzzHxaypcsxqLywm3w7wo6se6ht4dn4Eona1mVCyD7RYj/zo5Y5eKSY12gww79UNvmpGv8CzJ6OXgz0HXNJoSY5C580JkoxwXSs5mGaWpE12I3aDT442KTZPkl89s8/g2d4slIbPtPLs1TIuJDNQY3qPcn5WPQhfbOMA0w2J8paLL7xP4NM7rGHzfZe8AU5CpCmI0KkWiLJkV9nrYeAjfrXtZkAS+A2sfd8FHofthSNRChDGJ28chuR1pwH/EJnNzH6B5wKivQP/uIRYxw0Ismi1rKM9DNAF4QoruFq5b36rxjg+bMxIpE/nfxsRb+qA6JRtbL5/Dpt8E2RvNb9dY2f/dYmEeM6ajW4zce9woO3DC2g/fIk8QrZbH4H99ZSjqJ9efiJqZjZvkr6C4OkmtL5/FrLitP75nR9GV6ak039INiOWp+sJDZdW/zsJcrJz6QjsO8udLN7aSWE3BiXfi5Kfz+q3zVtvWg1rDvC0HqnVu9+nuxP/haYF1uqbf9tjGPaon9+UuisIofprOPnAb+DxF1JaUAbflDHwThwO2e3i4JEBeIXuxc0f1OP4tC3cEixVu71k8pXpGhwvmBig90RRxtnv74bv/pFwDfPxfJ1dvuVBLow/tBSeknwurURXEpjpvZkJJxoMo27JTtpftCs5/jRtWYU4Y4682fQAOWj0WhA1C/6KRCzON2CZoUKpsbtkCL+tMc0YjCUyBG/EvNonqhE6dx2SM6UisZ0wHrhVXYg1LpqTzUh2udF+8DyOz/kj9KhKJungYFkFAVlqsXUXtmqe3Ibru051lRhNi99gMrbzFVi0wHpZi6wHOquUtew5g6Nf2YJoY4Dbab/ybbvSIqsFBSI4/rWtaHrrI7uqHBvfI2yNfSnu/qTLJ0y7qZEwnMMGonzdVzF0vp/bvpaN4u7S99BZl7a4u47AL+9PeX0DTd9JW16fW4HilZMx6L4irgyjL6Olj0C8vG58eCum9hM0rD2Ipje7y+sOu3erCfz8TBocG2l6Kl2Dg9lu7ozRKHjcT+G0BK5Rg0iyom2Dg50d0atBBP7VgOY/1aBl18e0xk0bHNWE8OvLQs8lMm0xVaWUHVNaTAk4vB6478iDh8hZNBByrgFMbY8i1thBGW4LQqdbEG3v6E2L6TWS/LJsNvlYl2T9rZp8ibhqdGm4zPWkG4bR5hNl2ZqYWUewq8m3OWtNviQmWJv1pZS2U/bG7WuzWhi5j23UVY4UswB8L4xG994+R+EMf2pwJ02PE7GW1F3o28lwBsZPDd4m4Ef6fYxk69cqxMxomu4mGkfE/juPyNuloU6iVhg/9mC3v2ORiOv0Cv1HmZ+Dn/Wf2/xPgAEAHjg+t8qmoCUAAAAASUVORK5CYII=\');     background-size:contain; background-repeat:no-repeat;" type="'.$type.'" uid="'.$identifier.'" default="'.htmlspecialchars($placeholder).'" val="'.htmlspecialchars($content).'"></cmsimg> ';
			}
			if($type == self::VIDEO){
				$content = @current(explode(" ", str_replace(array('"', "'"), '', end(explode('src=', html_entity_decode($content))))));
			}
			return $data.nl2br(($type == self::IMAGE) ? "<img src='".$content."' style='width:".$width."; height:".$height."' />" : (($type == self::VIDEO) ? '<iframe width="'.$width.'" height="'.$height.'" src="'.$content.'" frameborder="0" allowfullscreen></iframe>' :
 html_entity_decode($content)) );
		}else{
			if($type == self::VIDEO){
				$content = @current(explode(" ", str_replace(array('"', "'"), '', end(explode('src=', html_entity_decode($content))))));
			}
			return nl2br(($type == self::IMAGE) ? "<img src='".$content."' style='width:".$width."; height:".$height."' />" : (($type == self::VIDEO) ? '<iframe width="'.$width.'" height="'.$height.'" src="'.$content.'" frameborder="0" allowfullscreen></iframe>' :
 html_entity_decode($content)) );
		}
	}
	
	
	private static function galDisplay($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly){		
		$output = ($dataOnly)?'':'<h2 class="gallery-heading" style="text-align:center">'.$content[0]['description'].'</h2>';
		$output .= '<div class="page-gallery-flow" id="aweGal"><div id="slides">';
		foreach($content as $key => $image){
			$output .= "<img src='".str_replace(' ', '%20', $image["photo"])."' style='width:".$width."; height:".$height."' />";
			// $output .= "<img src='Thumb.php?url=".urlencode("http://openmindafrica.org/".str_replace(' ', '%20', $image["photo"]))."&w=".$width."&h=".$height."&cache' />";
		}
		$output .= '</div></div>';

		return $output;
		
	}
	
	private static function albDisplay($identifier, $type, $placeholder, $content, $edit, $width, $height, $dataOnly){		
		$output = '';
		$url = ($dataOnly) ? $dataOnly : '';
		$append = (strpos($_SERVER['REQUEST_URI'], "?") !== false) ? '&' : '?';
		$hash = (empty($url)) ? '#aweGal' : '';
		foreach($content as $key => $image){			
			$output .= "<a href='".$url.$append.'aweID='.$image["gallery_id"].$hash
			."'><div style='width:".$width."; height:".(0.75*$width)."px; background-image:url(".str_replace(' ', '%20', $image["photo"])."); background-size:cover; float:left; margin:5px;' title='".$image["description"]."'><span class='gallery-title' style='background:#000;color:#fff;padding:5px;'>".$image["description"]."</span></div></a>";
		}
		$output .= '<div style="clear:both"></div>';
		return $output;
		
	}
	
}

?>
