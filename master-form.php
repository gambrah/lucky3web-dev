
<!--New-->


<form id="indexform" method="post" action="javascript:void(0)">
    <div>
        <label class="check-reset"><img src="production/images/mnos-mtn.png">
            <input type="radio"  name="operator" value="mtn-gh" checked>
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-airtel.png">
            <input type="radio" name="operator" value="tigo-gh">
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-voda.png">
            <input type="radio" name="operator" value="vodafone-gh-ussd">
            <span class="checkmark"></span> </label>
    </div>
    <div>
        <label for="" id="correctformat" style="text-transform: lowercase; color: red">enter the write format e.g 244709137</label>
        <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="mobile" name="mobile" required maxlength="9
">
        <label class="app-l telephone_digit">+233</label>

    </div>
    <div>
        <h5>What are your 3 Lucky Numbers?</h5>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_1" min="0" max="9" required>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_2" min="0" max="9" required>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_3" min="0" max="9" required>
    </div>

    <div>
        <h5>Enter Your Stake (from GHC2)</h5>
        <label class="app-l">GHC</label>
        <input name="amt" type="number" required class="deposit-amount" placeholder="2-5,000" id="" min="2" max="5000" >
    </div>
    <div>
        <input type="submit" value="Play Now" class="submit_btn fa fa-arrows" id="submit">
    </div>
    <div id="payment-notification" class="success-notification">
        Please confirm payment on your phone. Enter Mobile Money PIN to Lucky 3 ticket purchase.
    </div>
    <div id="payment-notification-error" class="error-notification">
        Payment Unsuccessful
    </div>
</form>







<!--Old
<form id="indexform" ethod="post" action="javascript:void(0)">
    <div>
        <label class="check-reset"><img src="production/images/mnos-mtn.png">
            <input type="radio"  name="operator" value="mtn" checked>
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-airtel.png">
            <input type="radio" name="operator" value="airtel">
            <span class="checkmark"></span> </label>
        <label class="check-reset"><img src="production/images/mnos-voda.png">
            <input type="radio" name="operator" value="voda">
            <span class="checkmark"></span> </label>
    </div>
    <div>
        <label for="" id="correctformat" style="text-transform: lowercase; color: red">enter the write format e.g 244709137</label>
        <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="" name="mobile" required maxlength="9
">
        <label class="app-l telephone_digit">+233</label>

    </div>
    <div>
        <h5>What are your 3 Lucky Numbers?</h5>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_1" min="0" max="9" required>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_2" min="0" max="9" required>
        <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_3" min="0" max="9" required>
    </div>

    <div>
        <h5>Enter Your Stake (from GHC2)</h5>
        <label class="app-l">GHC</label>
        <input name="amount" type="number" required class="deposit-amount" placeholder="2-5,000" oninvalid="this.setCustomValidity('Please enter your desire Amount')" oninput="this.setCustomValidity('')" id="" min="2" max="5000">
    </div>
    <div>
        <input type="submit" value="Play Now" class="submit_btn fa fa-arrows" id="submit">
    </div>
    <div id="payment-notification">
        Please confirm payment on your phone. Enter Mobile Money PIN to Lucky 3 ticket purchase.
    </div>
    <div id="payment-notification-error">
        Payment Unsuccessful
    </div>
</form>-->