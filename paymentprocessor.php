<?php

$hubtel = include('config/hubtel.php');
$base_url = 'https://api.hubtel.com/v1/merchantaccount/merchants/79391/receive/mobilemoney';
$username=$hubtel['api_key']['client_id'];
$password=$hubtel['api_key']['client_secret'];

$msisdn="233".htmlspecialchars($_POST['msisdn']);
$channel=htmlspecialchars($_POST['operator']);
$amount=floatval(htmlspecialchars($_POST['amount']));
$paymentData = [
    "customermsisdn" =>$msisdn,
    "channel" =>$channel,
    "amount" =>$amount,
    "primarycallbackurl" => "https://webhook.site/58db5b47-ef1a-43e4-8ca2-f0af8abee143",
    "secondarycallbackurl" => "https://webhook.site/58db5b47-ef1a-43e4-8ca2-f0af8abee143",
    "description" => "Lucky3 Payments via Web",
    "clientreference" => $msisdn

];


$payload = json_encode($paymentData);

// Prepare new cURL resource
$ch = curl_init($base_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);

// Set HTTP Header for POST request
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($payload))

);

// Submit the POST request
$result = curl_exec($ch);

// Close cURL session handle
curl_close($ch);

$response = (array)json_decode($result);

// Process your response here

error_reporting(error_reporting() & ~E_NOTICE);

echo json_encode($response);


