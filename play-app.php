<div class="modal">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
        <div class="modal-header">

            <div class="modal-body">
                <div class="modal-content">



                    <div class="bet-app animated bounceIn">

                        <form id="indexform" method="post" action="javascript:void(0)">
                            <div>
                                <label class="check-reset"><img src="production/images/mnos-mtn.png">
                                    <input type="radio"  name="operator" value="mtn-gh" checked>
                                    <span class="checkmark"></span> </label>
                                <label class="check-reset"><img src="production/images/mnos-airtel.png">
                                    <input type="radio" name="operator" value="tigo-gh">
                                    <span class="checkmark"></span> </label>
                                <label class="check-reset"><img src="production/images/mnos-voda.png">
                                    <input type="radio" name="operator" value="vodafone-gh-ussd">
                                    <span class="checkmark"></span> </label>
                            </div>
                            <div>
                                <label for="" id="correctformat" style="text-transform: lowercase; color: red">enter the write format e.g 244709137</label>
                                <input type="tel" x-autocompletetype="tel" pattern="\d*" placeholder="Mobile Phone Number" id="mobile" name="mobile" required maxlength="9
">
                                <label class="app-l telephone_digit">+233</label>
                                <!--        <div class="country"></div>-->
                            </div>
                            <div>
                                <h5>What are your 3 Lucky Numbers?</h5>
                                <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_1" min="0" max="9" required>
                                <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_2" min="0" max="9" required>
                                <input type="number" pattern="\d*" placeholder="0" id="" name="lucky_3" min="0" max="9" required>
                            </div>

                            <div>
                                <h5>Enter Your Stake (from GHC2)</h5>
                                <label class="app-l">GHC</label>
                                <input name="amt" type="number" required class="deposit-amount" placeholder="2-5,000" id="" min="2" max="5000" >
                            </div>
                            <div>
                                <input type="submit" value="Play Now" class="submit_btn fa fa-arrows" id="submit">
                            </div>
                            <div id="payment-notification" class="success-notification">
                                Please confirm payment on your phone. Enter Mobile Money PIN to Lucky 3 ticket purchase.
                            </div>
                            <div id="payment-notification-error" class="error-notification">
                                Payment Unsuccessful
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
        <!--  validation script  -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.19.0/jquery.validate.min.js"></script>
        <script>
            // * Quick modal behavior---Please do not take this code **
            $('.modal-toggle').on('click', function(e) {
                e.preventDefault();
                $('.modal').toggleClass('is-visible');
            });
        </script>
        <script type="text/javascript">
            $("#payment-notification").hide();
            $("#payment-notification-error").hide();
            $("#correctformat").hide();
            let numberFormat=["20","23","24","26","27","50","54","55","56","57"];
            $(document).ready(function($){
                $("#submit").click(function(e){
                    var formStatus = $('#indexform').validate({
                        rules: {
                            mobile: {
                                required: false
                            },
                            lucky_1: {
                                required: true
                            },lucky_2: {
                                required: true
                            },lucky_3: {
                                required: true
                            },
                            amount:{
                                required:true
                            }
                        },
                        messages: {
                            mobile:"enter the correct number",
                            lucky_1: "required",
                            lucky_2: "required",
                            lucky_3: "required",
                            amount:"enter the desired amount"
                        },
                    }).form();

                    if (!checkNumberFormat()){
                        $("#correctformat").show().delay(1000).fadeOut();
                    }
                    if (formStatus==true && checkNumberFormat()){
                        e.preventDefault();
                        $.ajax({
                            type: "POST",
                            url: "paymentprocessor.php",
                            data: {
                                format:"json",
                                msisdn:$("input[name=mobile]").val(),
                                amount:$("input[name=amt]").val(),
                                numbers:$("input[name=lucky_1]").val()+","+$("input[name=lucky_2]").val()+","+$("input[name=lucky_3]").val(),
                                operator:$("input[name=operator]:checked").val(),
                                accessChannel:"WEB_MOBILE"
                            },
                            beforeSend: function(){
                                // Show image container
                                $("#submit").val("Playing ......");

                            },
                            success : function(data){
                                // let response =JSON.parse(data.replace(/(<([^>]+)>)/ig,""));
                                console.log(data)
                                $("#submit").val("Play Now");
                                clearFields();
                                $("#payment-notification").show().delay(6000).fadeOut();
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');
                                console.log('jqXHR:');
                                console.log(jqXHR);
                                console.log('textStatus:');
                                console.log(textStatus);
                                console.log('errorThrown:');
                                console.log(errorThrown);
                            }
                        });
                    }
                })

            });

            function clearFields(){
                $("input[name=mobile]").val('');
                $("input[name=lucky_1]").val('');
                $("input[name=lucky_2]").val('');
                $("input[name=lucky_3]").val('');
                $("input[name=amt]").val('');
                $("input[name=radio]").attr('checked', 'checked');
            }

            function checkNumberFormat(){
                let mobile = $("input[name=mobile]").val().substring(0,2);
                console.log(mobile)
                let a=false;
                numberFormat.map(number=>{
                    if (mobile && mobile==number){
                        a=true;
                    }
                });
                return a;
            }
        </script>